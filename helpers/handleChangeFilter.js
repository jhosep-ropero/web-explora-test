import { programate } from "../data/programate";

export const handleChangeFilter = (e, categories, setCategories, checked, setChecked, checkedMonth, setCheckedMonth, deleteCategory, checkedInit) => {
    console.log(deleteCategory);
    if (e.target.value === 'todos') {
        setChecked(checkedInit)
        setCategories([...categories, e.target.value])
        return
    }
    if (e.target.value === 'Educación') {
        setChecked({...checked, todos: false, educacion: !checked.educacion })
        if (!checked.educacion) {
            setCategories([...categories, e.target.value])
        } else {
            deleteCategory(e.target.value)
        }
    }
    if (e.target.value === 'Familiar') {
        setChecked({...checked, todos: false, todoPublico: false, familiar: !checked.familiar })
        if (!checked.familiar) {
            setCategories([...categories, e.target.value])
        } else {
            deleteCategory(e.target.value)
        }
    }
    if (e.target.value === 'Presencial') {
        setChecked({...checked, todos: false, presencial: !checked.presencial })
        if (!checked.presencial) {
            setCategories([...categories, e.target.value])
        } else {
            deleteCategory(e.target.value)
        }
    }
    if (e.target.value === 'Exploratorio') {
        setChecked({...checked, todos: false, todoMuseo: false, exploratorio: !checked.exploratorio })
        if (!checked.exploratorio) {
            setCategories([...categories, e.target.value])
        } else {
            deleteCategory(e.target.value)
        }
    }
    if (e.target.value === 'Jóvenes y adultos') {
        setChecked({...checked, todos: false, todoPublico: false, adultos: !checked.adultos })
        if (!checked.adultos) {
            setCategories([...categories, e.target.value])
        } else {
            deleteCategory(e.target.value)
        }
    }
    if (e.target.value === 'Online') {
        setChecked({...checked, todos: false, online: !checked.online })
        if (!checked.online) {
            setCategories([...categories, e.target.value])
        } else {
            deleteCategory(e.target.value)
        }
    }
    if (e.target.value === 'Parque Explora') {
        setChecked({...checked, todos: false, todoMuseo: false, explora: !checked.explora })
        if (!checked.explora) {
            const arregloData = [...categories, e.target.value];
            // Set permite almacenar valor único (No repetidos), en objeto
            const dataArr = new Set(arregloData);
            // Se convierte el objeto Set en un array
            let result = [...dataArr];
            setCategories(result);
        } else {
            deleteCategory(e.target.value)
        }
    }
    if (e.target.value === 'Niños') {
        setChecked({...checked, todos: false, todoPublico: false, niños: !checked.niños })
        if (!checked.niños) {
            setCategories([...categories, e.target.value])
        } else {
            deleteCategory(e.target.value)
        }
    }
    if (e.target.value === 'Planetario') {
        setChecked({...checked, todos: false, todoMuseo: false, planetario: !checked.planetario })
        if (!checked.planetario) {
            setCategories([...categories, e.target.value])
        } else {
            console.log(e.target.value)
            deleteCategory(e.target.value)
        }
    }
    if (e.target.value === 'Maestros') {
        setChecked({...checked, todos: false, todoPublico: false, maestros: !checked.maestros })
        if (!checked.maestros) {
            setCategories([...categories, e.target.value])
        } else {
            deleteCategory(e.target.value)
        }
    }
    if (e.target.value === 'Gratis') {
        setChecked({...checked, gratis: !checked.gratis })
        if (!checked.gratis) {
            setCategories([...categories, e.target.value])
        } else {
            deleteCategory(e.target.value)
        }
    }
    if (e.target.value === 'Donación') {
        setChecked({...checked, donacion: !checked.donacion })
        if (!checked.donacion) {
            setCategories([...categories, e.target.value])
        } else {
            deleteCategory(e.target.value)
        }
    }
    if (e.target.value === 'Museo') {
        setChecked({...checked, todoMuseo: !checked.todoMuseo })
        if (!checked.todoMuseo) {
            setCategories([...categories, e.target.value])
        } else {
            deleteCategory(e.target.value)
        }
    }
    if (e.target.value === 'Publico') {
        setChecked({...checked, todoPublico: !checked.todoPublico })
        if (!checked.todoPublico) {
            setCategories([...categories, e.target.value])
        } else {
            deleteCategory(e.target.value)
        }
    }
    if (e.target.value === 'ENE') {
        setCheckedMonth({...checkedMonth, Enero: true, Febrero: false, Marzo: false, Abril: false, Mayo: false, Junio: false, Julio: false, Agosto: false, Septiembre: false, Octubre: false, Noviembre: false, Diciembre: false })
        if (!checkedMonth.Enero) {
            setCategories([e.target.value])
        } else {
            deleteCategory(e.target.value)
        }
    }
    if (e.target.value === 'FEB') {
        setCheckedMonth({...checkedMonth, Febrero: true, Enero: false, Marzo: false, Abril: false, Mayo: false, Junio: false, Julio: false, Agosto: false, Septiembre: false, Octubre: false, Noviembre: false, Diciembre: false })
        if (!checkedMonth.Febrero) {
            setCategories([e.target.value])
        } else {
            deleteCategory(e.target.value)
        }
    }
    if (e.target.value === 'MAR') {
        setCheckedMonth({...checkedMonth, Marzo: true, Febrero: false, Enero: false, Abril: false, Mayo: false, Junio: false, Julio: false, Agosto: false, Septiembre: false, Octubre: false, Noviembre: false, Diciembre: false })
        if (!checkedMonth.Marzo) {
            setCategories([e.target.value])
        } else {
            deleteCategory(e.target.value)
        }
    }
    if (e.target.value === 'ABR') {
        setCheckedMonth({...checkedMonth, Abril: true, Marzo: false, Febrero: false, Enero: false, Mayo: false, Junio: false, Julio: false, Agosto: false, Septiembre: false, Octubre: false, Noviembre: false, Diciembre: false })
        if (!checkedMonth.Abril) {
            setCategories([e.target.value])
        } else {
            deleteCategory(e.target.value)
        }
    }
    if (e.target.value === 'MAY') {
        setCheckedMonth({...checkedMonth, Mayo: true, Abril: false, Marzo: false, Febrero: false, Enero: false, Junio: false, Julio: false, Agosto: false, Septiembre: false, Octubre: false, Noviembre: false, Diciembre: false })
        if (!checkedMonth.Mayo) {
            setCategories([e.target.value])
        } else {
            deleteCategory(e.target.value)
        }
    }
    if (e.target.value === 'JUN') {
        setCheckedMonth({...checkedMonth, Junio: true, Mayo: false, Abril: false, Marzo: false, Febrero: false, Enero: false, Julio: false, Agosto: false, Septiembre: false, Octubre: false, Noviembre: false, Diciembre: false })
        if (!checkedMonth.Junio) {
            setCategories([e.target.value])
        } else {
            deleteCategory(e.target.value)
        }
    }
    if (e.target.value === 'JUL') {
        setCheckedMonth({...checkedMonth, Julio: true, Junio: false, Mayo: false, Abril: false, Marzo: false, Febrero: false, Enero: false, Agosto: false, Septiembre: false, Octubre: false, Noviembre: false, Diciembre: false })
        if (!checkedMonth.Julio) {
            setCategories([e.target.value])
        } else {
            deleteCategory(e.target.value)
        }
    }
    if (e.target.value === 'AGO') {
        setCheckedMonth({...checkedMonth, Agosto: true, Julio: false, Junio: false, Mayo: false, Abril: false, Marzo: false, Febrero: false, Enero: false, Septiembre: false, Octubre: false, Noviembre: false, Diciembre: false })
        if (!checkedMonth.Agosto) {
            setCategories([e.target.value])
        } else {
            deleteCategory(e.target.value)
        }
    }
    if (e.target.value === 'SEP') {
        setCheckedMonth({...checkedMonth, Septiembre: true, Agosto: false, Julio: false, Junio: false, Mayo: false, Abril: false, Marzo: false, Febrero: false, Enero: false, Octubre: false, Noviembre: false, Diciembre: false })
        if (!checkedMonth.Septiembre) {
            setCategories([e.target.value])
        } else {
            deleteCategory(e.target.value)
        }
    }
    if (e.target.value === 'OCT') {
        setCheckedMonth({...checkedMonth, Octubre: true, Septiembre: false, Agosto: false, Julio: false, Junio: false, Mayo: false, Abril: false, Marzo: false, Febrero: false, Enero: false, Noviembre: false, Diciembre: false })
        if (!checkedMonth.Octubre) {
            setCategories([e.target.value])
        } else {
            deleteCategory(e.target.value)
        }
    }
    if (e.target.value === 'NOV') {
        setCheckedMonth({...checkedMonth, Noviembre: true, Octubre: false, Septiembre: false, Agosto: false, Julio: false, Junio: false, Mayo: false, Abril: false, Marzo: false, Febrero: false, Enero: false, Diciembre: false })
        if (!checkedMonth.Noviembre) {
            setCategories([e.target.value])
        } else {
            deleteCategory(e.target.value)
        }
    }
    if (e.target.value === 'DIC') {
        setCheckedMonth({...checkedMonth, Diciembre: true, Noviembre: false, Octubre: false, Septiembre: false, Agosto: false, Julio: false, Junio: false, Mayo: false, Abril: false, Marzo: false, Febrero: false, Enero: false })
        if (!checkedMonth.Diciembre) {
            setCategories([e.target.value])
        } else {
            deleteCategory(e.target.value)
        }
    }
}

export const handleChangeFilterCalendar = (e, categories, setCategories, valueDateSelect, setValueDateSelect, deleteCategory, defaultRange) => {
    if (JSON.stringify(e.from) === JSON.stringify(e.to) || e.to === null) {
        programate.map((item) => {
            if (e.from.day === item.dateCalendar.day && e.from.month === item.dateCalendar.month && e.from.year === item.dateCalendar.year) {
                const date = item.date.day + " " + item.date.month;
                const arregloData = [date];
                // Set permite almacenar valor único (No repetidos), en objeto
                const dataArr = new Set(arregloData);
                // Se convierte el objeto Set en un array
                let result = [...dataArr];
                setCategories(result);
                console.log(result);
            }
        })
    } else if (JSON.stringify(e.from) !== JSON.stringify(e.to) && e.to !== null) {
        // TODO Validar fecha
        let fromTimestamp = new Date(e.from.year, e.from.month - 1, e.from.day) / 1000;
        let toTimestamp = new Date(e.to.year, e.to.month - 1, e.to.day) / 1000;

        // console.log(fromTimestamp);
        // console.log(toTimestamp);

        const arregloData = [];

        programate.filter((item) => {
            let dateItemTimestamp = new Date(item.dateCalendar.year, item.dateCalendar.month - 1, item.dateCalendar.day) / 1000;
            if (dateItemTimestamp >= fromTimestamp && dateItemTimestamp <= toTimestamp) {
                const date = item.date.day + " " + item.date.month;
                arregloData.push(date);
            }
        })

        const dataArr = new Set(arregloData);
        let result = [...dataArr];
        setCategories(result.sort());
    }
}