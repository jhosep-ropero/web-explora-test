import { ticketsTypes } from "../types/ticketsTypes";

export const setTicketState = (state) => ({
  type: ticketsTypes.ticketState,
  payload: state
});

export const setTicketDate = (date) => ({
  type: ticketsTypes.ticketDate,
  payload: date
});

export const setTicketNumber = (number) => ({
  type: ticketsTypes.ticketNumber,
  payload: number
});
export const setTicketProyection = (proyection) => ({
  type: ticketsTypes.ticketProyection,
  payload: proyection
});

export const setTicketChairs = (chairs) => ({
  type: ticketsTypes.ticketChairs,
  payload: chairs

});
export const setTicketName = (name) => ({
  type: ticketsTypes.ticketName,
  payload: name
});
export const setTicketLastName = (lastname) => ({
  type: ticketsTypes.ticketLastName,
  payload: lastname
});
export const setTicketPhoneNumber = (phonenumber) => ({
  type: ticketsTypes.ticketPhoneNumber,
  payload: phonenumber
});
export const setTicketEmail = (email) => ({
  type: ticketsTypes.ticketEmail,
  payload: email
});
export const setTicketId = (id) => ({
  type: ticketsTypes.ticketId,
  payload: id
});
