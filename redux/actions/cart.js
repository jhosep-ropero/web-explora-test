import { cartTypes } from "../types/cartTypes";

export const addCourse = (state) => ({
  type: cartTypes.addCourse,
  payload: state,
});

export const removeCourse = (state) => ({
  type: cartTypes.removeCourse,
  payload: state,
});
