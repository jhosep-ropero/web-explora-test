import { uiTypes } from "../types/uiTypes"


export const uiTabFacebook = ()=>({
    type: uiTypes.uiTabFacebook,
})
export const uiTabInstagram = ()=>({
    type: uiTypes.uiTabInstagram
})
export const uiTabTwitter = ()=>({
    type: uiTypes.uiTabTwitter
})

export const uiTabAccesibility = ()=>({
    type: uiTypes.uiTabAccesibility
})
