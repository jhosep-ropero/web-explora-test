import { accesibilityTypes } from "../types/accesibilityTypes"


export const accesContrastNormal = ()=>({
    type: accesibilityTypes.accesContrastNormal
})
export const accesContrastDark = ()=>({
    type: accesibilityTypes.accesContrastDark
})
export const accesContrastLight = ()=>({
    type: accesibilityTypes.accesContrastLight
})
export const accesContrastHigh = ()=>({
    type: accesibilityTypes.accesContrastHigh
})
/****************************************** */

export const accesMonocromatico = ()=>({
    type: accesibilityTypes.accesMonocromatico
})
export const accesSaturationLight = ()=>({
    type: accesibilityTypes.accesSaturationLight
})
export const accesSaturationHigh = ()=>({
    type: accesibilityTypes.accesSaturationhigh
})
/******************************************** */

export const acceshighlightTitles = ()=>({
    type: accesibilityTypes.acceshighlightTitles
})
export const accesTitlesNormal = ()=>({
    type: accesibilityTypes.accesTitlesNormal
})
export const acceshighlightLinks = ()=>({
    type: accesibilityTypes.acceshighlightLinks
})
export const accesLinksNormal = ()=>({
    type: accesibilityTypes.accesLinksNormal
})
/******************************************** */

export const accesFontSizeNormal = ()=>({
    type: accesibilityTypes.accesFontSizeNormal
})
export const accesFontSize2x = ()=>({
    type: accesibilityTypes.accesFontSize2x
})
export const accesFontSizeLess = ()=>({
    type: accesibilityTypes.accesFontSizeLess
})
/***************************************** */

export const accesSpaceNormal = ()=>({
    type: accesibilityTypes.accesSpaceNormal
})
export const accesSpaceLong = ()=>({
    type: accesibilityTypes.accesSpaceLong
})
export const accesSpaceShort = ()=>({
    type: accesibilityTypes.accesSpaceShort
})
/***************************************** */

export const accesLineNormal = ()=>({
    type: accesibilityTypes.accesLineNormal
})
export const accesLineLong = ()=>({
    type: accesibilityTypes.accesLineLong
})
export const accesLineShort = ()=>({
    type: accesibilityTypes.accesLineShort
})
/***************************************** */

export const scaleContentNormal = ()=>({
    type: accesibilityTypes.scaleContentNormal
})
export const scaleContentLess = ()=>({
    type: accesibilityTypes.scaleContentLess
})
export const scaleContent2x = ()=>({
    type: accesibilityTypes.scaleContent2x
})

/***************************************** */
export const accesSilence = ()=>({
    type: accesibilityTypes.accesSilence
})
/***************************************** */
export const accesHideImages = ()=>({
    type: accesibilityTypes.accesHideImages
})
/***************************************** */
export const accesReadingGuide = ()=>({
    type: accesibilityTypes.accesReadingGuide
})
/***************************************** */
export const accesReadingMode = ()=>({
    type: accesibilityTypes.accesReadingMode
})
/***************************************** */
export const accesStopAnimations = ()=>({
    type: accesibilityTypes.accesStopAnimations
})
/***************************************** */
