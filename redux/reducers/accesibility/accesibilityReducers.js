import { accesibilityTypes } from "../../types/accesibilityTypes";

const initialState = {
    contrast: 'Normal',
    fontSize: 'Normal',
    highlightTitles: 'Normal',
    highlightLinks: 'Normal',
    space:'Normal',
    lineHeight:'Normal',
    scale: 'Normal',
    silence: false,
    hideImages: false,
    readingGuide: false,
    stopAnimations: false,
    readingMode: false
}

//All room reducer
export const accesibilityReducers = (state = initialState, action) => {
  switch (action.type) {

    case accesibilityTypes.accesContrastNormal:
        return{
            ...state,
            contrast: 'Normal'
        }
    case accesibilityTypes.accesContrastDark:
        return{
            ...state,
            contrast: 'Dark'
        }
    case accesibilityTypes.accesContrastLight:
        return{
            ...state,
            contrast: 'Light'
        }
    case accesibilityTypes.accesContrastHigh:
        return{
            ...state,
            contrast: 'High'
        }
    /*********************************************** */

    case accesibilityTypes.accesMonocromatico:
        return{
            ...state,
            contrast: 'Monocromatico'
        }
    case accesibilityTypes.accesSaturationLight:
        return{
            ...state,
            contrast: 'SaturationLight'
        }
    case accesibilityTypes.accesSaturationhigh:
        return{
            ...state,
            contrast: 'SaturationHigh'
        }
    /********************************************* */

    case accesibilityTypes.accesFontSizeNormal:
        return{
            ...state,
            fontSize: 'Normal'
        }
    case accesibilityTypes.accesFontSizeLess:
        return{
            ...state,
            fontSize: 'less'
        }
    case accesibilityTypes.accesFontSize2x:
        return{
            ...state,
            fontSize: '2x'
        }
    /*********************************************** */

    case accesibilityTypes.acceshighlightTitles:
        return{
            ...state,
            highlightTitles: 'titles'
        }
    case accesibilityTypes.accesTitlesNormal:
        return{
            ...state,
            highlightTitles: 'Normal'
        }
    case accesibilityTypes.acceshighlightLinks:
        return{
            ...state,
            highlightLinks: 'links'
        }
    case accesibilityTypes.accesLinksNormal:
        return{
            ...state,
            highlightLinks: 'Normal'
        }
    /************************************************** */

    case accesibilityTypes.accesSpaceNormal:
        return{
            ...state,
            space: 'Normal'
        }
    case accesibilityTypes.accesSpaceShort:
        return{
            ...state,
            space: 'short'
        }
    case accesibilityTypes.accesSpaceLong:
        return{
            ...state,
            space: 'long'
        }
    /********************************************* */

    case accesibilityTypes.accesLineNormal:
        return{
            ...state,
            lineHeight: 'Normal'
        }
    case accesibilityTypes.accesLineShort:
        return{
            ...state,
            lineHeight: 'short'
        }
    case accesibilityTypes.accesLineLong:
        return{
            ...state,
            lineHeight: 'long'
        }

    /********************************************* */

    case accesibilityTypes.scaleContentNormal:
        return{
            ...state,
            scale: 'Normal'
        }
    case accesibilityTypes.scaleContentLess:
        return{
            ...state,
            scale: 'less'
        }
    case accesibilityTypes.scaleContent2x:
        return{
            ...state,
            scale: '2x'
        }
    /********************************************* */

    case accesibilityTypes.accesSilence:
        return{
            ...state,
            silence: !state.silence
        }
    case accesibilityTypes.accesHideImages:
        return{
            ...state,
            hideImages: !state.hideImages
        }
    case accesibilityTypes.accesReadingGuide:
        return{
            ...state,
            readingGuide: !state.readingGuide
        }
    case accesibilityTypes.accesReadingMode:
        return{
            ...state,
            readingMode: !state.readingMode
        }
    case accesibilityTypes.accesStopAnimations:
        return{
            ...state,
            stopAnimations: !state.stopAnimations
        }
        
    default:
        return state
}
};
