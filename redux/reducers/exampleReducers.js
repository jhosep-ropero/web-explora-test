import { exampleTypes } from "../types/exampleTypes";

const initialState = {
    error: false
}
//All room reducer
export const exampleReducer = (state=initialState , action) =>{
    switch (action.type) {
        case exampleTypes.succes:
            return {
                ...state,
                error : false
            }
        case exampleTypes.fail:
            return{
                ...state,
                error : true
            }
    
        default:
            return state;
    }
}