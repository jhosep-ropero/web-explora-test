import { cartTypes } from "../../types/cartTypes";

const initialState = {
  courses: [],
  products: [],
};

export default function cart(state = initialState.courses, action) {
  switch (action.cartTypes) {
    case cartTypes.addCourse:
      return [...state, action.payload];
    case cartTypes.removeCourse:
      return state.filter((i) => i.id !== action.payload.id);
    default:
      return state;
  }
}
