import { ticketsTypes } from "../../types/ticketsTypes";

const initialState = {
  ticketState: 0,
  ticketDate: "",
  ticketNumber: 1,
  ticketProyection: "",
  ticketChairs: "",
  ticketName: "",
  ticketLastName: "",
  ticketPhoneNumber: "",
  ticketEmail: "",
  ticketId: "",
};
//All room reducer
export const ticketsReducers = (state = initialState, action) => {
  switch (action.type) {
    case ticketsTypes.ticketState:
      return {
        ...state,
        ticketState: action.payload,
      };
    case ticketsTypes.ticketDate:
      return {
        ...state,
        ticketDate: action.payload,
      };
    case ticketsTypes.ticketNumber:
      return {
        ...state,
        ticketNumber: action.payload,
      };
    case ticketsTypes.ticketProyection:
      return {
        ...state,
        ticketProyection: action.payload,
      };
    case ticketsTypes.ticketChairs:
      return {
        ...state,
        ticketChairs: action.payload,
      };
    case ticketsTypes.ticketName:
      return {
        ...state,
        ticketName: action.payload,
      };
    case ticketsTypes.ticketLastName:
      return {
        ...state,
        ticketLastName: action.payload,
      };
    case ticketsTypes.ticketPhoneNumber:
      return {
        ...state,
        ticketPhoneNumber: action.payload,
      };
    case ticketsTypes.ticketEmail:
      return {
        ...state,
        ticketEmail: action.payload,
      };
    case ticketsTypes.ticketId:
      return {
        ...state,
        ticketId: action.payload,
      };
    default:
      return state;
  }
};
