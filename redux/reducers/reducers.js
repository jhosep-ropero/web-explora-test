import { combineReducers } from "redux";
import { exampleReducer } from "./exampleReducers";
import { uiTabNetReducer } from "./ui/uiTabNetReducer";
import { ticketsReducers } from "./tickets/ticketsReducer";
import { accesibilityReducers } from "./accesibility/accesibilityReducers";
import { uiTabAccesReducer } from "./ui/uiTabAccesReducer";
const reducer = combineReducers({
    examples: exampleReducer,
    uiTabRedes: uiTabNetReducer,
    tickets: ticketsReducers,
    accesibility: accesibilityReducers,
    uiTabAccesibility: uiTabAccesReducer
})

export default reducer