import { uiTypes } from "../../types/uiTypes";

const initialState = {
    accesibilityActive: false,
}
//All room reducer
export const uiTabAccesReducer = (state=initialState , action) =>{
    switch (action.type) {
        case uiTypes.uiTabAccesibility:
            return {
                ...state,
                accesibilityActive: !state.accesibilityActive,
            }        
    
        default:
            return state;
    }
}