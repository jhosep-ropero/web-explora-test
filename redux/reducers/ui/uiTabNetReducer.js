import { uiTypes } from "../../types/uiTypes";

const initialState = {
    facebookActive: true,
    instagramActive: false,
    twitterActive: false
}
//All room reducer
export const uiTabNetReducer = (state=initialState , action) =>{
    switch (action.type) {
        case uiTypes.uiTabFacebook:
            return {
                ...state,
                facebookActive: true,
                instagramActive: false,
                twitterActive: false
            }
        case 'tabInstagram':
            console.log('instagram')
            return {
                ...state,
                facebookActive: false,
                instagramActive: true,
                twitterActive: false
            }
        case uiTypes.uiTabTwitter:
            return {
                ...state,
                facebookActive: false,
                instagramActive: false,
                twitterActive: true
            }
        
    
        default:
            return state;
    }
}