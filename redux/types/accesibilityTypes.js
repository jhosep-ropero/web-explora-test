export const accesibilityTypes = {

    accesContrastNormal: 'contrastNormal',
    accesContrastDark: 'contrastDark',
    accesContrastLight: 'contrastLight',
    accesContrastHigh: 'contrastHigh',

    accesMonocromatico: 'monocromatico',
    accesSaturationLight: 'saturationLight',
    accesSaturationhigh: 'saturationhigh',

    accesFontSizeNormal: 'fontSizeNormal',
    accesFontSize2x: 'fontSize2x',
    accesFontSizeLess: 'fontSizeLess',

    accesTitlesNormal: 'titlesNormal',
    acceshighlightTitles: 'highlightTitles',
    accesLinksNormal: 'linksNormal',
    acceshighlightLinks: 'highlightLinks',

    accesSpaceNormal: 'spaceNormal',
    accesSpaceShort: 'spaceShort',
    accesSpaceLong: 'spaceLong',

    accesLineNormal: 'lineNormal',
    accesLineShort: 'lineShort',
    accesLineLong: 'lineLong',

    scaleContentNormal: 'scaleNormal',
    scaleContentLess: 'scaleLess',
    scaleContent2x: 'scale2x',

    accesSilence: 'silence',
    accesHideImages: 'hideImages',
    accesReadingGuide: 'readingGuide',
    accesStopAnimations: 'stopAnimations',
    accesReadingMode: 'readingMode',
}