export const ticketsTypes = {
    ticketState: '[tickets] Set ticket State',
    ticketDate: '[tickets] Set ticket Date',
    ticketNumber: '[tickets] Set ticket Number',
    ticketProyection: '[tickets] Set ticket Proyection',
    ticketChairs: '[tickets] Set ticket chair',
    ticketName: '[tickets] Set ticket Name',
    ticketLastName: '[tickets] Set ticket LastName',
    ticketPhoneNumber: '[tickets] Set ticket PhoneNumber',
    ticketEmail: '[tickets] Set ticket Email',
    ticketId: '[tickets] Set ticket Id',
}