export const cartTypes = {
  addCourse: "[cart] Add Course",
  removeCourse: "[cart] Remove Course",
  addProduct: "[cart] Add Products",
  removeProduct: "[cart] Remove Products",
};
