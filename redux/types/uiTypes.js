export const uiTypes = {
    uiTabFacebook: 'tabFacebook',
    uiTabInstagram: 'tabInstagram',
    uiTabTwitter: 'tabTwitter',
    uiTabAccesibility: 'tabAccessibility'
}