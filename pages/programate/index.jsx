import React from "react";

// Components
import { Layout } from "../../components/layouts/Layout";
import { NavbarMenu } from "../../components/templates/navbar/NavbarMenu";
import Banner from "../../components/pages/programate/Banner/Banner";
import CarouselStore from "../../components/modules/CarouselStore";
import ExploraRedes from "../../components/pages/inicio/ExploraRedes";
import OurProgramming from "../../components/pages/programate/OurProgramming/OurProgramming";

//Data
import { getByType } from "../../lib/api";
import { constants } from "../../config/";

const index = ({ data }) => {
  return (
    <Layout
      meta={{ title: "Programate" }}
      withNetworkig
      withCourseShop
      shopColor="#f9d51d"
    >
      <Banner title={"NUESTRA PROGRAMACIÓN"} />
      <OurProgramming events={data} />
    </Layout>
  );
};

export async function getStaticProps() {
  try {
    const data =
      (await getByType(
        constants.ourProgramming,
        "title,content,slug,metadata.audience,metadata.location,metadata.date,metadata.modality,metadata.hour,metadata.price,thumbnail",
        {}
      )) || undefined;
    data.sort(function (a, b) {
      var c = new Date(a.metadata.date);
      var d = new Date(b.metadata.date);
      return c - d;
    });
    return {
      props: { data },
    };
  } catch (error) {
    console.log(error);
  }
}
export default index;
