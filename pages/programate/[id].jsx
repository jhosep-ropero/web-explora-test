import { useRouter } from "next/router";

// Components
import { Layout } from "../../components/layouts/Layout";
import { NavbarMenu } from "../../components/templates/navbar/NavbarMenu";
import BannerEvent from "../../components/pages/programate/Event/BannerEvent";
import CarouselStore from "../../components/modules/CarouselStore";
import InfoEvent from "../../components/pages/programate/Event/InfoEvent";
import PlayList from "../../components/pages/visitanos/Rooms/singleRooms/PlayList";
import FeaturedContent from "../../components/pages/inicio/FeaturedContent";
import PhotoGallery from "../../components/pages/visitanos/Rooms/singleRooms/photo-gallery/PhotoGallery";
import Programate from "../../components/pages/inicio/Programate";
import ExploraRedes from "../../components/pages/inicio/ExploraRedes";

// Data
import { programate as imagesProgramate } from "../../data/images";
import { getPageBySlug } from "../../lib/api";

const Event = ({ event }) => {
  // Instanciamos useRouter para obtener el id del evento que viene por la url
  const router = useRouter();

  if (router.isFallback) {
    return (
      <Layout>
        <div style={{ flexGrow: 1 }}>CARGANDO...</div>
      </Layout>
    );
  }
  const {
    title,
    location,
    img,
    date,
    description,
    time,
    quotas,
    modality,
    price,
    content,
    extra_content,
  } = event;
  console.log(extra_content);

  return (
    <Layout withProductsShop withNetworkig shopColor="#f9d51d">
      <BannerEvent
        dataEvent={{
          title,
          location,
          img,
          date,
          description,
          time,
          quotas,
          modality,
          price,
        }}
      />
      <InfoEvent
        descriptionExtend={{
          content,
          extra_content,
        }}
      />
      {/* <Programate />  */}
      {/* <PhotoGallery images={imagesProgramate} /> */}
      {/* <PlayList playList={rooms.acuario.playList} /> */}
      <FeaturedContent />
    </Layout>
  );
};

// Creando todos los archivos jsx correspondiente al evento 001, 002, etc

export async function getStaticPaths() {
  const paths = [];

  return {
    paths,
    fallback: true,
  };
}

// Accediendo a los datos de los archivos del evento 001, 002, etc

export async function getStaticProps({ params }) {
  try {
    const res = await getPageBySlug(params.id, "title,content,metadata", {});

    return {
      props: {
        event: { title: res.title, content: res.content, ...res.metadata },
      },
    };
  } catch (error) {
    console.log(error);
  }
}

export default Event;
