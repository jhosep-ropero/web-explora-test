import { Layout } from "../../../components/layouts/Layout";
import { NavbarMenu } from "../../../components/templates/navbar/NavbarMenu";
import BannerCourses from "../../../components/pages/aprende/Banner/BannerCourses";
import OurCourses from "../../../components/pages/aprende/OurCourses/OurCourses";

export default function index() {
  return (
    <>
      <Layout>
        <BannerCourses title={"NUESTROS CURSOS"} />
        <OurCourses />
      </Layout>
    </>
  );
}
