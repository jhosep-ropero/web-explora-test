import { useRouter } from "next/router";
import React from "react";
import { Layout } from "../../../components/layouts/Layout";
// import { CoursePucharseFlow } from "../../../components/templates/CoursePucharseFlow/CoursePucharseFlow";
import { ActionBar } from "../../../components/pages/compracursos/ActionBar/ActionBar";
import { CourseMainSection } from "../../../components/pages/compracursos/CourseMainSection.jsx/CourseMainSection";
import { NavbarMenu } from "../../../components/templates/navbar/NavbarMenu";
import { allCourses } from "../../../data/courses";
import { CourseDescription } from "../../../components/pages/compracursos/CourseDescription.jsx/CourseDescription";

const Cursos = () => {
  const router = useRouter();
  const { id } = router.query;
  return (
    <Layout withCourseShop={true} navbarTrasparen={true}>
      <ActionBar />
      <CourseMainSection
        img={allCourses[12].img}
        title={allCourses[12].title}
        shortDescription={allCourses[12].shortDescription}
        price={allCourses[12].price}
        audience={allCourses[12].audience}
        dateCourse={allCourses[12].dateCourse}
        modality={allCourses[12].modality}
        hourCourse={allCourses[12].hourCourse}
        quota={allCourses[12].hourCourse}
        teacher={allCourses[12].teacher}
      />
      <CourseDescription
        largeDescriptionFirstPart={allCourses[12].largeDescriptionFirstPart}
        largeDescriptionSecondPart={allCourses[12].largeDescriptionSecondPart}
        requirements={allCourses[12].requirements}
        modality={allCourses[12].modality}
      />
    </Layout>
  );
};

export default Cursos;
