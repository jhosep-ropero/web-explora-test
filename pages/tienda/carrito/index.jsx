import React from "react";
import { Layout } from "../../../components/layouts/Layout";
import { BillingPage } from "../../../components/pages/carrito/BillingPage/BillingPage";

const index = () => { 
  return (
    <Layout withCourseShop={true}>
      <BillingPage />
    </Layout>
  );
};

export default index;
