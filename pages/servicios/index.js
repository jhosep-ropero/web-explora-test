// Components
import { Layout } from "../../components/layouts/Layout";
import { NavbarMenu } from "../../components/templates/navbar/NavbarMenu";
import LayoutCardsWidthTabs from "../../components/pages/servicios/LayoutCardsWidthTabs/LayoutCardsWidthTabs";
import CarouselStore from "../../components/modules/CarouselStore";
import Contact from "../../components/pages/servicios/Contact/Contact";
import ExploraRedes from "../../components/pages/inicio/ExploraRedes";

const index = () => {
  return (
    <Layout withCourseShop withNetworkig shopColor="#F9D51D">
      <LayoutCardsWidthTabs />
      <Contact />
    </Layout>
  );
};

export default index;
