import { useRouter } from "next/router";
import React from "react";
import { getPageBySlug } from "../../lib/api";

const TestEvent = ({ event }) => {
  const router = useRouter();
  console.log(event);

  if (router.isFallback) {
    return <div>CARGANDO...</div>;
  }
  return <div>Works </div>;
};

export async function getStaticPaths() {
  const paths = [];

  return {
    paths,
    fallback: true,
  };
}

export async function getStaticProps({ params }) {
  const res = await getPageBySlug(params.id, "title,metadata", {});

  return {
    props: {
      event: res,
    },
  };
}

export default TestEvent;
