import React from "react";
import { LayoutPlanetary } from "../../../components/layouts/LayoutPlanetary";
import { ProgramYourself } from "../../../components/pages/planetarioprogramate/ProgramYourself/ProgramYourself";

const index = () => {
  return (
    <div className="planetary">
      <LayoutPlanetary
        withNavbar={true}
        navbarTrasparent={true}
        withShop={true}
      >
        <ProgramYourself />
      </LayoutPlanetary>
    </div>
  );
};

export default index;
