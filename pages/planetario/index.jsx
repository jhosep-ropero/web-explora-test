import React from "react";
import { LayoutPlanetary } from "../../components/layouts/LayoutPlanetary";
import AboutPlanetary from "../../components/pages/planetario/AboutPlanetary";
import Exhibitions from "../../components/pages/planetario/Exhibitions";
import MainSliderPlanetary from "../../components/pages/planetario/MainSliderPlanetary";
import OurShopCarouselPlanetary from "../../components/pages/planetario/OurShopCarouselPlanetary";
import ShowsDomos from "../../components/pages/planetario/ShowsDomos";
import { NavbarMenuP } from "../../components/templates/navbar/NavbarMenuP";

//Data
import { getPageBySlug } from "../../lib/api";
import { constants } from "../../config/";

const index = ({ data }) => {
  const { featured } = data?.metadata;

  return (
    <div className="planetary">
      <LayoutPlanetary
        withNavbar={true}
        navbarTrasparent={true}
        withShop={true}
      >
        <MainSliderPlanetary featured={featured} />
        <ShowsDomos />
        <Exhibitions />
        <AboutPlanetary />
      </LayoutPlanetary>
    </div>
  );
};

export async function getStaticProps() {
  try {
    const data =
      (await getPageBySlug(constants.planetaryHome, "metadata, title", {})) ||
      undefined;
    return {
      props: { data },
    };
  } catch (error) {
    console.log(error);
  }
}

export default index;
