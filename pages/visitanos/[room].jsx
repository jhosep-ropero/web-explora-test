import { useRouter } from "next/router";
import { Layout } from "../../components/layouts/Layout";
import CarouselStore from "../../components/modules/CarouselStore";
import ExploraRedes from "../../components/pages/inicio/ExploraRedes";
import Description from "../../components/pages/visitanos/Rooms/singleRooms/Description";
import PhotoGallery from "../../components/pages/visitanos/Rooms/singleRooms/photo-gallery/PhotoGallery";
import PlayList from "../../components/pages/visitanos/Rooms/singleRooms/PlayList";
import SliderOne from "../../components/pages/visitanos/Rooms/singleRooms/SliderOne";
import SliderTwo from "../../components/pages/visitanos/Rooms/singleRooms/SliderTwo";
import { NavbarMenu } from "../../components/templates/navbar/NavbarMenu";
import { getPageBySlug } from "../../lib/api";

const Room = ({ room }) => {
  console.log(room);
  const router = useRouter();

  if (router.isFallback) {
    return (
      <Layout>
        <NavbarMenu />
        <div>CARGANDO...</div>
      </Layout>
    );
  }

  return (
    <Layout withCourseShop shopColor="#F9D51D" withNetworkig>
      <SliderOne
        background={room.background.url}
        room={room.title}
        title={room.intro.intro_text_1}
      />
      <SliderTwo
        background={room.intro.fondo_2.url}
        title={room.intro.intro_text_2}
      />
      {room.extras.gallery.length !== 0 && (
        <PhotoGallery images={room.extras.gallery} />
      )}
      {room.extras.playlist.length !== 0 && (
        <PlayList playList={room.extras.playlist} />
      )}

      <Description
        background={room.informacion.ilustracion_de_descripcion.url}
        title={""}
        description={room.informacion.description}
        awards={room.informacion.awards}
      />
    </Layout>
  );
};

// Creando todos los archivos jsx

export async function getStaticPaths() {
  const paths = [];

  return {
    paths,
    fallback: true,
  };
}

// Accediendo a los datos de los archivos

export async function getStaticProps({ params }) {
  try {
    const res = await getPageBySlug(params.room, "title,metadata", {});

    return {
      props: {
        room: { title: res.title, ...res.metadata },
      },
    };
  } catch (error) {
    console.log(error);
  }
}

export default Room;
