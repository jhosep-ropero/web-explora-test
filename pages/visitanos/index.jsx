import PageBar from "../../components/ui/PageBar/PageBar";
import Slider from "react-slick";
import SliderFullWidth from "../../components/modules/SliderFullWidth";
import Exhibitions from "../../components/pages/visitanos/Exhibitions/Exhibitions";
import Explora from "../../components/pages/visitanos/Explora/Explora";
import Information from "../../components/pages/visitanos/Information/Information";
import Rooms from "../../components/pages/visitanos/Rooms/Rooms";
import { Layout } from "../../components/layouts/Layout";
import { getPageBySlug } from "../../lib/api";
import { constants } from "../../config";

const index = ({ data }) => {
  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };
  return (
    <Layout shopColor="#F45858" withCourseShop>
      <div>
        <PageBar title={"VISíTANOS"} color={"#F9D41E"} />
        {/* 
          <Slider {...settings} className="featurevisitus">
            <SliderFullWidth
              minHeightSlider="600px"
              imgBackground={"url(../../assets/slider-acuario03.png)"}
              title={
                <Fragment>
                  El acuario es un <br /> espacio de naturaleza
                  <br /> educativa
                </Fragment>
              }
              textButton="Conoce nuestro acuario"
              linkButton=""
            />
            <SliderFullWidth
              minHeightSlider="600px"
              imgBackground={"url(../../assets/slider-domodig.png)"}
              title={
                <Fragment>
                  Un domo digital <br /> para experiencias
                  <br /> de inmersión
                </Fragment>
              }
              textButton="Conoce nuestro planetario"
              linkButton="/planetario"
            />
          </Slider> */}
        <Information />
      </div>
      <Rooms rooms={data.rooms} />
      <Exhibitions />
      <Explora />
    </Layout>
  );
};

export async function getStaticProps() {
  try {
    const data =
      (await getPageBySlug(
        constants.visitUs,
        "title,metadata.rooms.slug,metadata.rooms.title,metadata.rooms.metadata.background",
        {}
      )) || undefined;
    return {
      props: { data: { ...data.metadata } },
    };
  } catch (error) {
    console.log(error);
  }
}

export default index;
