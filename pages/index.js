import { useDispatch, useSelector } from "react-redux";
import { fail, succes } from "../redux/actions/example";
import Programate from "../components/pages/inicio/Programate";
import MainSliderExplora from "../components/pages/inicio/MainSliderExplora";
import FeaturedContent from "../components/pages/inicio/FeaturedContent";
import ContractUs from "../components/pages/inicio/ContractUs";
import ExploraRedes from "../components/pages/inicio/ExploraRedes";
import { NavbarMenu } from "../components/templates/navbar/NavbarMenu";
import CarouselStore from "../components/modules/CarouselStore";
import { Layout } from "../components/layouts/Layout";

export default function Home() {
  const { error } = useSelector((state) => state.examples);
  const dispatch = useDispatch();

  const handleExample = () => {
    dispatch(fail());
  };

  return (
    <Layout withNetworkig withCourseShop shopColor="#F9D51D" navbarTrasparent>
      <main>
        <MainSliderExplora />
        <Programate />
        <FeaturedContent />
        <ContractUs />
      </main>
    </Layout>
  );
}

// export const getServerSideProps = wrapper.getServerSideProps(async ( {req,store} )=> {
//   await store.dispatch(succes(req))
// })
