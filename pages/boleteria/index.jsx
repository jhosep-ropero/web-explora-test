import { Layout } from "../../components/layouts/Layout";
import { NavbarMenu } from "../../components/templates/navbar/NavbarMenu";
import { SubheaderOnlyTitle } from "../../components/ui/SubheaderOnlyTitle";
import TicketsList from "../../components/modules/TicketsList";
import CarouselStore from "../../components/modules/CarouselStore";
Layout;

const index = () => {
  return (
    <Layout navbarTrasparent>
      <SubheaderOnlyTitle
        Title="Boletería"
        Subtitle={"Conoce todos los planes"}
        ColorTitle={"white"}
        ColorSubtitle={"white"}
        ColorLine={"#F45858"}
        Background={"url('/assets/img-2.jpg')"}
        PaddingBot={"100px"}
      />
      <div style={{ marginTop: `-150px` }}>
        <TicketsList />
      </div>
      <div className="my-5 py-5">
        <CarouselStore colorSeparator={"#F9D51D"} />
      </div>
    </Layout>
  );
};

export default index;
