import React, { useState, useEffect } from "react";
import { BannerShop } from "../../../components/pages/compraboleta/BannerShop/BannerShop";
import { ShopFlow } from "../../../components/pages/compraboleta/ShopFlow/ShopFlow";
import { Layout } from "../../../components/layouts/Layout";
import banner from "../../../public/assets/taquilla/banner-compra.png";
import bannerMobile from "../../../public/assets/taquilla/banner-movil-compra.png";
import back from "../../../public/assets/taquilla/icon-back-ticket.png";

import CarouselStore from "../../../components/modules/CarouselStore";
import Separator from "../../../components/ui/Separator/Separator";
import Image from "next/image";
import { useSelector, useDispatch } from "react-redux";
import { setTicketState } from "../../../redux/actions/tickets";
import { useRouter } from "next/router";

const Boleta = () => {
  const router = useRouter();
  const { id } = router.query;
  const step = useSelector((state) => state.tickets.ticketState);
  const [buttonTitle, setButtonTitle] = useState("CONTINUAR CON LA COMPRA");
  const dispatch = useDispatch();

  const nextStep = () => {
    step < 3 && dispatch(setTicketState(step + 1));
  };

  const backStep = () => {
    step == 0 && (window.location.href = "/boleteria");
    step > 0 && dispatch(setTicketState(step - 1));
  };

  useEffect(() => {
    step == 0 && setButtonTitle("CONTINUAR CON LA COMPRA");
    step == 1 && setButtonTitle("RESUMEN DE LA COMPRA");
    step == 2 && setButtonTitle("FINALIZAR COMPRA");
  }, [step]);

  return (
    <Layout>
      <div className="overlay-box">
        <BannerShop img={banner} imgMobile={bannerMobile} />
        <div className="overlay-button" onClick={() => backStep()}>
          <Image src={back} width={45} height={45} alt="Back" />
        </div>
      </div>
      <div className="overlay-item">
        <ShopFlow />
      </div>
      <div style={{ marginTop: "192px" }}>
        <Separator color={"#f9d51d"} />
        <CarouselStore />
      </div>
    </Layout>
  );
};

export default Boleta;
