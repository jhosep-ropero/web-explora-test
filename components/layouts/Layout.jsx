import Head from "next/head";
import CarouselStore from "../modules/CarouselStore";
import ExploraRedes from "../pages/inicio/ExploraRedes";
import SidebarAccesibility from "../templates/accesiibility/SidebarAccesibility";
import Footer from "../templates/footer/Footer";
import { NavbarMenu } from "../templates/navbar/NavbarMenu";
import * as styles from "./Layout.module.scss";

const fontSora =
  "https://fonts.googleapis.com/css2?family=Sora:wght@100;200;300;400;500;600;700;800;900&display=swap";
export const Layout = ({
  children,
  meta, // meta tags
  navbarTrasparent = false, // navbar transparent
  withContent = false, // Con contenidos recomendados
  withCourseShop = false, // Con tienda de cursos
  withProductsShop = false, // Con tienda de productos
  withNetworkig = false, // Con redes sociales
  shopColor = "F9D51D",
}) => {
  return (
    <>
      <Head>
        <title>{meta?.title || "Parque Explora"}</title>
        <meta
          name="description"
          content={
            meta?.description ||
            "Recursos para explorar, conectarse y programarse."
          }
        />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="theme-color" content="#ff7777" />
        <meta
          name="image"
          content="https://parqueexplora.com/static/images/logo.png"
        />
        <link rel="preconnect" href="https://fonts.googleapis.com"></link>

        <link rel="preconnect" href="https://fonts.gstatic.com"></link>
        <link href={fontSora} rel="stylesheet"></link>
      </Head>
      <NavbarMenu home={navbarTrasparent}></NavbarMenu>
      <SidebarAccesibility />
      <main
        className={`${styles.mainContent} ${
          !navbarTrasparent ? styles.hasNavbar : ""
        }`}
      >
        {children}
        {withCourseShop && <CarouselStore colorSeparator={shopColor} />}
        {withNetworkig && <ExploraRedes />}

        <Footer />
      </main>
    </>
  );
};
