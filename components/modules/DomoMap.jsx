import { Chairs } from "../../data/chairs.js";

import Chair from "./Chair";

const DomoMap = () => {
  const len = Chairs.ColumnaRelativa.length;
  const rowsLen = len / 21;
  let rows = [];
  let cols = [];
  let names = [];
  let types = [];
  let chairs = [];
  for (let i = 0; i < rowsLen; i++) {
    rows.push(Chairs.ColumnaRelativa.slice(i * 21, (i + 1) * 21));
    cols.push(Chairs.ColumnaTotal.slice(i * 21, (i + 1) * 21));
    names.push(Chairs.FilaTotal.slice(i * 21, (i + 1) * 21));
    types.push(Chairs.TipoSilla.slice(i * 21, (i + 1) * 21));
    chairs.push(Chairs.TipoZona.slice(i * 21, (i + 1) * 21));
  }

  return (
    <div>
      <div className="map">
        {rows.map((row, i) => (
          <div className="row" key={i}>
            <div className={`d-flex align-items-center pe-3 chairIdBox`}>
              {names[i][0]}
            </div>
            {row.map((chair, j) =>
              chair != 0 ? (
                <div className="chair" key={j}>
                  <Chair
                    state={0}
                    number={rows[i][j]}
                    name={names[i][j]}
                    type={types[i][j]}
                    chair={chairs[i][j]}
                    col={cols[i][j]}
                    colNumber={i}
                    rowNumber={j}
                    isDynamic={true}
                  />
                </div>
              ) : null
            )}
          </div>
        ))}
      </div>
    </div>
  );
};

export default DomoMap;
