import Image from "next/image";
import { useSelector } from "react-redux";

export default function CardEvents({ img, price, title, size, color }) {

  //usar reducer de accesibilidad
  const { hideImages } = useSelector((state) => state.accesibility)

  return (
    <div className="position-relative">
      {
        !hideImages &&
          <Image
            src={`/assets/${img}.png`}
            className="card-img-top"
            alt="..."
            width="340px"
            height="257px"
          />

      }

      <div className="card-body">
        <h5 className="card-title text-uppercase pt-2">{title}</h5>
        <p className="card-text fw-bold work-sans">{price}</p>
        <div style={{ height: 35 }}>
          {size && (
            <div className="d-flex flex-sm-column flex-xl-row">
              <span className="text-dark">Tallas disponibles:</span>
              <span className="text-dark ms-4 ms-sm-0 ms-xl-4 ps-2 ps-sm-0 ps-xl-2">
                {size.map((item, i) => (
                  <b key={i}>
                    {item} {i !== size.length - 1 && <i> - </i>}
                  </b>
                ))}
              </span>
            </div>
          )}
        </div>
        <div style={{ height: 35 }}>
          {color && (
            <div className="d-flex flex-sm-column flex-xl-row">
              <span className="text-dark">Colores disponibles:</span>
              <span className="text-dark ms-3 ms-sm-0 ms-xl-3">
                {color.map((item, i) => (
                  <b
                    key={i}
                    style={{
                      backgroundColor: `${item}`,
                      padding: "0 1.1rem 0 0",
                      margin: "0 0.3rem 0 0",
                      borderRadius: "2rem",
                    }}
                  >
                    {" "}
                  </b>
                ))}
              </span>
            </div>
          )}
        </div>
        <button className="btn d-block fw-bold py-3 mx-auto my-4">
          VER MÁS
        </button>
        <p className="text-dark text-center text-uppercase m-4">
          Quiero inscribirme
        </p>
      </div>
    </div>
  );
}
