import Image from "next/image";

export default function CardEventsPlanetary({ img, price, title, size, color }) {
  return (
    <div className="position-relative">
      <Image
        src={`/assets/${img}.png`}
        className="card-img-top"
        alt="..."
        width="340px"
        height="257px"
      />

      <div className="card-body">
        <span className="category fw-light">Planetario</span>
        <h5 className="card-title text-white fw-light m-0">{title}</h5>
        <div className="card-title text-white m-0">
          {size && (
            <div className="d-flex flex-sm-column flex-xl-row">
              <span className="">Tallas disponibles:</span>
              <span className="ms-4 ms-sm-0 ms-xl-4 ps-2 ps-sm-0 ps-xl-2">
                {size.map((item, i) => (
                  <b key={i}>
                    {item} {i !== size.length - 1 && <i> - </i>}
                  </b>
                ))}
              </span>
            </div>
          )}
          {color && (
            <div className="d-flex flex-sm-column flex-xl-row">
              <span className="">Colores disponibles:</span>
              <span className="ms-3 ms-sm-0 ms-xl-3">
                {color.map((item, i) => (
                  <b
                    key={i}
                    style={{
                      backgroundColor: `${item}`,
                      padding: "0 1.1rem 0 0",
                      margin: "0 0.3rem 0 0",
                      borderRadius: "2rem",
                    }}
                  >
                    {" "}
                  </b>
                ))}
              </span>
            </div>
          )}
        </div>
        <p className="fs-4 fw-bold">{price}</p>
        <button className="btn btn-outline-light d-block fw-bold py-2 mx-auto my-3">
          Comprar
        </button>
      </div>
    </div>
  );
}
