
import Link from "next/link";

export default function InformationBar({
    iconCol1,
    titleCol1,
    texxtCol1,
    iconCol2,
    titleCol2,
    texxtCol2,
    iconCol3,
    titleCol3,
    texxtCol3,
    moreStyle,
    linkAbout,
    linkSchedule,
    linkLocation,
}) {
  return (
    <div id="Infobar" className={`d-lg-flex justify-content-between ${moreStyle}`}>
        <Link href={`${linkAbout}`} passHref>
            <a href="" className="col-">
                <div>
                    <i className={`icon-explora ${iconCol1}`}></i>
                </div>
                <div>
                    <h6>{titleCol1}</h6>
                    <p>{texxtCol1}</p>
                </div>
            </a>
        </Link>
        <Link href={`${linkSchedule}`} passHref>
            <a href="" className="col-">
                <div>
                    <i className={`icon-explora ${iconCol2}`}></i>
                </div>
                <div>
                    <h6>{titleCol2}</h6>
                    <p>{texxtCol2}</p>
                </div>
            </a>
        </Link>
        <Link href={`${linkLocation}`} passHref>
            <a href="" className="col-">
                <div>
                    <i className={`icon-explora ${iconCol3}`}></i>
                </div>
                <div>
                    <h6>{titleCol3}</h6>
                    <p>{texxtCol3}</p>
                </div>
            </a>
        </Link>
    </div>
  );
}
