import Hbar from "../ui/Hbar/Hbar";
import Separator from "../ui/Separator/Separator";
import Slider from "react-slick";
import { programate } from "../../data/programate";
import CardFeaturedContent from "./CardFeaturedContent";
import '../../styles/modules/Home.module.scss'
import ButtonBar from "../ui/ButtonBar/ButtonBar";


export default function CarouselFeaturedContent() {
  const settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 4,
    initialSlide: 0,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2
        }
      },
      {
        breakpoint: 521,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };
    return (
        <div className="section-home02 section-carousel" style={{backgroundColor: '#F2F2F7'}}>            
            <div className="container px-4 py-5">
                <Hbar color={'#F9D51D'} title='CONTENIDO DESTACADO'/>
            </div>
            <Slider {...settings} className="container" style={{paddingBottom: '200px'}}>
              {programate.map(ev =>(
                <CardFeaturedContent 
                  key={ev.id} 
                  img={ev.img} 
                  date={ev.date} 
                  category={ev.category} 
                  title={ev.title} 
                  dateExtend={ev.dateExtend}
                />
              ))}
            </Slider>            
        </div>
    )
}
