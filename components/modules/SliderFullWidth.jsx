import React, { Component } from "react";
import ReactPlayer from 'react-player'
import Link from 'next/link';
import { useSelector } from "react-redux";
import PropTypes from "prop-types";

export default function SliderFullWidth({
    minHeightSlider,
    imgBackground,
    videoBackground,
    title,
    subtitle,
    description,
    textButton,
    linkButton,
  }){

  //usar reducer de accesibilidad
  const { hideImages, stopAnimations } = useSelector((state) => state.accesibility)

  return (
    <div className={`bg-cover bg-dark text-white position-relative border-0`} style={{background: !hideImages ? `${imgBackground}`: '#000', minHeight: `${minHeightSlider}`}}>
        
        <div className={`d-flex align-items-center position-relative`} style={{minHeight: `${minHeightSlider}`}}>
          <div className={`overlay position-absolute top-0 start-0 w-100 p-5`} style={{opacity:'0.5', minHeight: `${minHeightSlider}`}}>
            {
              !hideImages &&
              <ReactPlayer className={`bg-video`}
                url={`${videoBackground}`}
                playing={!stopAnimations}
                width={'auto'}
                height={'auto'}
                loop={true}
              />
            }
          </div>
          <div className="container px-lg-0 py-5" style={{zIndex:'1'}}>
            <div className="col-lg-6 py-5">
              <h2 className={`slider-title fs-1 fw-bold`}>{title}</h2>
              <h4 className={`slider-subtitle`}>{subtitle}</h4>
              <p className={`slider-description`}>{description}</p>
              <Link href={`${linkButton}`} passHref>
                <a href="" className={`btn btn-warning text-uppercase fs-5 py-3 px-4`}>{textButton} <span className={`ms-3 icon-explora icon-arrow-right`}></span></a>
              </Link>
              <hr className={`m-0 py-1 col-2 opacity-100`} />
            </div>
          </div>
        </div>
    </div>
  );
}

SliderFullWidth.protoTypes = {
  minHeightSlider: PropTypes.string,
  imgBackground: PropTypes.string,
  videoBackground: PropTypes.string,
  title: PropTypes.string,
  subtitle: PropTypes.string,
  description: PropTypes.string,
  textButton: PropTypes.string,
  linkButton: PropTypes.string,
};
