import Link from "next/link";
import React, { Fragment } from "react";
import { tickets } from "../../data/tickets";
import styles from "../../styles/modules/TickesList.module.scss";
//import Image from 'next/image';

const TicketsList = ({
  id,
  img,
  icon,
  title,
  subtitle,
  currency,
  price,
  description,
  button,
}) => {
  return (
    <div id="Tickes" className="container">
      {tickets.map((item) => (
        <div
          className="row card mb-3 bg-white border shadow-sm mb-5"
          key={item.id}
        >
          <div className="row g-0">
            <div
              className="col-md-3 p-0 image position-relative"
              style={{ backgroundImage: `url('${item.img}')` }}
            >
              <div className="d-flex align-items-center justify-content-center w-100 h-100 fs-1">
                <div className="overlay position-absolute top-0 start-0 w-100 h-100 "></div>
                <i className={`icon-explora text-white ${item.icon}`}></i>
              </div>
            </div>
            <div className="card-body col-md-9 p-5">
              <h3 className="fw-bold mb-0">{item.title}</h3>
              <h4 className="mb-0">{item.subtitle}</h4>
              <div className="text-primary fs-3 fw-bold">
                {item.currency} {item.price}
              </div>
              <div className="mt-3">
                <span>Incluye:</span>
                <br />
                {item.description}
              </div>
              <hr />
              <Link href="/boleteria/boleta/0" passHref>
                <a
                  href=""
                  className="btn btn-info text-uppercase text-white fs-6 py-2 px-4"
                >
                  {item.button}
                </a>
              </Link>
            </div>
          </div>
        </div>
      ))}
    </div>
  );
};

export default TicketsList;
