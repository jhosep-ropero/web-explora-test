import Image from 'next/image';

export default function CardFeaturedContent({img, date, category, title, dateExtend}) {
  return (
    <div className='position-relative' style={{backgroundColor: '#fff'}}>
      <Image
        src={`/assets/${img}.png`}
        className="card-img-top"
        alt="..."
        width="340px"
        height="257px"
      />
      <div className='card-date d-inline-block p-2'>
        <p className='mb-0 fw-bold'>{date.day}</p>        
      </div>
      <div className="card-body">        
        <h5 className="card-title text-uppercase pt-2">{title}</h5>
        <p className="card-text font-weight-light work-sans">
          {dateExtend}
        </p>
        <button className="btn d-block fw-bold py-3 mx-auto my-4">VER MÁS</button>
        <p className='text-dark text-center text-uppercase m-4'>Quiero inscribirme</p>
      </div>
    </div>
  );
}
