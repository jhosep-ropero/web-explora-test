import styles from "../../styles/modules/DomoMap.module.scss";
import React, { useState } from "react";

const Chair = ({
  state,
  name,
  number,
  type,
  chair,
  col,
  colNumber,
  rowNumber,
  isDynamic,
}) => {
  const [chairState, setChairState] = useState(state);

  const handleClick = () => {
    if (isDynamic) {
      switch (chairState) {
        case 0:
          setChairState(1);
          break;
        case 1:
          setChairState(0);
          break;
        default:
          break;
      }
    }
  };

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="20.141"
      height="31.644"
      onClick={handleClick}
      viewBox="0 0 28.141 39.644"
    >
      <g
        id="Grupo_6644"
        data-name="Grupo 6644"
        transform="translate(-15571.3 -1855.901)"
        className={isDynamic && styles.hover}
      >
        <path
          id="Trazado_5245"
          data-name="Trazado 5245"
          d="M858.162,301.9l-17.856-.205a3.6,3.6,0,0,1-3.369-3.717l-.076-18.323a1.451,1.451,0,0,1,1.418-1.511l22.267.254a1.464,1.464,0,0,1,1.394,1.544l-.354,18.32a3.563,3.563,0,0,1-3.425,3.639"
          transform="translate(14735.827 1593.645)"
          fill={
            chairState == 0 ? "#ccc" : chairState == 1 ? "#F9D51D" : "#F45858"
          }
        />
        <path
          id="Trazado_5246"
          data-name="Trazado 5246"
          d="M859.227,265.606l-21.668-.247a1.756,1.756,0,0,1-1.813-1.55l-1.3-11.714a1.681,1.681,0,0,1,1.822-1.639l24.516.278a1.7,1.7,0,0,1,1.8,1.68l-1.52,11.684a1.739,1.739,0,0,1-1.836,1.508"
          transform="translate(14736.854 1605.446)"
          fill={
            chairState == 0 ? "#ccc" : chairState == 1 ? "#F9D51D" : "#F45858"
          }
        />
      </g>
    </svg>
  );
};

export default Chair;
