import Slider from "react-slick";
import CardStore from "./CardStore";
import ButtonBar from "../ui/ButtonBar/ButtonBar";
import Hbar from "../ui/Hbar/Hbar";
import Separator from "../ui/Separator/Separator";
import { store } from "../../data/store";

export default function CarouselStore({colorSeparator}) {
    const settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 4,
        initialSlide: 0,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
              dots: true
            }
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              initialSlide: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
      };
    return (
        <main className="section-home04 section-carousel">
            <Separator color={colorSeparator}/>
            <div className="container px-4 py-5">
                <Hbar color={'#F9D51D'} title='Visita nuestra tienda'/>
            </div>
            <Slider {...settings} className="container">
              {store.map(st =>(
                <CardStore 
                  key={st.id} 
                  img={st.img} 
                  title={st.title} 
                  price={st.price}
                  color={st.color}
                  size={st.size}
                />
              ))}
            </Slider>
            <div className="col-12 col-md-6 mx-auto pb-5 text-center">
              <ButtonBar text='Visita nuestra tienda' center={true} colorBtn='#F9D51D' colorTxt='#44444A' colorHr='#F45858'/>
            </div>
        </main>
    )
}
