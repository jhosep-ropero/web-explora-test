import styles from "./Separator.module.scss";

export default function Separator({color}) {
    return (
        <div className={styles.separator} style={{background: `${color}`}}>
        </div>
    )
}
