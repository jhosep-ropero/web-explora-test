import Image from 'next/image';
import React from 'react';
import styles from '../../templates/accesiibility/Accesibility.module.scss'

export default function BtnAccesibility({title, icon, active, action}) {
  return (
    <div className={`${styles.cardAccesibility} text-center ${active && styles.activeAccesibility}`} >
        <button className="bg-transparent border-0 mb-3" onClick={action}>
            <Image src={`/assets/accesibility/${icon}.png`} alt="icon" height={'52px'} width={'52px'} />
        </button>
        <a className="text-dark fw-light is-size-9" onClick={action}>{title}</a>
    </div>
  );
}
