import React, { useState } from "react";
import "react-modern-calendar-datepicker/lib/DatePicker.css";
import { Calendar } from "react-modern-calendar-datepicker";

// Styles
import styles from './DatePickerFilter.module.scss';

export const DatePickerFilter = ({handleChangeCalendar, valueDateSelect}) => {
  const myCustomLocale = {
    // months list by order
    months: [
      'ENE',
      'FEB',
      'MAR',
      'ABR',
      'MAY',
      'JUN',
      'JUL',
      'AGO',
      'SEP',
      'OCT',
      'NOV',
      'DIC',
      ],
  
    // week days by order
    weekDays: [
      {
        name: 'Domingo', 
        short: 'D', 
        isWeekend: true,
      },
      {
        name: 'Lunes',
        short: 'L',
      },
      {
        name: 'Martes',
        short: 'M',
      },
      {
        name: 'Miércoles',
        short: 'W',
      },
      {
        name: 'Jueves',
        short: 'J',
      },
      {
        name: 'Viernes',
        short: 'V',
      },
      {
        name: 'Sábado',
        short: 'S',
        isWeekend: true,
      },
    ],
  
    // just play around with this number between 0 and 6
    weekStartingIndex: 0,
  
    // return a { year: number, month: number, day: number } object
    getToday(gregorainTodayObject) {
      return gregorainTodayObject;
    },
  
    // return a native JavaScript date here
    toNativeDate(date) {
      return new Date(date.year, date.month - 1, date.day);
    },
  
    // return a number for date's month length
    getMonthLength(date) {
      return new Date(date.year, date.month, 0).getDate();
    },
  
    // return a transformed digit to your locale
    transformDigit(digit) {
      return digit;
    },
  
    // texts in the date picker
    nextMonth: 'Next Month',
    previousMonth: 'Previous Month',
    openMonthSelector: 'Open Month Selector',
    openYearSelector: 'Open Year Selector',
    closeMonthSelector: 'Close Month Selector',
    closeYearSelector: 'Close Year Selector',
    defaultPlaceholder: 'Select...',
  
    // for input range value
    from: 'from',
    to: 'to',
  
  
    // used for input value when multi dates are selected
    digitSeparator: ',',
  
    // if your provide -2 for example, year will be 2 digited
    yearLetterSkip: 0,
  
    // is your language rtl or ltr?
    isRtl: false,
  }

  return (
    <div className={`d-flex justify-content-center ${styles.datePickerFilter_container}`}>
      <Calendar        
        value={valueDateSelect}
        onChange={handleChangeCalendar}        
        locale={myCustomLocale}
        shouldHighlightWeekends
        colorPrimary="#f45858"
        colorPrimaryLight="#f2f2f7"
      />
    </div>
    // <Calendar
    //   value={selectedDayRange}
    //   onChange={setSelectedDayRange}
      
    // />
  );
};
