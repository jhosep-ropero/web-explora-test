import React from "react";
import { useDispatch } from "react-redux";
import { setTicketProyection } from "../../../redux/actions/tickets";
import styles from "./Radiobutton.module.scss";

const Radiobuttons = ({ id, hour, title, category }) => {

  const dispatch = useDispatch()

  const onchange = (proyection) => {
    // console.log(proyection);
    dispatch(setTicketProyection(proyection))
  }

  return (
    <div className="d-flex align-items-center py-lg-3 py-4">
      <label htmlFor={`myRadio${id}`} className={`${styles.radio} `} onClick={() => onchange(`${hour}. ${title} * ${category}`)}>
        <input
          type="radio"
          className={styles.radioInput}
          name="myRadioField"
          id={`myRadio${id}`}
        />
        <div className={styles.radioRadio}></div>
      </label>
      <p className="m-0 p-0 is-size-8">{`${hour}. ${title} * ${category}`}</p>
    </div>
  );
};

export default Radiobuttons;
