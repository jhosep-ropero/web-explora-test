import Link from 'next/link';
import Image from 'next/image';

// Styles
import styles from './ButtonTransparent.module.scss';

const ButtonTransparent = ({text, logo}) => {
    return (
        <div className={styles.buttonTransparent}>
            <Link href="#" passHref>
                <a href="">
                    <div className={styles.detailsButton}>
                        <Image src={logo} height={38} width={44} alt={`Logo ${logo}`} />
                        <p className='is-bold is-size-6'>{text}</p>
                    </div>                    
                </a>
            </Link>
        </div>
    );
}
 
export default ButtonTransparent;