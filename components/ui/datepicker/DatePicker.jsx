import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { setTicketDate } from "../../../redux/actions/tickets";

export const DatePicker = () => {
  const dispatch = useDispatch()
  const currentDate = new Date();
  const month = currentDate.getMonth();
  const year = currentDate.getFullYear();

  const [id, setId] = useState("day1");
  const [isSelected, setIsSelected] = useState(false);
  const [date, setDate] = useState();
  const [monthNumber, setMonthNumber] = useState(month);
  const [currentYear, setCurrentYear] = useState(year);

  const months = [
    "ENERO",
    "FEBRERO",
    "MARZO",
    "ABRIL",
    "MAYO",
    "JUNIO",
    "JULIO",
    "AGOSTO",
    "SEPTIEMBRE",
    "OCTUBRE",
    "NOVIEMBRE",
    "DICIEMBRE",
  ];
  let days = [];
  //Si el año es bisiesto
  const isLeap = () => {
    return (
      (currentYear % 100 !== 0 && currentYear % 4 === 0) ||
      currentYear % 400 === 0
    );
  };
  // Dia que comienza el mes
  const startDay = () => {
    let start = new Date(`${currentYear}-${monthNumber + 1}-01`);
    return start.getDay();
  };
  // Cantidad de dias
  const getTotalDays = (month) => {
    if (
      month == 0 ||
      month == 2 ||
      month == 4 ||
      month == 6 ||
      month == 7 ||
      month == 9 ||
      month == 11
    ) {
      return 31;
    } else if (month == 3 || month == 5 || month == 8 || month == 10) {
      return 30;
    } else {
      return isLeap() ? 29 : 28;
    }
  };
  // Completar Arreglo de dias
  const writeMonth = (month) => {
    let prevDays = getTotalDays(monthNumber - 1 == -1 ? 11 : monthNumber - 1);
    let lastDays = prevDays;
    for (let i = startDay(); i > 0; i--) {
      lastDays = prevDays - (i - 1);
      days.push({ id: `last-${lastDays}`, day: lastDays, class: "prevDate", disable: true });
    }

    for (let i = 1; i <= getTotalDays(month); i++) {
      days.push({ id: `day${i}`, day: i, class: "", disable: false });
    }
    // for (let i = 1; i <= getTotalDays(month); i++) {
    //   days.push(i);
    // }
  };

  const lastMonth = () => {
    resetStyles();
    setDate(null);
    if (monthNumber <= 0) {
      setMonthNumber(11);
      setCurrentYear(currentYear - 1);
    } else {
      monthNumber >= 0 && setMonthNumber(monthNumber - 1);
    }
  };

  const nextMonth = () => {
    resetStyles();
    setDate(null);
    if (monthNumber >= 11) {
      setMonthNumber(0);
      setCurrentYear(currentYear + 1);
    } else {
      setMonthNumber(monthNumber + 1);
    }
  };

  const setNewDate = (event) => {
    let nowId = event.target.id;
    let elementOld = document.getElementById(id);
    let elementNew = document.getElementById(nowId);
    if (nowId !== id) {
      elementOld.className = "";
      elementNew.className = " today";
      setId(nowId);
      let dt = new Date(`${currentYear}-${monthNumber + 1}-${nowId.replace("day", "")}`)
      dispatch(setTicketDate(`${dt}`))

      setDate(
        new Date(`${currentYear}-${monthNumber + 1}-${nowId.replace("day", "")}`)
      );
    } else {
      elementOld.className = "";
      setDate(null);
    }
    !isSelected && setIsSelected(true);
  };

  writeMonth(monthNumber);

  const resetStyles = () => {
    let elementOld = document.getElementById(id);
    elementOld.className = "";
  };

  return (
    <div>
      <div className="calendar">
        <div className="month">
          <div style={{ cursor: "pointer" }} onClick={lastMonth}>
            {"<"}
          </div>
          <div className="date">
            <h1>
              {months[monthNumber]} {currentYear}
            </h1>
          </div>
          <div style={{ cursor: "pointer" }} onClick={nextMonth}>
            {">"}
          </div>
        </div>
        <div className="weekdays">
          <div>D</div>
          <div>L</div>
          <div>M</div>
          <div>M</div>
          <div>J</div>
          <div>V</div>
          <div>S</div>
        </div>
        <div className="days">
          {days.map((item, i) => (
            <div
              key={i + 1}
              id={item.id}
              className={item.class}
              onClick={(e) => setNewDate(e)}
            >
              {item.day}
            </div>
          ))}
        </div>
      </div>
      <div
        className={
          ([isSelected ? "show" : "hidden"],
          "d-flex mt-4 mb-3 mx-lg-5 align-items-center")
        }
      >
        <p className={"is-size-8 is-regular"}>
          {date &&
            `${date.getDate()} de ${
              months[date.getMonth()]
            } - Horario de atención:  10:00 am a 8:00 pm`}
          </p>
      </div>
    </div>
  );
};
