// Styles
import styles from './SeparatorFullWidth.module.scss';

const SeparatorFullWidth = ({color}) => {
    return (
        <div className={styles.separatorFullWidth} style={{background: `${color}`}} />
    );
}
 
export default SeparatorFullWidth;