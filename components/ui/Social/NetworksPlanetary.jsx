import React from 'react';

export default function NetworksPlanetary() {

    return (
        <ul className='social'>
            <li>
                <a href={`https://www.facebook.com/PlanetarioMed`} target={`_blank`}>
                        <i className={`icon-explora icon-facebook`}></i>
                    </a>
            </li>
            <li>
                <a href={`https://www.instagram.com/planetariomed`} target={`_blank`}>
                        <i className={`icon-explora icon-instagram`}></i>
                    </a>
            </li>
            <li>
                <a href={`https://twitter.com/planetariomed`} target={`_blank`}>
                        <i className={`icon-explora icon-twitter`}></i>
                    </a>
            </li>
        </ul>
    )
}