import React from 'react';

export default function NetworksExplore() {

    return (
        <ul className='social'>
            <li>
                <a href={`https://www.facebook.com/ParqueExplora/`} target={`_blank`}>
                        <i className={`icon-explora icon-facebook`}></i>
                    </a>
            </li>
            <li>
                <a href={`https://www.instagram.com/parqueexplora/`} target={`_blank`}>
                        <i className={`icon-explora icon-instagram`}></i>
                    </a>
            </li>
            <li>
                <a href={`https://twitter.com/parqueexplora`} target={`_blank`}>
                        <i className={`icon-explora icon-twitter`}></i>
                    </a>
            </li>
            <li>
                <a href={`https://www.youtube.com/c/parqueexplora`} target={`_blank`}>
                        <i className={`icon-explora icon-youtube`}></i>
                    </a>
            </li>
        </ul>
    )
}