import Link from "next/link";
import styles from "./Buttons.module.scss";

export default function Buttons({href = '#', text, colorBtn, colorTxt='#fff'}) {
    return (
        <div className={`${styles.buttonbar}`}>
            <Link href="#" passHref>
                <a href="" style={{backgroundColor:`${colorBtn}`, color: `${colorTxt}`}}>
                    <span className="pe-3 text-uppercase">{text}</span>
                </a>
            </Link>
        </div>
        )
}
