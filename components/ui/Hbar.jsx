import styles from "../../styles/Components.module.scss";

export default function Hbar({ color, title }) {
  return (
    <>
      <div className={styles.hbar}>
        <h2 className="mb-0 mt-5 text-uppercase">{title}</h2>
        <hr style={{ background: `${color}` }} />
      </div>
    </>
  );
}
