import React from 'react';
import styles from "../../styles/Components.module.scss";

export const SubheaderOnlyTitle = ({Title, Subtitle, ColorTitle, ColorSubtitle, ColorLine, Background, PaddingBot}) => {
    return (
        <div id='Subheader' className="pt-5 bg-secondary position-relative" style={{paddingBottom: `${PaddingBot}`, background: `${Background}`}}>
            <div className='overlay position-absolute top-0 start-0 w-100 h-100'></div>
            <div className="container position-relative px-4 py-5">
                <div className={styles.hbar}>
                    <h2 className="mb-0 mt-5 text-uppercase fs-4 fw-bold" style={{color: `${ColorTitle}`}}>{Title}</h2>
                    <hr style={{background: `${ColorLine}`}}/>
                </div>
                <h3 className="text-center py-5 text-white fs-1 fw-bold" style={{color: `${ColorSubtitle}`}}>{Subtitle}</h3>
            </div> 
        </div>
    )
}