import Image from "next/image";
import Link from "next/link";
// Files
import arrow from '../../../public/assets/arrow-left-white.png';
import arrowBlack from '../../../public/assets/icon-arrow-left1.png';
import styles from "./PageBar.module.scss";
import hbar from '../Hbar/Hbar.module.scss'

const PageBarAlt = ({title, color, transparent=false}) => {
    return (     
            <div className={`${transparent ? styles.pagebarTransparentAlt : styles.pagebarRelative} " container "` }>
                <Link href={'/'} passHref>
                    <a href="" className={styles.btnLink}>
                        <Image src={transparent ? arrow : arrowBlack} alt="..." width="42px" height="42px"/>
                    </a>
                </Link>
                <div className={`${hbar.hbar} ' pb-4 ps-4 ps-lg-1 '`}>
                    <h2 className="mb-0 mt-5 text-uppercase">{title}</h2>
                    <hr style={{background: `${color}`}}/>
                </div>
            </div>
    );
}
 
export default PageBarAlt;