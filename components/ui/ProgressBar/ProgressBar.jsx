import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { setTicketState } from "../../../redux/actions/tickets";
import styles from "./ProgressBar.module.scss";

export const ProgressBar = ({ children, step, changeStep }) => {
  // const step = useSelector((state) => state.tickets.ticketState);

  const [btnState, setBtnState] = useState({
    btn2: "btn-secondary",
    btn3: "btn-secondary",
    btn4: "btn-secondary",
  });

  useEffect(() => {
    if (step == 0) {
      setBtnState({
        btn2: "btn-secondary",
        btn3: "btn-secondary",
        btn4: "btn-secondary",
      });
    }
    if (step == 1) {
      setBtnState({
        btn2: "btn-primary",
        btn3: "btn-secondary",
        btn4: "btn-secondary",
      });
    }
    if (step == 2) {
      setBtnState({
        btn2: "btn-primary",
        btn3: "btn-primary",
        btn4: "btn-secondary",
      });
    }
    if (step == 3) {
      setBtnState({
        btn2: "btn-primary",
        btn3: "btn-primary",
        btn4: "btn-primary",
      });
    }
  }, [step]);

  // const buttonStyle =
  //   "text-white btn-progress position-absolute top-0 start-0 translate-middle btn btn-sm rounded-0";

  return (
    <div className={`container ${styles.progressBox}`}>
      <div className="d-xl-flex justify-content-center aling-self-center pt-3">
        <div className="w-progress mt-5">
          <div className="position-relative">
            <div className="progress ">
              <div
                className={`progress-bar step-${step}`}
                role="progressbar"
                aria-valuenow="30"
                aria-valuemin="0"
                aria-valuemax="100"
              ></div>
            </div>
            <div className="d-flex align-items-center is-size-8 bg-white position-absolute top-0 start-0 translate-middle  ">
              <button
                type="button"
                className={`text-white btn-progress btn btn-sm rounded-0 btn-primary`}
                onClick={() => changeStep(0)}
              >
                1
              </button>
              <p
                className={`txt-compra-subtitle px-3 m-0 ${styles.isHiddenMobile}`}
              >
                Informacion
                <br /> del plan
              </p>
            </div>
            <div className="d-flex align-items-center is-size-8 bg-white position-absolute top-0 start-30 translate-middle">
              <button
                type="button"
                className={`ms-lg-4 text-white btn-progress btn btn-sm rounded-0 ${btnState.btn2}`}
                onClick={() => changeStep(1)}
              >
                2
              </button>
              <p
                className={`txt-compra-subtitle px-3 m-0 ${styles.isHiddenMobile}`}
              >
                Datos del <br /> comprador
              </p>
            </div>
            <div className="d-flex align-items-center is-size-8 bg-white position-absolute top-0 start-65 translate-middle ">
              <button
                type="button"
                className={`ms-lg-4 text-white btn-progress btn btn-sm rounded-0 ${btnState.btn3}`}
                onClick={() => changeStep(2)}
              >
                3
              </button>
              <p
                className={`txt-compra-subtitle px-3 m-0 ${styles.isHiddenMobile}`}
              >
                Resumen
                <br /> de la compra
              </p>
            </div>
            <div className="d-flex align-items-center is-size-8 bg-white position-absolute top-0 start-100 translate-middle align-items-center is-size-8">
              <button
                type="button"
                className={`ms-lg-4 text-white btn-progress btn btn-sm rounded-0 ${btnState.btn4}`}
                onClick={() => changeStep(3)}
              >
                4
              </button>
              <p
                className={`txt-compra-subtitle px-3 m-0 ${styles.isHiddenMobile}`}
              >
                Pago
              </p>
            </div>
          </div>
        </div>
      </div>
      {children}
    </div>
  );
};
