import Image from "next/image";
import Link from "next/link";
import styles from "./ButtonBar.module.scss";

export default function ButtonBar({href = '#', center=false, text, colorBtn, colorTxt='#fff', colorHr}) {
  return (
    <div className={`${styles.buttonbar} ${center && 'm-auto'}`}>
      <Link href="#" passHref>
        <a href="" style={{backgroundColor:`${colorBtn}`, color: `${colorTxt}`}}>
          <span className="pe-3 text-uppercase">{text}</span>
          <Image
            src={`${colorTxt === '#fff' ? "/assets/arrow-right4079.png" : "/assets/arrow-right-black.png"}`}
            alt="..."
            width={"23"}
            height={"15"}
          />
        </a>
      </Link>
      <hr style={{backgroundColor:`${colorHr}`}}/>
    </div>
  );
}
