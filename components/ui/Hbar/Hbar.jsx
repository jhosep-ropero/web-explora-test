import styles from "./Hbar.module.scss";

export default function Hbar({color, title, titleColor='#44444A'}) {
    return (
        <>
            <div className={styles.hbar}>
                <h2 className="mb-0 mt-5 text-uppercase fw-normal" style={{color: titleColor}}>{title}</h2>
                <hr style={{background: `${color}`}}/>
            </div>
        </>
    )
}
