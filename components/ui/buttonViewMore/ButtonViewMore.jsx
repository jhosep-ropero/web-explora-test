import Image from "next/image";

// Data
import arrowBot from "../../../public/assets/icon-arrowBot.png";
import arrowTop from "../../../public/assets/icon-arrowTop.png";

// Styles
import styles from './ButtonViewMore.module.scss';

const ButtonViewMore = ({viewMore, setViewMore, typeElements}) => {
    return (
        <a className={styles.buttonViewMore} onClick={() => setViewMore(!viewMore)}>
            {viewMore 
            ?  <a className="d-flex align-items-center text-uppercase text-center">Ver menos {typeElements} <Image width={14} height={10} src={arrowTop} alt=""/> </a> 
            : <a className="d-flex align-items-center text-uppercase text-center">Ver más {typeElements}<Image width={14} height={10} src={arrowBot} alt="" /></a>}          
        </a>
    );
}
 
export default ButtonViewMore;