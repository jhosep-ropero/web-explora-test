import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useRef, useState } from "react";
import { Container, Navbar } from "react-bootstrap";

export const NavbarMenu = ({ home = false }, { scroll = true }) => {
  const router = useRouter();
  const [navBackground, setNavBackground] = useState(false);
  const [navScroll, setNavScroll] = useState(false);
  const [tooglerNav, setTooglerNav] = useState(false);
  const navRef = useRef();

  navRef.current = navBackground;

  useEffect(
    () => {
      setNavBackground(false);
      if (home == true) {
        const show = window.scrollY > 50;
        setNavBackground(true);
      }
      const handleScroll = () => {
        const show = window.scrollY > 50;
        setNavScroll(true);
        if (home == false) {
          setNavBackground(false);
          setNavScroll(show);
        }
        if (home == true) {
          setNavScroll(show);
        } else if (navRef.current !== show) {
          setNavScroll(show);
        }
      };
      document.addEventListener("scroll", handleScroll);
      return () => {
        document.removeEventListener("scroll", handleScroll);
      };
    },
    [home],
    [scroll]
  );

  function isActive(route) {
    let indexRoute = router.pathname.indexOf("/", 2);
    let routeInitial;

    if (indexRoute === -1) {
      routeInitial = router.pathname;
    } else {
      routeInitial = router.pathname.slice(0, indexRoute);
    }

    if (route == routeInitial) {
      return "nav-link active";
    } else {
      return "nav-link";
    }
  }

  return (
    <div className="section-navbar">
      <Navbar
        className={`fixed-top ${
          navBackground ? "navbar-transparent" : "navbar-white"
        } ${navScroll ? "nav-scroll" : ""}`}
      >
        <Container>
          <Navbar.Brand href="#">
            {navScroll ? (
              <Image
                src={"/assets/logos/logo-explora.svg"}
                alt="parque explora"
                width="150px"
                height="60px"
              />
            ) : (
              <Image
                src={"/assets/logos/logo-explora.svg"}
                alt="parque explora"
                width="150px"
                height="60px"
              />
            )}
          </Navbar.Brand>

          <Navbar.Collapse id="navbarScroll">
            <ul className="justify-content-star align-items-center w-100 navbar-nav d-none d-xl-flex">
              <li className="nav-item">
                <Link href={"/"} passHref>
                  <a href="" className={isActive("/")} aria-current="page">
                    Inicio
                  </a>
                </Link>
              </li>
              <li className="nav-item">
                <Link href={"/visitanos"} passHref>
                  <a href="" className={isActive("/visitanos")}>
                    Visítanos
                  </a>
                </Link>
              </li>
              <li className="nav-item">
                <Link href={"/programate"} passHref>
                  <a href="" className={isActive("/programate")}>
                    Prográmate
                  </a>
                </Link>
              </li>
              <li className="nav-item">
                <Link href={"/aprende"} passHref>
                  <a href="" className={isActive("/aprende")}>
                    Aprende
                  </a>
                </Link>
              </li>
              <li className="nav-item">
                <a href="" className={isActive("/proyectos")}>
                  Proyectos
                </a>
              </li>
              <li className="nav-item">
                <Link href={"/servicios"} passHref>
                  <a href="" className={isActive("/servicios")}>
                    Servicios
                  </a>
                </Link>
              </li>
              <a href="" className="search ms-3">
                <i className="icon-explora icon-search" />
              </a>
            </ul>

            <div
              className={`nabvar-mobile ${tooglerNav ? "" : "d-none"}`}
              aria-hidden={!tooglerNav}
            >
              {/* Menu principal para versión móvil */}
              <ul className="list-unstyled nav-mobile">
                <li className="nav-item">
                  <Link href={"/"} passHref>
                    <a href="" className={isActive("/")} aria-current="page">
                      Inicio
                    </a>
                  </Link>
                </li>
                <li className="nav-item">
                  <Link href={"/visitanos"} passHref>
                    <a href="" className={isActive("/visitanos")}>
                      Visítanos
                    </a>
                  </Link>
                </li>
                <li className="nav-item">
                  <Link href={"/programate"} passHref>
                    <a href="" className={isActive("/programate")}>
                      Prográmate
                    </a>
                  </Link>
                </li>
                <li className="nav-item">
                  <Link href={"/aprende"} passHref>
                    <a href="" className={isActive("/aprende")}>
                      Aprende
                    </a>
                  </Link>
                </li>
                <li className="nav-item">
                  <a href="" className={isActive("/proyectos")}>
                    Proyectos
                  </a>
                </li>
                <li className="nav-item">
                  <Link href={"/servicios"} passHref>
                    <a href="" className={isActive("/servicios")}>
                      Servicios
                    </a>
                  </Link>
                </li>
                <a href="" className="search ms-3">
                  <i className="icon-explora icon-search" />
                </a>
              </ul>
            </div>
            <div
              className={`nabvar-mobile ${tooglerNav ? "" : "is-hidden"}`}
              aria-hidden={!tooglerNav}
            >
              {/* Menu principal para versión móvil */}
              <ul className="list-unstyled nav-mobile">
                <li className="nav-item">
                  <Link href={"/visitanos"} passHref>
                    <a href="" className={isActive("/visitanos")}>
                      Visítanos
                    </a>
                  </Link>
                </li>
                <li className="nav-item">
                  <Link href={"/programate"} passHref>
                    <a href="" className={isActive("/programate")}>
                      Prográmate
                    </a>
                  </Link>
                </li>
                <li className="nav-item">
                  <Link href={"/aprende"} passHref>
                    <a href="" className={isActive("/aprende")}>
                      Aprende
                    </a>
                  </Link>
                </li>
                <li className="nav-item">
                  <a className={isActive("/proyectos")}>Proyectos</a>
                </li>
                <li className="nav-item">
                  <Link href={"/servicios"} passHref>
                    <a className={isActive("/servicios")}>Servicios</a>
                  </Link>
                </li>
              </ul>

              {/* Navegación de Tienda y Boletería */}
              <ul className="extras list-unstyled">
                <li className="nav-item item-shop">
                  <Link href={"/boleteria"} passHref>
                    <a href="" className="nav-link">
                      <i className="icon-explora icon-planet-ticket" />
                      <span>Boletería</span>
                    </a>
                  </Link>
                </li>
                <li className="nav-item item-shop">
                  <Link href={"/tienda/cursos"} passHref>
                    <a href="http://3000/tienda/cursos" className="nav-link">
                      <i className="icon-explora icon-store" />
                      <span>Tienda</span>
                    </a>
                  </Link>
                </li>
                <li className="nav-item">
                  <a className="nav-link">
                    <i className="icon-explora icon-about" />
                    <div>
                      <h6>PARQUE EXPLORA</h6>
                      <p>Quiénes somos</p>
                    </div>
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link">
                    <i className="icon-explora icon-clock" />
                    <div>
                      <h6>HORARIOS Y TARIFAS</h6>
                      <p>Hoy 8:00 a.m 6:00 p.m</p>
                    </div>
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link">
                    <i className="icon-explora icon-location" />
                    <div>
                      <h6>CÓMO LLEGAR</h6>
                      <p>Ubicación</p>
                    </div>
                  </a>
                </li>
              </ul>

              <div className="icons-mobile">
                <a href="">
                  <i className="icon-explora icon-search" />
                </a>
                <span
                  className="close-nav"
                  onClick={() => setTooglerNav(false)}
                >
                  &times;
                </span>
              </div>
            </div>

            <ul className="buttons list-unstyled d-none d-xl-flex text-uppercase fs-4 m-0">
              <li className="nav-item">
                <Link href="/boleteria" passHref>
                  <a href="" className="nav-link">
                    <i className="icon-explora icon-planet-ticket" />
                    <span>Boletería</span>
                  </a>
                </Link>
              </li>
              <li className="nav-item">
                <Link href={"/tienda/cursos"} passHref>
                  <a href="" className="nav-link">
                    <i className="icon-explora icon-store" />
                    Tienda
                  </a>
                </Link>
              </li>
              <li className="nav-item">
                <Link href={""} passHref>
                  <a href="" className="nav-link">
                    <i className="icon-explora icon-clock" />
                    <span>Horarios</span>
                  </a>
                </Link>
              </li>
            </ul>
          </Navbar.Collapse>

          <div className="store-buttons d-flex d-xl-none">
            <a className="ms-0 me-2">Boletas</a>
            <a className="border-start ps-2">Tienda</a>
          </div>
          <div className="d-flex d-xl-none ms-4">
            <a className="search" href="">
              <i className="icon-explora icon-search" />
            </a>
          </div>

          <Navbar.Toggle
            className="d-block d-xl-none ms-3"
            aria-controls="navbarScroll"
            onClick={() => setTooglerNav(true)}
          />
        </Container>
      </Navbar>
    </div>
  );
};
