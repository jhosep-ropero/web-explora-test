import Image from "next/image";
import { useState } from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
// actions
import { accesContrastDark, accesContrastHigh, accesContrastLight, accesContrastNormal, accesFontSize2x, accesFontSizeLess, accesFontSizeNormal, accesHideImages, acceshideImages, acceshighlightLinks, acceshighlightTitles, accesLineLong, accesLineNormal, accesLineShort, accesLinksNormal, accesMonocromatico, accesReadingGuide, accesReadingMode, accesSaturationHigh, accesSaturationLight, accesSilence, accesSpaceLong, accesSpaceNormal, accesSpaceShort, accesStopAnimations, accesTitlesNormal, scaleContent2x, scaleContentLess, scaleContentNormal } from "../../../redux/actions/accesibility";
import { uiTabAccesibility } from "../../../redux/actions/ui";
//components
import BtnAccesibility from "../../ui/btnAccesibility/BtnAccesibility";
//styles
import styles from './Accesibility.module.scss';

export default function SidebarAccesibility() {

    //estado para manejar abrir y cerrar menu
    const [open,setOpen] = useState(false)

    const dispatch = useDispatch();
    //usar reducer de accesibilidad
    const {contrast,
        fontSize,
        highlightTitles,
        highlightLinks,
        space,
        scale,
        lineHeight,
        silence,
        hideImages,
        readingGuide,
        stopAnimations,
        readingMode
    } = useSelector((state) => state.accesibility)
    const {accesibilityActive} = useSelector((state) => state.uiTabAccesibility)

    useEffect(() => {
        //cambiar data-propertys de HTML segun data de reducer
        document.getElementsByTagName("HTML")[0].setAttribute("data-contrast", contrast);
        document.getElementsByTagName("HTML")[0].setAttribute("data-fontSize", fontSize);
        document.getElementsByTagName("HTML")[0].setAttribute("data-space", space);
        document.getElementsByTagName("HTML")[0].setAttribute("data-scaleContent", scale);
        document.getElementsByTagName("HTML")[0].setAttribute("data-lineHeight", lineHeight);
        document.getElementsByTagName("HTML")[0].setAttribute("data-highlightTitles", highlightTitles);
        document.getElementsByTagName("HTML")[0].setAttribute("data-highlightLinks", highlightLinks);
        document.getElementsByTagName("HTML")[0].setAttribute("data-highlightLinks", highlightLinks);

        //crear linea de cursor html
        if(readingGuide){
            const cursor = document.getElementById('cursor')
            window.addEventListener('mousemove', (e)=>{
                cursor.style.left = e.pageX + 5 + 'px'
                cursor.style.top = e.pageY + 15 + 'px'
            })
        }

    }, [contrast, fontSize, highlightTitles, highlightLinks, space, lineHeight, scale, readingGuide]);

    const closeModal = ()=>{
        
        dispatch(uiTabAccesibility())
        setTimeout(() => {
            document.querySelector('.Accesibility_bgAccesNone__0X5Q7').classList.add('d-none')
        }, 1200);
        
    }

    const openModal = ()=>{
        setOpen(true)
        dispatch(uiTabAccesibility())
    }
    
    return (
        <>
        {
            readingGuide &&
            <div id="cursor" className={styles.customCursor}></div>
        }
        <button className={styles.BtnAccesibility} onClick={openModal}>
            <Image 
                src={'/assets/icon-accesibility.png'}
                alt="accesibilidad"
                height={'46px'}
                width={'36px'}
            />
        </button>
        {
            accesibilityActive ?
            <div className={`${styles.bgAcces} d-flex justify-content-end`} onClick={closeModal}>
                <div 
                    className={`${styles.mainAcces} contain ml-auto p-sm-5 animate__animated ${accesibilityActive && 'animate__fadeInRight'}`}
                    onClick={(e)=> e.stopPropagation()}
                >
                    <div className="my-5 p-4">
                        <div className="col-12">
                            <h4 className="letter-340 font-weight-bold text-primary" >OPCIONES DE ACCESIBILIDAD  (CTRL+U)</h4>
                        </div>
                    </div>
                    <div className="w-100 px-1 px-sm-4">
                        {/* Seccion 1 */}
                        <div className="d-flex">
                            <h6 className="fw-bold">AJUSTES DE CONTENIDO</h6>
                            <hr />
                        </div>
                        <div className="d-flex justify-content-between py-4">
                            
                            <BtnAccesibility 
                                title="RESALTAR TITULOS" 
                                icon="titulos"
                                active={highlightTitles === 'titles' && true}
                                action={highlightTitles === 'Normal' ? ()=>dispatch(acceshighlightTitles()) :()=>dispatch(accesTitlesNormal())}
                            />
                            <BtnAccesibility 
                                title="RESALTAR ENLACES" 
                                icon="enlaces"
                                active={highlightLinks === 'links' && true}
                                action={highlightLinks === 'Normal' ? ()=>dispatch(acceshighlightLinks()) :()=>dispatch(accesLinksNormal())}
                            />
                             <div>
                                <BtnAccesibility 
                                    title="TAMAÑO DE TIPOGRAFÍA" 
                                    icon="tipografia"
                                />
                                <div className="d-flex justify-content-around px-4">
                                    <span 
                                        className={`${fontSize=== 'less' && styles.btnActive} bg-secondary is-size-6 py-2 w-50 text-center`}
                                        onClick={fontSize=== 'Normal' ? ()=>dispatch(accesFontSizeLess()) : ()=>dispatch(accesFontSizeNormal())}
                                    >&minus;
                                    </span>
                                    <span 
                                        className={`${fontSize=== '2x' && styles.btnActive} icon-explora icon-plus bg-secondary is-size-6 py-2 w-50 text-center`}
                                        onClick={fontSize=== 'Normal' ? ()=>dispatch(accesFontSize2x()) : ()=>dispatch(accesFontSizeNormal())}
                                    >
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div className="d-flex justify-content-between py-4">
                            {/* <div>
                                <BtnAccesibility 
                                    title="ESCALAR CONTENIDO" 
                                    icon="escalar-contenido" 
                                />
                                <div className="d-flex justify-content-around px-4">
                                    <span 
                                        className={`${scale=== 'less' && styles.btnActive} bg-secondary is-size-6 py-2 w-50 text-center`}
                                        onClick={scale=== 'Normal' ? ()=>dispatch(scaleContentLess()) : ()=>dispatch(scaleContentNormal())}
                                    >&minus;
                                    </span>
                                    <span 
                                        className={`${scale=== '2x' && styles.btnActive} icon-explora icon-plus bg-secondary is-size-6 py-2 w-50 text-center`}
                                        onClick={scale=== 'Normal' ? ()=>dispatch(scaleContent2x()) : ()=>dispatch(scaleContentNormal())}
                                    >
                                    </span>
                                </div>
                            </div> */}
                           
                            <div>
                                <BtnAccesibility 
                                    title="AJUSTAR INTERLINEADO" 
                                    icon="interlineado"
                                />
                                <div className="d-flex justify-content-around px-4">
                                    <span 
                                        className={`${lineHeight=== 'short' && styles.btnActive} bg-secondary is-size-6 py-2 w-50 text-center`}
                                        onClick={lineHeight=== 'Normal' ? ()=>dispatch(accesLineShort()) : ()=>dispatch(accesLineNormal())}
                                    >&minus;
                                    </span>
                                    <span 
                                        className={`${lineHeight=== 'long' && styles.btnActive} icon-explora icon-plus bg-secondary is-size-6 py-2 w-50 text-center`}
                                        onClick={lineHeight=== 'Normal' ? ()=>dispatch(accesLineLong()) : ()=>dispatch(accesLineNormal())}
                                    >
                                    </span>
                                </div>
                            </div>
                        </div>
                        {/* <div className="d-flex py-md-4">
                            <div>
                                <BtnAccesibility 
                                    title="AJUSTAR ESPACIO" 
                                    icon="espaciado"
                                />
                                <div className="d-flex justify-content-around px-4">
                                    <span 
                                        className={`${space=== 'short' && styles.btnActive} bg-secondary is-size-6 py-2 w-50 text-center`}
                                        onClick={space=== 'Normal' ? ()=>dispatch(accesSpaceShort()) : ()=>dispatch(accesSpaceNormal())}
                                    >&minus;
                                    </span>
                                    <span 
                                        className={`${space=== 'long' && styles.btnActive} icon-explora icon-plus bg-secondary is-size-6 py-2 w-50 text-center`}
                                        onClick={space=== 'Normal' ? ()=>dispatch(accesSpaceLong()) : ()=>dispatch(accesSpaceNormal())}
                                    >
                                    </span>
                                </div>

                            </div>
                        </div> */}

                        {/* seccion2 */}
                        <div className="d-flex justify-content-around mt-5 py-4">
                            <h6 className="fw-bold">AJUSTES DE COLOR</h6>
                            <hr />
                        </div>
                        <div className="d-flex justify-content-between py-4">
                            <BtnAccesibility 
                                title="CONTRASTE OSCURO" 
                                icon="contraste-oscuro"
                                active={contrast === 'Dark' && true}
                                action={contrast === 'Normal' ? ()=>dispatch(accesContrastDark()) :()=>dispatch(accesContrastNormal())}
                            />
                            <BtnAccesibility 
                                title="SATURACIÓN ALTA" 
                                icon="saturacion-alta"
                                active={contrast === 'SaturationHigh' && true}
                                action={contrast === 'Normal' ? ()=>dispatch(accesSaturationHigh()) : ()=>dispatch(accesContrastNormal())}
                            />
                            <BtnAccesibility 
                                title="MODO CROMÁTICO" 
                                icon="monocromatico"
                                active={contrast === 'Monocromatico' && true}
                                action={contrast === 'Normal' ? ()=>dispatch(accesMonocromatico()) : ()=>dispatch(accesContrastNormal())}
                                />
                        </div>
                        <div className="d-flex justify-content-between py-4"> 
                            {/* <BtnAccesibility 
                                title="SATURACIÓN BAJA" 
                                icon="saturacion-baja"
                                active={contrast === 'SaturationLight' && true}
                                action={contrast === 'Normal' ? ()=>dispatch(accesSaturationLight()) : ()=>dispatch(accesContrastNormal())}
                            />
                            <BtnAccesibility 
                                title="CONTRASTE ALTO" 
                                icon="contraste-alto"
                                active={contrast === 'High' && true}
                                action={contrast === 'Normal' ? ()=>dispatch(accesContrastHigh()) : ()=>dispatch(accesContrastNormal())}
                            /> 
                            <BtnAccesibility 
                                title="CONTRASTE CLARO" 
                                icon="contraste-claro" 
                                active={contrast === 'Light' && true}
                                action={contrast === 'Normal' ? ()=>dispatch(accesContrastLight()) : ()=>dispatch(accesContrastNormal())}
                            /> */}
                        </div>

                        {/* seccion23*/}
                        <div className="d-flex mt-5 py-4">
                            <h6 className="fw-bold">AJUSTES DE ORIENTACIÓN</h6>
                            <hr />
                        </div>
                        <div className="d-flex justify-content-between py-4">
                            <BtnAccesibility 
                                title="OCULTAR IMÁGENES" 
                                icon="imagenes-ocultar"
                                active={hideImages && true}
                                action={()=>dispatch(accesHideImages())}
                            /> 
                            <BtnAccesibility 
                                title="LÍNEA GUÍA DE LECTURA" 
                                icon="guia-lectora"
                                active={readingGuide && true}
                                action={()=>dispatch(accesReadingGuide())}
                            /> 
                            <BtnAccesibility 
                                title="DETENER VIDEOS" 
                                icon="animaciones-detenr"
                                active={stopAnimations && true}
                                action={()=>dispatch(accesStopAnimations())}
                            />
                        </div>
                        <div className="d-flex justify-content-between py-4">
                            {/* <BtnAccesibility 
                                title="OMITIR SONIDOS" 
                                icon="sonido-omitir"
                                active={silence && true}
                                action={()=>dispatch(accesSilence())}
                            />  */}
                        </div>
                    </div>
                    
                    
                    <div className={styles.divClose}>
                        <div className={`${styles.btnClose} text-white is-size-5`} onClick={closeModal}>
                        X
                        </div>
                    </div>
                    
                </div>
            </div>
            :
            open &&
            <div className={`${styles.bgAccesNone} d-flex justify-content-end`}>
                <div className={`${styles.mainAcces} contain ml-auto p-sm-5 animate__animated ${!accesibilityActive && 'animate__fadeOutRight'}`}>
                    <div className="row my-5 py-4">
                        <div className="col-12">
                            <h3 className="letter-340 font-weight-bold text-primary" >OPCIONES DE ACCESIBILIDAD  (CTRL+U)</h3>
                        </div>
                    </div>
                    <div className="w-75">
                        {/* Seccion 1 */}
                        <div className="d-flex">
                            <h6 className="text-secondary font-weight-bold ">AJUSTES DE CONTENIDO</h6>
                            <hr />
                        </div>
                        <div className="d-flex py-md-4">
                            <BtnAccesibility title="ESCALAR CONTENIDO" icon="escalar-contenido" />
                            <BtnAccesibility title="RESALTAR TITULOS" icon="titulos"/>
                            <BtnAccesibility title="RESALTAR ENLACES" icon="enlaces"/>
                        </div>
                        <div className="d-flex py-md-4">
                            <BtnAccesibility title="TAMAÑO DE TEXTO" icon="tipografia"/>
                            <div>
                                <BtnAccesibility 
                                    title="TAMAÑO DE TIPOGRAFÍA" 
                                    icon="tipografia"
                                    active={fontSize !== 'Normal' && true}
                                    action={()=>dispatch(accesFontSizeNormal())}
                                />
                                <div className="d-flex justify-content-around px-4">
                                    <Image src="/assets/icon_17.png" alt="" />
                                    <Image src="/assets/icon_16.png" alt="" />
                                </div>
                            </div>
                            <BtnAccesibility title="AJUSTAR INTERLINEADO" icon="interlineado"/>
                        </div>
                        <div className="d-flex py-md-4">
                            <BtnAccesibility title="AJUSTAR ESPACIO" icon="espaciado"/>
                        </div>

                        {/* seccion2 */}
                        <div className="d-flex mt-5 py-4">
                            <h6 className="text-secondary font-weight-bold">AJUSTES DE COLOR</h6>
                            <hr />
                        </div>
                        <div className="d-flex py-4">
                            <BtnAccesibility 
                                title="CONTRASTE OSCURO" 
                                icon="contraste-oscuro"
                                active={contrast === 'Dark' && true}
                                action={contrast === 'Normal' ? ()=>dispatch(accesContrastDark()) :()=>dispatch(accesContrastNormal())}
                            />
                            <BtnAccesibility 
                                title="CONTRASTE CLARO" 
                                icon="contraste-claro" 
                                active={contrast === 'Light' && true}
                                action={contrast === 'Normal' ? ()=>dispatch(accesContrastLight()) : ()=>dispatch(accesContrastNormal())}
                            />
                            <BtnAccesibility title="MODO MONOCROMÁTICO" icon="monocromatico"/>
                        </div>
                        <div className="d-flex py-4">
                            <BtnAccesibility title="SATURACIÓN ALTA" icon="saturacion-alta"/>
                            <BtnAccesibility title="SATURACIÓN BAJA" icon="saturacion-baja"/>
                            <BtnAccesibility 
                                title="CONTRASTE ALTO" 
                                icon="contraste"
                                active={contrast === 'High' && true}
                                action={contrast === 'Normal' ? ()=>dispatch(accesContrastHigh()) : ()=>dispatch(accesContrastNormal())}
                            /> 
                        </div>

                        {/* seccion23*/}
                        <div className="d-flex mt-5 py-4">
                            <h6 className="text-secondary font-weight-bold">AJUSTES DE ORIENTACIÓN</h6>
                            <hr />
                        </div>
                        <div className="d-flex py-4">
                            <BtnAccesibility title="OMITIR SONIDOS" icon="sonido-omitir"/> 
                            <BtnAccesibility title="OCULTAR IMÁGENES" icon="imagenes-ocultar"/> 
                            <BtnAccesibility title="LÍNEA GUÍA DE LECTURA" icon="guia-lectora"/> 
                        </div>
                        <div className="d-flex py-4">
                            <BtnAccesibility title="DETENER ANIMACIONES" icon="animaciones-detenr"/> 
                            <BtnAccesibility title="MODO DE LECTURA" icon="modo-lectura"/> 
                            <BtnAccesibility title="OCULTAR IMÁGENES" icon="imagenes-ocultar"/> 
                        </div>
                        <div className="d-flex py-4">
                            <BtnAccesibility title="ENFOCAR CONTENIDO" icon="enfocar-contenido"/> 
                        </div>
                    </div>
                    
                    
                    <div className={styles.divClose}>
                        <div className={`${styles.btnClose} text-white is-size-5`}>
                        X
                        </div>
                    </div>
                    
                </div>
            </div>
        }
        </>
    )
}
