// Data
import { rooms } from "../../../data/rooms";
import { Layout } from "../../layouts/Layout";
import { NavbarMenu } from "../navbar/NavbarMenu";
import SliderOne from "../../pages/visitanos/Rooms/singleRooms/SliderOne";
import SliderTwo from "../../pages/visitanos/Rooms/singleRooms/SliderTwo";
import PhotoGallery from "../../pages/visitanos/Rooms/singleRooms/photo-gallery/PhotoGallery";
import PlayList from "../../pages/visitanos/Rooms/singleRooms/PlayList";
import Description from "../../pages/visitanos/Rooms/singleRooms/Description";
import CarouselStore from "../../modules/CarouselStore";
import ExploraRedes from "../../pages/inicio/ExploraRedes";

export default function index() {
  const { acuario } = rooms;
  return (
    <Layout>
      <NavbarMenu />
      <div>
        <SliderOne
          background={acuario.background1}
          room={acuario.info.title}
          title={acuario.title1}
        />
        <SliderTwo background={acuario.background2} title={acuario.title2} />
        <PhotoGallery images={acuario.gallery} />
        <PlayList playList={acuario.playList} />
        <Description
          background={acuario.info.background}
          title={acuario.info.title}
          description={acuario.info.description}
          reconocimientos={acuario.info.reconocimientos}
        />
        <CarouselStore colorSeparator={"#F9D51D"} />
        <ExploraRedes />
      </div>
    </Layout>
  );
}
