import Image from "next/image";
import Link from "next/link";
import NetworksPlanetary from "../../ui/Social/NetworksPlanetary";
import styles from "./Footer.module.scss"

export default function FooterPlanetary() {
    return (
        <footer className={`${styles.footerPlanetary} text-white`}>
            <div className="container pt-4">
                <div className="row py-5 px-3 align-items-start justify-content-center position-relative">
                    <div className={`${styles.logos} d-flex col-lg-6 mt-2 mb-4 py-0`}>
                        <a href={`http://localhost:3000`}>
                            <Image src='/assets/logos/logo-explora_white.svg' alt="explora" width='160px' height='80px'/>
                        </a>
                        <a>
                            <Image src='/assets/logos/logo-planetario_white.svg' alt="planetario" width='160px' height='65px'/>
                        </a>
                        <a>
                            <Image src='/assets/logos/logo-exploratorio_white.svg' alt="planetario" width='180px' height='65px'/>
                        </a>
                    </div>
                    <div className={`col-md-3 col-lg-2 p-0`}>
                        <h5 className="fw-light">El Planetario</h5>
                        <ul className="text-uppercase fs-6">
                            <li>
                                <Link href={``} passHref>
                                    <a href="">Quiénes somos</a>
                                </Link>
                            </li>
                            <li>
                                <Link href={``} passHref>
                                    <a href="">Prensa</a>
                                </Link>
                            </li>
                            <li>
                                <Link href={``} passHref>
                                    <a href="">Patrocinadores</a>
                                </Link>
                            </li>
                        </ul>
                        <div  className={`${styles.social}`}>
                            <NetworksPlanetary />
                        </div>
                    </div>
                    <div className={`col-md-3 col-lg-2 p-0`}>
                        <h5 className="fw-light">Conéctate</h5>
                        <ul className="text-uppercase fs-6">
                            <li>
                                <Link href={``} passHref>
                                    <a href="">Boletines</a>
                                </Link>
                            </li>
                            <li>
                                <Link href={``} passHref>
                                    <a href="">Cotacto</a>
                                </Link>
                            </li>
                            <li>
                                <Link href={``} passHref>
                                    <a href="">Trabaja con nosotros</a>
                                </Link>
                            </li>
                        </ul>
                    </div>
                    <div className={`col-md-3 col-lg-2 p-0`}>
                        <h5 className="fw-light">Ubicación</h5>
                        <p className="fs-6">
                        CARRERA 52 N. 71 - 117<br/>
                        MEDELLÍN - COLOMBIA<br/>
                        +57(4) 516 83 00<br/>
                        GOOGLE MAPS  -  WAZE<br/>
                        </p>
                    </div>
                </div>
            </div>
        </footer>
    )
}
