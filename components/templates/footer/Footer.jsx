import Image from "next/image";
import Link from "next/link";
import NetworksExplore from "../../ui/Social/NetworksExplore";
import styles from "./Footer.module.scss"

export default function Footer() {
    return (
        <footer className={`${styles.footerExplora} text-white`}>
            <div className="container pt-4">
                <div className="row py-5 px-3 align-items-stretch position-relative fs-5">
                    <div className={`${styles.logos} d-flex col-lg-6 mt-4 mb-4 py-0`}>
                        <a>
                            <Image src='/assets/logos/logo-explora_white.svg' alt="explora" width='160px' height='80px'/>
                        </a>
                        <a href={`http://localhost:3000/planetario`}>
                            <Image src='/assets/logos/logo-planetario_white.svg' alt="planetario" width='160px' height='65px'/>
                        </a>
                        <a>
                            <Image src='/assets/logos/logo-exploratorio_white.svg' alt="planetario" width='180px' height='65px'/>
                        </a>
                    </div>
                    <div className={`${styles.location} col-12 col-md-6 col-lg-3`}>
                        <div>
                            <h5>Ubicación</h5>
                            <p className="">
                            CARRERA 52 N. 71 - 117<br/>
                            MEDELLÍN - COLOMBIA<br/>
                            +57(4) 516 83 00<br/>
                            GOOGLE MAPS  -  WAZE<br/>
                            </p>
                            <div  className={`${styles.social}`}>
                                <NetworksExplore />
                            </div>
                        </div>
                    </div>
                    <div className={`col-12 col-md-6 col-lg-3`}>
                        <ul className="text-uppercase">
                            <li>
                                <Link href={``} passHref>
                                    <a href="">¿Qué es el Parque Explora?</a>
                                </Link>
                            </li>
                            <li>
                                <Link href={``} passHref>
                                    <a href="">Patrocinadores y aliados</a>
                                </Link>
                            </li>
                            <li>
                                <Link href={``} passHref>
                                    <a href="">Consejo asesor científico</a>
                                </Link>
                            </li>
                            <li>
                                <Link href={``} passHref>
                                    <a href="">Fundación amigos de explora</a>
                                </Link>
                            </li>
                            <li>
                                <Link href={``} passHref>
                                    <a href="">Condiciones legales</a>
                                </Link>
                            </li>
                            <li>
                                <Link href={``} passHref>
                                    <a href="">Sobre la corporación</a>
                                </Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
    )
}
