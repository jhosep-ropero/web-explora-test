import { useState, useEffect } from "react";

// Components
import CardProgramate from "../CardProgramate/CardProgramate";
import Search from "../Search/Search";
import Filter from "../Filter/Filter";
// import ButtonViewMore from "../../../ui/buttonViewMore/ButtonViewMore";

// Data
import { programate } from "../../../../data/programate";
import { getAllProgramates } from "../../../../lib/api";

// Styles
import styles from "./OurProgramming.module.scss";

const OurProgramming = ({ events }) => {
  const eventos = programate;

  // State para manejar el filtro de eventos
  const [eventosFiltrados, setEventosFiltrados] = useState(events);
  // State para manejar la busqueda
  const [busqueda, setBusqueda] = useState("");

  // State para ver más
  // const [viewMore, setViewMore] = useState(true);
  // State boton de ver más
  // const [showButtonViewMore, setShowButtonViewMore] = useState(false);

  // useEffect(()=>{
  //   window.addEventListener("resize", () => {
  //     if (window.screen.width <= 768){
  //       setShowButtonViewMore(true)        
  //       setViewMore(false)
  //     }else{
  //       setShowButtonViewMore(false)
  //       setViewMore(true)
  //     }
  //   });
  // },[])

  const handleChange = (e) => {
    setBusqueda(e.target.value);
    filtroEventos(e.target.value);
  };

  const filtroEventos = (valor) => {
    const results = eventos.filter((evento) => {
      if (
        evento.title.toLowerCase().includes(valor.toLowerCase()) ||
        evento.location.toLowerCase().includes(valor.toLowerCase()) ||
        evento.keywords.toLowerCase().includes(valor.toLowerCase())
      ) {
        return evento;
      }
    });
    setEventosFiltrados(results);
  };

  // const eventsOrderAsc = eventosFiltrados
  //   .sort((a, b) => {
  //     if (a.dateCalendar.year > b.dateCalendar.year) {
  //       return 1;
  //     }
  //     if (a.dateCalendar.year <= b.dateCalendar.year) {
  //       return -1;
  //     }
  //     return 0;
  //   })
  //   .sort((a, b) => {
  //     if (a.dateCalendar.month > b.dateCalendar.month) {
  //       return 1;
  //     }
  //     if (a.dateCalendar.month <= b.dateCalendar.month) {
  //       return -1;
  //     }
  //     return 0;
  //   })
  //   .sort((a, b) => {
  //     if (a.dateCalendar.day > b.dateCalendar.day) {
  //       return 1;
  //     }
  //     if (a.dateCalendar.day <= b.dateCalendar.day) {
  //       return -1;
  //     }
  //     return 0;
  //   });

    // const data = eventsOrderAsc.slice(0, 3);

  return (
    <div>
      <div className="container py-5 d-sm-flex align-items-center justify-content-end position-relative">
        <Filter events={programate} setEventsFilter={setEventosFiltrados} />
        <Search handleChange={handleChange} busqueda={busqueda} />
      </div>
      <div className="container d-flex justify-content-center">
        <div className="row justify-content-center justify-content-md-start w-100">
        {eventosFiltrados.map((event, index) => (
            <div
              className={`col-12 col-md-6 col-lg-3 ${styles.listCardProgramate}`}
              key={index}
            >
              <CardProgramate
                location={event.metadata.location[0]}
                img={event.thumbnail}
                audience={event.metadata.audience[0]}
                modality={event.metadata.modality}
                price={event.metadata.price}
                date={event.metadata.date}
                title={event.title}
                time={event.metadata.hour}
                slug={event.slug}
              />
            </div>
          ))}
          {/* {viewMore &&
            eventsOrderAsc.map((ev) => (
              <div
                className={`col-12 col-md-6 col-lg-3 ${styles.listCardProgramate}`}
                key={ev.id}
              >
                <CardProgramate
                  id={ev.id}
                  location={ev.location}
                  img={ev.img}
                  audience={ev.audience}
                  modality={ev.modality}
                  price={ev.price}
                  free={ev.free}
                  date={ev.date}
                  title={ev.title}
                  time={ev.time}
                  dateExtend={ev.dateExtend}
                />
              </div>
            ))} */}
          {/* {!viewMore &&
            data.map((ev) => (
              <div
                className={`col-12 col-md-6 col-lg-3 ${styles.listCardProgramate}`}
                key={ev.id}
              >
                <CardProgramate
                  id={ev.id}
                  location={ev.location}
                  img={ev.img}
                  audience={ev.audience}
                  modality={ev.modality}
                  price={ev.price}
                  free={ev.free}
                  date={ev.date}
                  title={ev.title}
                  time={ev.time}
                  dateExtend={ev.dateExtend}
                />
              </div>
            ))} */}
        </div>
      </div>
      {/* {showButtonViewMore && (
        <ButtonViewMore viewMore={viewMore} setViewMore={setViewMore} typeElements="eventos"/>
      )} */}
    </div>
  );
};

export default OurProgramming;
