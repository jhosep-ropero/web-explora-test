// Styles
import styles from './Search.module.scss';

const Search = ({handleChange, busqueda}) => {
    return (
        <form className="position-relative d-flex justify-content-center justify-content-lg-end">                    
            <input 
                className={`${styles.inputSearch} `} 
                type="text" 
                value={busqueda}
                onChange={handleChange} 
            />
            <button className={`${styles.buttonSearch}`} type="submit" >Buscar</button>                
        </form>
    );
}

export default Search;