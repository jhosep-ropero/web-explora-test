import Image from "next/image";
import Link from "next/link";

import styles from "./CardProgramate.module.scss";

const CardProgramate = ({
  location,
  img,
  audience,
  modality,
  price,
  date,
  title,
  time,
  slug,
}) => {
  const separatedDate = date ? date.split("-") : ["2022", 1, "27"];

  const formatedDate = new Date(
    separatedDate[0],
    separatedDate[1] - 1,
    separatedDate[2]
  );

  return (
    <div className={`${styles.cardProgramate}`}>
      <div className="position-relative">
        <Image
          src={img}
          className="card-img-top img-fluid "
          alt="..."
          width="100%"
          height="52%"
          layout="responsive"
          objectFit="cover"
        />
        {location && (
          <p className={`${styles.cardLocation} px-4 py-2`}>{location}</p>
        )}
        <div className={`${styles.cardAudience} d-flex`}>
          <p className={`py-2 px-3 bg-primary`}>{audience}</p>
          <p className={`py-2 px-3 ${styles.cardBgModality}`}>{modality}</p>
        </div>
      </div>

      {date && (
        <>
          <div className="d-flex justify-content-between align-items-center pt-3 px-3">
            <div className="d-flex ">
              <h3 className={`${styles.date} px-1 is-size-6`}>
                {formatedDate.toLocaleString("default", {
                  day: "2-digit",
                })}
              </h3>
              <h3
                className={`${styles.date} px-1 is-size-6 is-light text-uppercase `}
              >
                {formatedDate
                  .toLocaleString("default", {
                    month: "short",
                  })
                  .replace(".", "")}
              </h3>
            </div>
            <div className="d-flex align-items-center border rounded-2 border-primary text-primary px-3 py-2 ">
              <Image
                src={"/assets/icon-transmision.png"}
                alt="evento"
                width={"28px"}
                height={"21px"}
              />
              <span className="ms-2 fs-5">Ocurre ahora</span>
            </div>
          </div>
          <hr />
        </>
      )}

      <div className={`${styles.cardBody}`}>
        <p
          className={`${styles.cardTime} font-weight-light work-sans is-size-8`}
        >
          {time}
        </p>
        <h5 className={`${styles.cardTitle} text-uppercase is-size-8 is-bold`}>
          {title}
        </h5>
        {price !== "0" ? (
          <p
            className={`${styles.cardPrice} font-weight-light work-sans is-bold`}
          >
            $ {price}
          </p>
        ) : (
          <p
            className={`${styles.cardFree} font-weight-light work-sans text-uppercase is-bold`}
          >
            sin costo
          </p>
        )}
        <Link href="/programate/[slug]" as={`/programate/${slug}`} passHref>
          <button className={`${styles.btn} d-block fw-bold py-3 mx-auto my-4`}>
            VER MÁS
          </button>
        </Link>
        {location && (
          <p
            className={`${styles.cardRegister} text-center text-uppercase m-4`}
          >
            Quiero inscribirme
          </p>
        )}
      </div>
    </div>
  );
};

export default CardProgramate;
