
// Components
import ButtonBar from "../../../ui/ButtonBar/ButtonBar";
import PageBarAlt from "../../../ui/PageBar/PageBarAlt";

// Styles
import styles from "./Banner.module.scss";

const Banner = ({title}) => {
    return (        
        <div className={`${styles.bannerProgramming} d-flex flex-column`}>
            <PageBarAlt title={title} color={'#FFCB05'} transparent={true}/>
            <div className={`container ${styles.infoBannerProgramming} text-white ps-md-auto`}>
                <h3 className="is-size-2 is-bold">Taller en Moravia: <br /> La Física de un soplido</h3>
                <p className="is-color-white is-size-7 text-uppercase is-bold">Programación destacada</p>
                <ButtonBar
                    text={'Ver evento'}                    
                    colorBtn={'#F45858'}
                    colorHr={'#F9D41E'}
                />
            </div>
        </div>       
    );
}
 
export default Banner;