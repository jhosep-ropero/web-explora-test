import { useEffect, useState } from 'react';
import Image from "next/image";
import { Button } from "react-bootstrap";
import { DatePickerFilter } from '../../../ui/datepickerFilter/DatePickerFilter';

// Helpers
import { handleChangeFilter, handleChangeFilterCalendar } from '../../../../helpers/handleChangeFilter';

// Data
import { months } from '../../../../data/months';
import close from '../../../../public/assets/taquilla/close.png';
import arrowRigth from "../../../../public/assets/taquilla/arrow-rigth-white.png";

// Styles
import styles from './Filter.module.scss';

const checkedInit = {
    todoMuseo: true,
    explora: false,
    planetario: false,
    exploratorio: false,
    todoPublico: true,
    familiar: false,
    adultos: false,
    niños: false,
    maestros: false,
    presencial: false,
    online: false,
    gratis: false,
    donacion: false
}

const selectedInit = {
    Enero: false,
    Febrero: false,
    Marzo: false,
    Abril: false,
    Mayo: false,
    Junio: false,
    Julio: false,
    Agosto: false,
    Septiembre: false,
    Octubre: false,
    Noviembre: false,
    Diciembre: false
}

const date = new Date();

const defaultFrom = {
    year: date.getFullYear(),
    month: date.getMonth() + 1,
    day: date.getDate(),
};

const defaultTo = {
    year: date.getFullYear(),
    month: date.getMonth() + 1,
    day: date.getDate(),
};

const defaultRange = {
    from: defaultFrom,
    to: defaultTo,
};

export default function Filter({ events, setEventsFilter }) {
    const [categories, setCategories] = useState([]);

    // State para manejar las selecciones
    const [checked, setChecked] = useState(checkedInit);
    // State para manejar las selecciones de mes
    const [checkedMonth, setCheckedMonth] = useState(selectedInit);
    // State para manejar el rango de fechas
    const [valueDateSelect, setValueDateSelect] = useState(defaultRange);


    // State para abrir el filtro de eventos
    const [openFilter, setOpenFilter] = useState(false);
    // State para mostrar etiquetas de los filtros seleccionados
    const [viewFilters, setViewFilters] = useState(false);

    // State para abrir el filtro de eventos por fecha
    const [openFilterCalendar, setOpenFilterCalendar] = useState(false);
    // State para mostar etiquetas de los filtros del calendario seleccionados
    const [viewFiltersCalendar, setViewFiltersCalendar] = useState(false);

    // State para controlar el select y mostar el select de meses
    const [viewSelectMonth, setViewSelectMonth] = useState(false);
    // State para controlar el select y mostrar el select de calendario
    const [viewCalendar, setViewCalendar] = useState(false);

    

    // Se ejecuta cada vez que cambia el state categories
    useEffect(() => {

    }, [categories])

    //funcion eliminar categorias del estado
    const deleteCategory = (category)=>{            
        const newCategories = categories.filter(cat => cat !== category)
        setCategories(newCategories)
    }
    

    //funcion de manejador de cambios de los checkbox
    const handleChange = (e) => {                       
        handleChangeFilter(e, categories, setCategories, checked, setChecked, checkedMonth, setCheckedMonth, deleteCategory, checkedInit);
    }

    // Función para manejar cambios en la selección calendario
    const handleChangeCalendar=(e)=>{
        setValueDateSelect(e);
        //setCategories([]); 
        // console.log(e);
        handleChangeFilterCalendar(e, categories, setCategories, valueDateSelect, setValueDateSelect, deleteCategory, defaultRange);
    }

    const deleteAllFilters = () => {
        setCategories([])
        setViewFilters(false)
        // setViewFiltersCalendar(false)
        setEventsFilter(events)
    }

    const deleteFilter = (category) => {
        const newCategories = categories.filter(cat => cat !== category)
        if (newCategories.length == 0) {
            setViewFilters(false)
            // setViewFiltersCalendar(false)
            setCategories([])
            setEventsFilter(events)
        } else {
            setCategories(newCategories)
            filterEvents(newCategories)
        }
    }

    //filtrar eventos por categorias, se recorren ambos arreglos para buscar coincidencias
    const filterEvents = (newCategories = categories) => {
        const eventsResult = [];
 
        if (newCategories.length === 0) {
            return setEventsFilter(events)
        }
        
        events.map((event) => newCategories.map(categ => { 
            const dateEvent=event.date.day+" "+event.date.month;
            if (categ === 'Museo') {
                eventsResult.push(event)
                setCategories([])
                setViewFilters(false)
                // setViewFiltersCalendar(false)
            } else if (categ === 'Publico') {
                eventsResult.push(event)
                setCategories([])
                setViewFilters(false)
                // setViewFiltersCalendar(false)
            } else if (categ === event.audience) {
                eventsResult.push(event)
            } else if (categ === event.location) {
                eventsResult.push(event)
            } else if (categ === event.modality) {
                eventsResult.push(event)
            } else if (categ === event.free) {
                eventsResult.push(event)
            } else if (categ === event.date.month) {
                eventsResult.push(event)
            }else if(categ===dateEvent){   
                // Setear categorias luego de filtrado y ver que pasa con el filtro de fecha             
                eventsResult.push(event)
            }
        })
        );

        //eliminar duplicados
        const dataArr = new Set(eventsResult);
        let result = [...dataArr];

        if (result.length === 0) {
            setEventsFilter([])
        } else {
            setEventsFilter(result);
        }
    }

    // Función que se ejecuta al cerrar el modal de filtros por fecha
    const handleCloseFilterDate = () => {
        setOpenFilterCalendar(!openFilterCalendar)
        setViewSelectMonth(false)
        setViewCalendar(false)
    }

    // Función que se ejecuta al dar click en el input Por Mes
    const handleChangeInputMonth = () => {
        setViewSelectMonth((prevState)=> !prevState)
        setViewCalendar(false)
    }

    // Función que se ejecuta al dar click en el input Por rango de fecha
    const handleChangeInputDate = () => {
        setValueDateSelect(defaultRange)
        setViewCalendar((prevState)=> !prevState)
        setViewSelectMonth(false)
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        if (categories.length > 0) {
            setViewFilters(true)
            // setViewFiltersCalendar(true)
        }
        setChecked(checkedInit)
        setCheckedMonth(selectedInit)
        setOpenFilter(false)
        setOpenFilterCalendar(false)
        setViewSelectMonth(false)
        setViewCalendar(false)
        filterEvents()
    }


    return (
        <>
            <div className='d-flex justify-content-between'>
                {
                    !viewFilters ?
                        <>
                            <div className={`${styles.selectFilter} "fw-light is-size-9 ps-1 pb-4 pb-md-0" `}>
                                Todo el museo
                                <span className={`${styles.pointer} " ps-3 text-black-50 "`} onClick={() => setOpenFilter(!openFilter)}>▼</span>
                            </div>
                            <div className={`${styles.selectFilterCalendar} "fw-light is-size-9 ps-1 pb-4 pb-md-0" `}>
                                Rangos de fecha
                                <span className={`${styles.pointer} " ps-3 text-black-50 "`} onClick={() => setOpenFilterCalendar(!openFilterCalendar)}>▼</span>
                            </div>
                        </>
                        : <div className={`${styles.selectFilter2} "fw-light ps-1 pb-4 pb-md-0 h4" `}>
                            <span className={`${styles.deleteFilter} text-decoration-underline pe-5`}>
                                <span className={"pe-2 text-black-50"} onClick={deleteAllFilters}>&times;</span>
                                Limpiar filtros
                            </span>
                            <div className='d-flex flex-wrap'>
                                {
                                    categories.map((cat, i) =>
                                        <span key={i} className='text-primary px-2 is-size-9'>
                                            <span className={`${styles.pointer} ' text-black-50 pe-1 '`} onClick={() => deleteFilter(cat)}>&times;</span>
                                            {cat}
                                        </span>
                                    )
                                }
                            </div>
                        </div>
                }
            </div>

            {
                openFilter &&
                <div className={`${styles.filter} fw-light is-size-9`}>
                    <div className='ps-1 pb-4 pb-md-0'>
                        Todo el museo
                        <span className={`${styles.pointer} " ps-3 text-black-50 "`} onClick={() => setOpenFilter(!openFilter)}>▼</span>
                    </div>
                    <form onSubmit={handleSubmit}>
                        <div className='d-flex justify-content-between flex-wrap'>
                            <ul className="d-flex justify-content-start h-100 flex-column">
                                <h3 className='text-center is-size-7 text-uppercase is-bold'>Museo</h3>
                                <li className='mb-3 mb-md-4'>
                                    <input type="checkbox" id="todoMuseo" value="Museo" onChange={handleChange} checked={checked.todoMuseo} />
                                    <label htmlFor="todoMuseo">Todo el museo</label>
                                    <div className={styles.check}></div>
                                </li>
                                <li className='mb-3 mb-md-4'>
                                    <input type="checkbox" id="explora" value="Parque Explora" onChange={handleChange} checked={checked.explora} />
                                    <label htmlFor="explora">Parque Explora</label>
                                    <div className={styles.check}></div>
                                </li>
                                <li className='mb-3 mb-md-4'>
                                    <input type="checkbox" id="planetario" value="Planetario" onChange={handleChange} checked={checked.planetario} />
                                    <label htmlFor="planetario">Planetario</label>
                                    <div className={styles.check}></div>
                                </li>
                                <li className='mb-3 mb-md-4'>
                                    <input type="checkbox" id="exploratorio" value="Exploratorio" onChange={handleChange} checked={checked.exploratorio} />
                                    <label htmlFor="exploratorio">Exploratorio</label>
                                    <div className={styles.check}></div>
                                </li>
                            </ul>

                            <ul className="d-flex justify-content-start h-100 flex-column">
                                <h3 className='text-center is-size-7 text-uppercase is-bold'>Público</h3>
                                <li className='mb-3 mb-md-4'>
                                    <input type="checkbox" id="todoPublico" value="Publico" onChange={handleChange} checked={checked.todoPublico} />
                                    <label htmlFor="todoPublico">Todos los públicos</label>
                                    <div className={styles.check}></div>
                                </li>
                                <li className='mb-3 mb-md-4'>
                                    <input type="checkbox" id="familiar" value="Familiar" onChange={handleChange} checked={checked.familiar} />
                                    <label htmlFor="familiar">Familiar</label>
                                    <div className={styles.check}></div>
                                </li>
                                <li className='mb-3 mb-md-4'>
                                    <input type="checkbox" id="adultos" value="Jóvenes y adultos" onChange={handleChange} checked={checked.adultos} />
                                    <label htmlFor="adultos">Jóvenes y adultos</label>
                                    <div className={styles.check}></div>
                                </li>
                                <li className='mb-3 mb-md-4'>
                                    <input type="checkbox" id="niños" value="Niños" onChange={handleChange} checked={checked.niños} />
                                    <label htmlFor="niños">Niños</label>
                                    <div className={styles.check}></div>
                                </li>
                                <li className='mb-3 mb-md-4'>
                                    <input type="checkbox" id="maestros" value="Maestros" onChange={handleChange} checked={checked.maestros} />
                                    <label htmlFor="maestros">Maestros</label>
                                    <div className={styles.check}></div>
                                </li>
                            </ul>

                            <ul className="d-flex justify-content-start h-100 flex-column">
                                <h3 className='text-center is-size-7 text-uppercase is-bold'>Modalidad</h3>
                                <li className='mb-3 mb-md-4'>
                                    <input type="checkbox" id="presencial" value="Presencial" onChange={handleChange} checked={checked.presencial} />
                                    <label htmlFor="presencial">Presencial</label>
                                    <div className={styles.check}></div>
                                </li>
                                <li className='mb-3 mb-md-4'>
                                    <input type="checkbox" id="online" value="Online" onChange={handleChange} checked={checked.online} />
                                    <label htmlFor="online">Online</label>
                                    <div className={`${styles.check}`}></div>
                                </li>
                            </ul>

                            <ul className="d-flex justify-content-start h-100 flex-column">
                                <h3 className='text-center is-size-7 text-uppercase is-bold'>Precio</h3>
                                <li className='mb-3 mb-md-4'>
                                    <input type="checkbox" id="gratis" value="Gratis" onChange={handleChange} checked={checked.gratis} />
                                    <label htmlFor="gratis">Sin costo</label>
                                    <div className={styles.check}></div>
                                </li>
                                <li className='mb-3 mb-md-4'>
                                    <input type="checkbox" id="donacion" value="Donación" onChange={handleChange} checked={checked.donacion} />
                                    <label htmlFor="donacion">Donación voluntaria</label>
                                    <div className={`${styles.check}`}></div>
                                </li>
                            </ul>
                        </div>

                        <div className='text-center'>
                            <input type="submit" value="ELEGIR" className={styles.btnSubmit} />
                        </div>

                    </form>
                </div>
            }

            {
                openFilterCalendar &&
                <div className={`${styles.filterCalendar}  fw-light is-size-9`}>
                    <div className='ps-1 pb-4 pb-md-0 d-flex justify-content-between align-items-center'>
                        <div>
                            Rangos de fecha
                            <span className={`${styles.pointer} " ps-3 text-black-50 "`} onClick={() => setOpenFilterCalendar(!openFilterCalendar)}>▼</span>
                        </div>
                        <Image src={close} onClick={handleCloseFilterDate} alt="Close" />
                    </div>
                    <form onSubmit={handleSubmit}>
                        <div className='d-flex justify-content-center flex-wrap'>
                            <ul className=" d-flex justify-content-between justify-content-md-start h-100">
                                <li className='mx-lg-5'>
                                    <div className={`${viewSelectMonth ? styles.showInput : styles.hideInput}`}>
                                        <input type="checkbox" id="mes" value="Mes" onChange={handleChangeInputMonth} />
                                        <label htmlFor="mes">Por Mes</label>
                                        <div className={styles.check}></div>
                                    </div>
                                </li>
                                <li className='mx-lg-5'>
                                    <div className={`${viewCalendar ? styles.showInput : styles.hideInput}`}>
                                        <input type="checkbox" id="rangoFecha" value="Rango Fecha" onChange={handleChangeInputDate} />
                                        <label htmlFor="rangoFecha">Por rango de fecha</label>
                                        <div className={styles.check}></div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        {
                            viewSelectMonth &&
                            <select
                                className={`form-select form-select-sm ${styles.filter_selectMonth}`}
                                aria-label="form-select-sm example"
                                size="6"
                                onChange={handleChange}
                            >
                                {
                                    months.map(({ id, month, text }) => (
                                        <option
                                            key={id}
                                            value={month}
                                            selected={selectedInit.text}
                                            className="is-size-8 is-regular"
                                        >{text}</option>
                                    ))
                                }
                            </select>
                        }
                        {
                            viewCalendar &&
                            <div className='d-flex justify-content-center'>
                                <DatePickerFilter
                                    valueDateSelect={valueDateSelect}
                                    handleChangeCalendar={handleChangeCalendar}
                                />
                            </div>
                        }
                        
                        <div className='d-flex justify-content-center flex-column align-items-center mt-4'>
                            <Button
                                type="submit"
                                className={`is-size-9 ${styles.btnNext}`}
                                // onClick={handleClose}
                            >
                                SELECCIONE FECHA
                                <Image src={arrowRigth} alt="flecha derecha" />
                            </Button>
                            <Button
                                className={`is-size-9 ${styles.btnClose}`}
                                onClick={() => setOpenFilterCalendar(!openFilterCalendar)}
                            >
                                CANCELAR
                            </Button>
                        </div>
                    </form>
                </div>
            }
        </>
    )
}
