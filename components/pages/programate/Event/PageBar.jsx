import Image from "next/image";
import Link from "next/link";
// Files
import arrow from '../../../../public/assets/arrow-left-white.png';
import arrowBlack from '../../../../public/assets/icon-arrow-left1.png';
import styles from "./Event.module.scss";

export default function PageBar({title, color, transparent=false}) {
    return (
        <div className={`${transparent ? styles.pagebarTransparent : styles.pagebarRelative} " container "` }>
            <Link href={'/programate'} passHref>
                <a href="" className={styles.btnLink}>
                    <Image src={transparent ? arrow : arrowBlack} alt="..." width="42px" height="42px"/>
                </a>
            </Link>
            <div className={`pb-4 ps-lg-1 d-flex ${styles.pagebar_h2}`}>
                <Link href={'/programate'} passHref>
                    <h2 className="mb-0 is-size-8 is-regular">Prográmate</h2> 
                </Link>                              
                <h2 className="mb-0 is-size-8 is-regular mx-1"> &gt; {title}</h2>
            </div>
        </div>
    )
}