// Components
import ButtonBar from "../../../ui/ButtonBar/ButtonBar";
import AditionalInfoBanner from "./AditionalInfoBanner";

// Styles
import styles from "./Event.module.scss";
import PageBar from "./PageBar";

const BannerEvent = ({ dataEvent }) => {
  const {
    title,
    location,
    img,
    date,
    description,
    time,
    quotas,
    modality,
    price,
  } = dataEvent;

  return (
    <div className={`${styles.bannerProgramming} d-flex flex-column `}>
      <PageBar title={title} transparent={true} />
      <div
        className={`container ${styles.infoBannerProgramming} text-white ps-md-auto`}
      >
        <h3 className="is-size-2 is-bold">{title}</h3>
        <h5 className="is-regular">{description}</h5>
        <ButtonBar
          text={"Quiero inscribirme"}
          colorBtn={"#6FBE75"}
          colorHr={"#F45858"}
        />
      </div>
      <AditionalInfoBanner
        modality={modality}
        location={location}
        date={date}
        time={time}
        quotas={quotas}
        price={price}
      />
    </div>
  );
};

export default BannerEvent;
