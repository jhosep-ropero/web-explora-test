import Image from "next/image";

// Data
import iconCalendar from "../../../../public/assets/programming/icon-calendar.png";
import iconLocation from "../../../../public/assets/programming/icon-location.png";
import iconTime from "../../../../public/assets/programming/icon-time.png";
import iconCapacity from "../../../../public/assets/programming/icon-capacity.png";
import iconPen from "../../../../public/assets/programming/icon-pen.png";
import iconFacebook from "../../../../public/assets/programming/icon-facebook.png";
import iconYoutube from "../../../../public/assets/programming/icon-youtube.png";
import iconMoney from "../../../../public/assets/programming/icon-money.png";

// Styles
import styles from "./Event.module.scss";

const AditionalInfoBanner = ({
  modality,
  location,
  date,
  time,
  quotas,
  price,
}) => {
  const separatedDate = date ? date.split("-") : ["2022", 1, "27"];

  const formatedDate = new Date(
    separatedDate[0],
    separatedDate[1] - 1,
    separatedDate[2]
  );
  return (
    <div className={`${styles.aditionalInfoBannerProgramming} `}>
      <div className="container d-flex flex-row flex-wrap justify-content-center justify-content-lg-start">
        <div className="d-flex align-items-start justify-content-md-center mb-2 mb-md-4 col-6 col-md-auto">
          <Image
            layout="intrinsic"
            src={iconPen}
            width={24.23}
            height={24.23}
            className="img-fluid"
            alt="logo lápiz"
          />
          <p className="mb-0">{modality}</p>
        </div>
        <div className="d-flex align-items-start justify-content-md-center mb-2 ml-2 mb-md-4 col-5 col-md-auto">
          <Image
            layout="intrinsic"
            src={iconLocation}
            width={20.89}
            height={24.89}
            className="img-fluid"
            alt="logo locacion"
          />
          <p className="mb-0">Lugar: {location.map((el) => el + " ")}</p>
        </div>

        <div className="d-flex align-items-start justify-content-md-center mb-2 mb-md-4 col-6 col-md-auto">
          <Image
            layout="intrinsic"
            src={iconCalendar}
            width={25.29}
            height={25.29}
            className="img-fluid"
            alt="logo calendario"
          />
          <p className="mb-0">
            {formatedDate.toLocaleString("default", {
              day: "2-digit",
            })}{" "}
            {formatedDate.toLocaleString("default", {
              month: "long",
            })}{" "}
            de
            {formatedDate.getFullYear()}
          </p>
        </div>
        <div className="d-flex align-items-start justify-content-md-center mb-2 mb-md-4 col-5 col-md-auto">
          <Image
            layout="intrinsic"
            src={iconTime}
            width={24.49}
            height={24.49}
            className="img-fluid"
            alt="logo hora"
          />
          <p className="mb-0">Hora: {time}</p>
        </div>

        <div className="d-flex align-items-start justify-content-md-center mb-2 mb-md-4 col-6 col-md-auto">
          <Image
            layout="intrinsic"
            src={iconCapacity}
            width={21.76}
            height={24.27}
            className="img-fluid"
            alt="logo cupos"
          />
          <p className="mb-0">{quotas} cupos</p>
        </div>
        <div className="d-flex align-items-start justify-content-md-center mb-2 mb-md-4 col-5 col-md-auto">
          <Image
            layout="intrinsic"
            src={iconMoney}
            width={27.76}
            height={27.76}
            className="img-fluid"
            alt="logo precio"
          />
          {price === "0" ? (
            <p className="mb-0">Costo: Sin costo</p>
          ) : (
            <p className={`mb-0 ${styles.aditionalInfoPrice}`}>
              Costo: {price} (por participante)
            </p>
          )}
        </div>

        <div
          className={`d-flex justify-content-center align-items-center mb-md-4 col-12 col-md-auto ${styles.socialButtons}`}
        >
          <Image
            layout="intrinsic"
            src={iconFacebook}
            width={11.5}
            height={24.69}
            className="img-fluid"
            alt="logo facebook"
          />
          <div className={`${styles.separator} mx-2`} />
          <Image
            layout="intrinsic"
            src={iconYoutube}
            width={24.57}
            height={19.66}
            className="img-fluid"
            alt="logo youtube"
          />
          <p className="mb-0">Vía Facebook live y youtube</p>
        </div>
      </div>
    </div>
  );
};

export default AditionalInfoBanner;
