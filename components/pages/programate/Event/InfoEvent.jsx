import Image from "next/image";

// Components
import ButtonBar from "../../../ui/ButtonBar/ButtonBar";

import iconFacebookBlack from "../../../../public/assets/programming/icon-facebook-black.png";
import iconYoutubeBlack from "../../../../public/assets/programming/icon-youtube-black.png";

// Styles
import styles from "./Event.module.scss";
import DOMPurify from "isomorphic-dompurify";

const InfoEvent = ({ descriptionExtend }) => {
  const { content, extra_content } = descriptionExtend;
  const sanitizer = DOMPurify.sanitize;

  return (
    <div className={styles.infoEvent}>
      <div className={`container ${styles.infoEventDescription}`}>
        <div className="row">
          <div
            className="col-6 text-content"
            dangerouslySetInnerHTML={{ __html: sanitizer(content) }}
          ></div>
          <div
            className="col-5 pl-1 text-content"
            dangerouslySetInnerHTML={{ __html: sanitizer(extra_content) }}
          ></div>
        </div>
        <div className="d-flex mb-4">
          <button
            className={`d-flex align-items-center ${styles.btnSocial} ${styles.marginButton}`}
          >
            <Image
              src={iconFacebookBlack}
              alt="Logo Facebook"
              className="img-fluid"
            />
            <span>Link para ver por Facebook</span>
          </button>
          <button
            className={`d-flex align-items-center ${styles.btnSocial} mx-md-5`}
          >
            <Image
              src={iconYoutubeBlack}
              alt="Logo Youtube"
              className="img-fluid"
            />
            <span>Ver por YouTube aquí</span>
          </button>
        </div>
        <ButtonBar
          text={"Quiero inscribirme"}
          colorBtn={"#6FBE75"}
          colorHr={"#F45858"}
        />
      </div>
    </div>
  );
};

export default InfoEvent;
