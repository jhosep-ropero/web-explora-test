import styles from "./BannerShop.module.scss"

export const BannerShop = ({ img, imgMobile, title, description, price, type }) => {
  return (
    <>
      <div
        className={`${styles.bannerCover} ${styles.isHiddenMobile}`}
        style={{
          background: ` url(/assets/taquilla/banner-compra.png)`,
        }}
      >
        <div className={`container ${styles.bannerContent}`}>
          <p className="text-white is-extrabold is-size-5">
            Combo Explora + Planetario / $48.000
          </p>
          <p className={`text-white is-size-8 is-regular ${styles.bannerDescription}`}>
            Incluye: Ingreso al Acuario + Exposición Dinosaurios, Sala Abierta +
            Acuario + Sala Tiempo + Sala Mente + Sala En Escena + Sala Música +
            Vivario y en el Planetario incluye ingreso una proyección en el Domo
            + experiencias del museo. Aplica para 2 días si en un día no termina
            el recorrido.
          </p>
        </div>
      </div>
      <div
        className={`${styles.bannerCover} ${styles.isMobile}`}
        style={{
          background: ` url(/assets/taquilla/banner-movil-compra.png)`,
        }}
      >
        <div className={styles.bannerContent}>
          <p className="text-white is-extrabold is-size-5">
            Combo Explora + <br />
            Planetario
          </p>
          <span className={`text-white is-extrabold ${styles.bannerPrice}`}>$48.000</span>
        </div>
      </div>
    </>
  );
};
