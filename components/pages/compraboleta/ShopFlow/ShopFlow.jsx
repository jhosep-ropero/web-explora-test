import React from "react";
import { ProgressBar } from "../../../ui/ProgressBar/ProgressBar";
import { CostumerData } from "../CostumerData/CostumerData";
import { PlanInformation } from "../PlanInformation/PlanInformation";
import { PlanSummary } from "../PlanSummary/PlanSummary";
import { useSelector, useDispatch } from "react-redux";
import { setTicketState } from "../../../../redux/actions/tickets";
import styles from "./ShopFlow.module.scss";
import ButtonBar from "../../../ui/ButtonBar/ButtonBar";
import Separator from "../../../ui/Separator/Separator";
import CarouselStore from "../../../modules/CarouselStore";
import { useState, useEffect } from "react";

export const ShopFlow = () => {
  const [buttonTitle, setButtonTitle] = useState("CONTINUAR CON LA COMPRA");
  const step = useSelector((state) => state.tickets.ticketState);
  const dispatch = useDispatch();
  const onClick = (step) => {
    dispatch(setTicketState(step));
  };

  const nextStep = () => {
    step < 3 && dispatch(setTicketState(step + 1));
  };

  const backStep = () => {
    step > 0 && dispatch(setTicketState(step - 1));
  };
  const changedState = (step) => {
    dispatch(setTicketState(step));
  };
  useEffect(() => {
    step == 0 && setButtonTitle("CONTINUAR CON LA COMPRA");
    step == 1 && setButtonTitle("RESUMEN DE LA COMPRA");
    step == 2 && setButtonTitle("FINALIZAR COMPRA");
  }, [step]);

  return (
    <div className="lg-container">
      <ProgressBar step={step} changeStep={changedState}>
        {step == 0 && <PlanInformation />}
        {step == 1 && <CostumerData />}
        {step == 2 && <PlanSummary />}
        {step == 3 && <PlanSummary />}
      </ProgressBar>
      {step == 2 && (
        <div className="container mt-5">
          <p className="is-bold is-size-4">Recomendaciones</p>
          <p className="is-regular is-size-8">
            *Para dar cumplimiento al decreto 1408 de 2021, deberás presentar el
            carné o el certificado digital de vacunación contra COVID-19
            (disponible aquí) al ingresar al Parque Explora y al Planetario. Los
            menores de 12 años se encuentran excentos de dicha medida. (Se debe
            tener aplicadas dos dosis en el caso de las personas mayores de 18
            años y mínimo una dosis en el caso de las personas de 12 a 18 años).
            <br />
            *Antes de comprar tu boleta planea tu visita consultando nuestras
            recomendaciones y protocolos de seguridad AQUÍ * Descarga AQUÍ el
            mapa de los espacios disponibles para la visita o recorridos
            habilitados.
            <br />
            * No abrimos los lunes, ni los martes después de festivos.
            <br />
            * Asegúrate de que el día que tienes planeado visitarnos sí estemos
            prestando servicio. Verifica nuestros horarios de operación AQUÍ
            <br />
            * Esta boleta tendrá vigencia solo para el día de tu reserva.
            <br />* La boletería electrónica no aplica para nuestros descuentos
            especiales. * Si tienes dudas o cualquier inconveniente con la
            compra de tu boleta, puedes escribirnos a
            liliana.monsalve@parqueexplora.org
          </p>
        </div>
      )}
      <div className={`is-size-8 ${styles.btnCenter}`} onClick={() => nextStep()}>
        <ButtonBar
          text={buttonTitle}
          colorBtn="#F45858"
          colorHr="#F9D51D"
          colorTxt="#fff"
        />
      </div>
    </div>
  );
};
