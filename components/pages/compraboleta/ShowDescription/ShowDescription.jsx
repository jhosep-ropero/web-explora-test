import Image from "next/image";
import React from "react";

export const ShowDescription = ({
  img,
  title,
  category,
  hour,
  description,
  onClick,
}) => {
  return (
    <div>
      <div className="py-2 text-end" onClick={onClick}>
        <i role={"button"} className="icon-explora icon-close fs-5" />
      </div>
      <div className="row">
        <div className="col-lg-5 col-sm-12">
          <Image
            src={"/assets/taquilla/poster1-show.png"}
            width={317}
            height={378}
            alt={title}
          />
        </div>
        <div className="col-lg-7 col-sm-12">
          <p className="">
            <span className="is-size-7 is-bold">{title}</span> * {category} -
            <span className="text-primary is-size-7 is-bold"> {hour}</span>
          </p>
          <p className="is-size-8">{description}</p>
        </div>
      </div>
      <div className="text-center py-5">
      <button className="btn btn-secondary is-size-7" onClick={onClick}>CERRAR</button>
      </div>
    </div>
  );
};
