import Image from "next/image";
import Ticket from "../../../../public/assets/taquilla/icon-tiquet.png";
import Money from "../../../../public/assets/taquilla/icon-money.png";
import People from "../../../../public/assets/taquilla/icon-people.png";
import { useSelector } from "react-redux";
import styles from "./PlanSummary.module.scss";

export const PlanSummary = ({ title, price = 48000 }) => {
  const tickets = useSelector((state) => state.tickets.ticketNumber);

  return (
    <div className="container py-lg-5 pt-5 pt-sm-5">
      <p className={`${styles.txtCompra} m-0 text-center is-mobile`}>
        3. RESUMEN DE LA COMPRA
      </p>
      <div className="row justify-content-start pt-5">
        <div className="col-lg-4 ">
          <div className="d-flex justify-content-center">
            <div>
              <Image src={Ticket} alt="Tiquet" />
            </div>
            <div className="px-3">
              <p className={`${styles.txtTitleSummary} `}>
                COMBO EXPLORA + <br />
                PLANETARIO
              </p>
              <p className={styles.txtSubtitle}>
                Ver detalles / Modificar plan
              </p>
            </div>
          </div>
        </div>
        <div className="col-lg-4 cm-3">
          <div className="d-flex justify-content-center">
            <div>
              <Image src={People} alt="People" />
            </div>
            <div className="px-3">
              <p className={styles.txtTitleSummary}>
                {tickets} ASISTENTES <br />
                REGISTRADOS
              </p>
              <p className={styles.txtSubtitle}>
                Ver detalles / Modificar plan
              </p>
            </div>
          </div>
        </div>
        <div className="col-lg-4 cm-3">
          <div className="d-flex justify-content-center">
            <div>
              <Image src={Money} alt="Money" />
            </div>
            <div className="px-3">
              <p className={styles.txtTitleSummary}>
                TOTAL TIQUETES <br />
                {`$${tickets * price}`}
              </p>
              <p className={styles.txtSubtitle}>
                Ver detalles / Modificar plan
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
