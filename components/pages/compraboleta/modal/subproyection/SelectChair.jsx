import Chair from "../../../../modules/Chair";
import DomoMap from "../../../../modules/DomoMap";
import styles from "./SelectChair.module.scss"

const SelectChair = ({ title, hour, chairs, bussyChairs }) => {
  return (
    <>
      <div className="row ">
        <div className={`col-lg-8 col-md-12 ${styles.mobileDomo}`}>
          <DomoMap />
        </div>
        <div className="col-lg-4 col-md-12">
          <div>
            <div className="mt-5">
              <p className="is-size-5 is-extrabold">SELECCIONE ASIENTO PARA:</p>
              <p className="is-size-7 is-bold m-0">
                HELIOS: RELATOS DEL VECINDARIO SOLAR
              </p>
              <p className="is-size-9 is-semibold">
                HORA: 5:00 <br /> ASIENTOS ADQUIRIDOS: 4
              </p>
            </div>
            <div className="mt-lg-5">
              <div className="py-2 d-flex align-items-center">
                <Chair state={0} isDynamic={false} />
                <p className="m-0 ps-4 is-size-8 is-regular">
                  Asientos disponibles
                </p>
              </div>
              <div className="py-2 d-flex align-items-center">
                <Chair state={2} isDynamic={false} />
                <p className="m-0 ps-4 is-size-8 is-regular">
                  Asientos ocupados
                </p>
              </div>
              <div className="py-2 d-flex align-items-center">
                <Chair state={1} isDynamic={false} />
                <p className="m-0 ps-4 is-size-8 is-regular">Mis asientos</p>
              </div>
            </div>
            <div className="d-flex align-items-start mt-4">
              <i className="icon-explora icon-clock fs-2 pe-3 "></i>
              <p className="is-size-8 is-regular col-8">
                Recuerde que la programación del parque comienza desde las 9:00
                a.m hasta las 6:00 p.m. Procure llegar con el tiempo suficiente
                para disfrutar la programación del parque y también asistir a la
                proyección del domo
              </p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default SelectChair;
