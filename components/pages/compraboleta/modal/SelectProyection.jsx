import { Button, Modal } from "react-bootstrap";
import { domo } from "../../../../data/domo";
import { useState } from "react";

import styles from "./SelectProyection.module.scss";
// import SelectChair from "./subproyection/SelectChair";
// import ShowDescription from "../ShowDescription/ShowDescription.jsx";
import Radiobuttons from "../../../ui/RadioButtons/Radiobuttons";
import { SelectChair } from "./SelectChair/SelectChair";
import { useSelector } from "react-redux";
import { ShowDescription } from "../ShowDescription/ShowDescription";

export const SelectProyection = () => {
  const proyection = useSelector((state) => state.tickets.ticketProyection);
  const step = 0;
  const [visibility, setVisibility] = useState(1);
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [showDetails, setShowDetails] = useState({
    state: 0,
    id: "",
  });

  const onClick = (e) => {
    showDetails.state == 0
      ? setShowDetails({
          state: 1,
          id: e.target.id,
        })
      : setShowDetails({
          state: 0,
          id: e.target.id,
        });
  };

  return (
    <div className="d-xl-flex is-bold is-size-7 is-color-black">
      <p className="m-0 p-0 is-size-7">PROYECCIÓN DOMO</p>
      <Button
        variant="secondary"
        className={`d-flex justify-content-between align-items-center ms-lg-4 px-3 is-size-8 ${styles.btnGeneral}`}
        onClick={handleShow}
      >
        {proyection ? (
          `${proyection}`
        ) : (
          <p className="m-0 ps-3">No hay selcción</p>
        )}
        {/* <p className="m-0 ">No hay selcción</p> */}
        <i className="icon-explora icon-triangle-bot h6 my-auto" />
      </Button>

      <Modal
        show={show}
        onHide={handleClose}
        className={visibility ? "" : "d-none"}
        dialogClassName={styles.modal40w}
      >
        <Modal.Body>
          <div 
          className={
            !showDetails.state ? `text-end px-lg-4 pt-lg-3` : "d-none"
          }
          onClick={handleClose}
          >
            <i role={"button"} className="icon-explora icon-close" />
          </div>
          <p
            className={
              !showDetails.state ? `my-4 txt-compra text-center` : "d-none"
            }
          >
            PROGRAMACIÓN SHOW DOMO <br /> HASTA EL 16 DE ENERO
          </p>
          {domo &&
            domo.map((data, index) => (
              <div key={data.id}>
                <div>
                  <div
                    className={!showDetails.state ? `row px-lg-5` : "d-none"}
                  >
                    <div className="col-10">
                      <Radiobuttons
                        id={data.id}
                        hour={data.hour}
                        title={data.title}
                        category={data.category}
                      />
                    </div>
                    <div
                      id={data.id}
                      className="col-2 align-self-center"
                      onClick={(e) => onClick(e)}
                    >
                      <i
                        id={data.id}
                        className="icon-explora icon-eye"
                        color="#B7B7C1"
                      ></i>
                    </div>
                  </div>
                </div>
                <div className={!showDetails.state ? "d-none" : "row px-lg-5"}>
                  <div className={showDetails.id == data.id ? "" : "d-none"}>
                    <ShowDescription
                      img={data.img}
                      title={data.title}
                      category={data.category}
                      hour={data.hour}
                      description={data.description}
                      onClick={onClick}
                    />
                  </div>
                </div>
              </div>
            ))}
        </Modal.Body>
        <Modal.Footer
          className={
            !!showDetails.state == 0
              ? `d-flex flex-column align-self-start mx-lg-5 border-0`
              : "d-none"
          }
        >
          <div onClick={() => setVisibility(0)}>
            <SelectChair setVisibility={setVisibility} closeAll={handleClose} />
          </div>
          <Button
            variant="secondary"
            className={styles.btnClose}
            onClick={handleClose}
          >
            CERRAR
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};
