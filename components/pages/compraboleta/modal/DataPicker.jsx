import { useState } from "react";
import { useSelector } from "react-redux";
import { Button, Modal } from "react-bootstrap";

import close from "../../../../public/assets/taquilla/close.png";
import styles from "./SelectProyection.module.scss";

import { DatePicker } from "../../../ui/datepicker/DatePicker";
import { useEffect } from "react";

export const DataPicker = () => {
    const date = useSelector((state) => state.tickets.ticketDate);
  
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  
  const [selectedDate, setSelectedDate] = useState({
    month: "",
    day: "",
    year: "",
  });

  useEffect(() => {
    if (date) {
      let newDate = new Date(date)
      if (newDate.getMonth() + 1 < 10) {
        if (newDate.getDate() < 10) {
          setSelectedDate({
            ...selectedDate,
            month: `0${newDate.getMonth() + 1}`,
            day: `0${newDate.getDate()}`,
            year: `${newDate.getFullYear()}`,
          });
        } else {
          setSelectedDate({
            ...selectedDate,
            month: `0${newDate.getMonth() + 1}`,
            day: `${newDate.getDate()}`,
            year: `${newDate.getFullYear()}`,
          });
        }
      } else {
        setSelectedDate({
          ...selectedDate,
          month: `${newDate.getMonth() + 1}`,
          day: `${newDate.getDate()}`,
          year: `${newDate.getFullYear()}`,
        });
      }
    }

  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [date]);
  

  return (
    <div className="d-flex is-bold is-size-7 is-color-black  my-5">
      <p className="m-0 p-0 text-center">ELEGIR FECHA</p>
      <Button variant="secondary" className="d-flex align-items-center ms-4 is-size-8" onClick={handleShow}>
        {!date ? "Seleccionar Fecha" : `${selectedDate.month}/${selectedDate.day}/${selectedDate.year}`}
        <i className="icon-explora icon-triangle-bot ms-3 fs-6" />
      </Button>

      <Modal show={show} onHide={handleClose} className="modal-xxl">
        <Modal.Body>
          <div className="d-flex mt-4 mb-3 mx-lg-5 justify-content-between align-items-center">
            <div className="d-flex align-items-center">
              <div className="isDisable me-3"></div>
              <p className="p-0 m-0 is-size-9">Fechas no disponibles</p>
            </div>
            <i role={"button"} className="icon-explora icon-close" src={close} onClick={handleClose}/>
          </div>
          <DatePicker/>

        </Modal.Body>
        <Modal.Footer className="d-flex flex-column align-self-center px-5 mx-5 border-0">
          <Button
            variant="primary"
            className={styles.btnNext}
            onClick={handleClose}
          >
            SELECCIONE FECHA
            <i role={"button"} className="icon-explora icon-arrow-right fs-5" src={close} onClick={handleClose}/>
          </Button>
          <Button className={styles.btnClose} onClick={handleClose}>
            CANCELAR
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};
