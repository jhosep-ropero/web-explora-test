import { useState } from "react";
import { useSelector } from "react-redux";
import { Button, Modal } from "react-bootstrap";

import Image from "next/image";
import arrowDown from "../../../../public/assets/taquilla/arrow-down.png";
import arrowRigth from "../../../../public/assets/taquilla/arrow-rigth-white.png";
import img from "../../../../public/assets/taquilla/poster-proyection.png";

import Radiobuttons from "../../../ui/RadioButtons/Radiobuttons";
import { domo } from "../../../../data/domo";
import { useEffect } from "react";
import styles from "./SelectProyection.module.scss";
import SelectChair from "./subproyection/SelectChair";
import ShowDescription from "../ShowDescription/ShowDescription.jsx";

export const SelectProyection = () => {
  const [step, setStep] = useState(0);
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const state = useSelector((state) => state.tickets.ticketProyection);
  const [showDetails, setShowDetails] = useState({
    state: 0,
    id: "",
  });

  const nextStep = () => {
    step == 0 ? setStep(1) : (handleClose(), setStep(0));
  };
  const prevStep = () => {
    step == 1 && setStep(0)
  };

  const onClick = (e) => {
    showDetails.state == 0
      ? setShowDetails({
          state: 1,
          id: e.target.id,
        })
      : setShowDetails({
          state: 0,
          id: e.target.id,
        });
  };

  return (
    <div className="d-xl-flex is-bold is-size-7 is-color-black">
      <p className="m-0 p-0">PROYECCIÓN DOMO</p>
      <Button
        variant="secondary"
        className={`d-flex justify-content-between ms-lg-4 ${styles.btnGeneral}`}
        onClick={handleShow}
      >
        {state ? `${state}` : <p className="m-0 ps-3">No hay selección</p>}
        <div className="ms-2">
          <Image src={arrowDown} alt="flecha abajo" />
        </div>
      </Button>

      <Modal
        show={show}
        onHide={handleClose}
        dialogClassName={step == 0 ? styles.modal40w : styles.modal90w}
      >
        <Modal.Body>
          {step == 0 && (
            <div className="d-flex flex-column">
              <p
                className={
                  showDetails.state == 0
                    ? "my-5 txt-compra text-center"
                    : "d-none"
                }
              >
                PROGRAMACIÓN SHOW DOMO <br /> HASTA EL 16 DE ENERO
              </p>
              {domo &&
                domo.map((data) => (
                  <div key={data.id}>
                    <div
                      className={showDetails.state == 1 ? "d-none" : "d-flex"}
                    >
                      <Radiobuttons
                        id={data.id}
                        hour={data.hour}
                        title={data.title}
                        category={data.category}
                      />
                      <div
                        className="align-self-center ps-3"
                        onClick={(e) => onClick(e)}
                      >
                        <i className="icon-eye"></i>
                        {/* <Image id={data.id} src={eye} alt="Icono Ojo" /> */}
                      </div>
                    </div>
                    <div className={showDetails.id == data.id ? "" : "d-none"}>
                      <div className="d-flex justify-content-end px-3 pb-lg-5 pb-sm-3">
                        <button> X </button>
                      </div>
                      <ShowDescription
                        img={img}
                        title={data.title}
                        description={data.description}
                        hour={data.hour}
                        category={data.category}
                      />
                      <div className="d-flex justify-content-center">
                        <Button
                          className={styles.btnBack}
                          onClick={(e) => onClick(e)}
                        >
                          CERRAR
                        </Button>
                      </div>
                    </div>
                  </div>
                ))}
              <div className={showDetails.state == 1 ? "d-none" : styles.infoBox}>
                <i className="icon-explora icon-clock fs-2 pe-3 "></i>
                <p className="p-0 m-0 is-size-9 ">
                  Recuerde que la programación del parque comienza desde las
                  9:00 a.m hasta las 6:00 p.m. Procure llegar con el tiempo
                  suficiente para disfrutar la programación del parque y también
                  asistir a la proyección del domo
                </p>
              </div>
            </div>
          )}

          {step == 1 && <SelectChair prev={prevStep} close={handleClose} />}
        </Modal.Body>
        <Modal.Footer
          className={
            step == 0
              ? `d-flex flex-column align-self-start mx-5 border-0`
              : "d-flex flex-column align-self-center mx-5 border-0"
          }
        >
          <Button
            variant="primary"
            className={
              showDetails.state == 0 ? styles.btnNext : "d-none"
            }
            onClick={nextStep}
          >
            CONTINUAR
            <Image src={arrowRigth} alt="flecha derecha" />
          </Button>
          <Button
            className={
              showDetails.state == 0 ? styles.btnClose : "d-none"
            }
            onClick={handleClose}
          >
            CANCELAR
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};
