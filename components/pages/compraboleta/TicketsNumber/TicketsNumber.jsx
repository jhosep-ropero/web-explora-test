import { useDispatch, useSelector } from "react-redux";
import { setTicketNumber } from "../../../../redux/actions/tickets";
import styles from "./TicketsNumber.module.scss";

export const TicketsNumber = ({ price = 48000 }) => {
  const tickets = useSelector((state) => state.tickets.ticketNumber);
  const dispatch = useDispatch();

  const addTickets = () => {
    dispatch(setTicketNumber(tickets + 1));
  };

  const removeTickets = () => {
    tickets > 1 && dispatch(setTicketNumber(tickets - 1));
  };

  return (
    <>
      <div className="d-flex is-bold is-size-7 is-color-black ">
        <div className="d-flex">
          <div className="">
            <p className="m-0 p-0 ">NÚMERO DE TIQUETES</p>
            <p className="is-regular is-size-8 is-color-black">
              Total: ${tickets * price}
            </p>
          </div>
          <div className={`${styles.isHiddenMobile}`}>
            <div className="d-flex gap-1 ">
              <div className={styles.box}>
                <input type="text" value={tickets} />
              </div>
              <div className="d-flex flex-column ">
                <button
                  type="button"
                  className={styles.btnArrow}
                  onClick={addTickets}
                >
                  <i className="icon-explora icon-triangle-top fs-6" />
                </button>
                <button
                  type="button"
                  className={styles.btnArrow}
                  onClick={removeTickets}
                >
                  <i className="icon-explora icon-triangle-bot fs-6" />
                </button>
              </div>
            </div>
          </div>
          <div className="d-flex ms-5 d-lg-none d-md-none">
            <div className="d-flex gap-1 flex-column">
              <button
                type="button"
                className={`btn rounded-0 btn-secondary`}
                onClick={addTickets}
              >
                <i className="icon-explora icon-triangle-top fs-6" />
              </button>
              <button
                type="button"
                className={`btn rounded-0 btn-secondary`}
                onClick={removeTickets}
              >
                <i className="icon-explora icon-triangle-top fs-6 " />
              </button>
            </div>
            <div className="counter-box">
              <p className="m-0 p-0">{tickets}</p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
