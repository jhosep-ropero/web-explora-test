import { Formik, Field, Form } from "formik";
import React from "react";
import styles from "./CostumerData.module.scss";
// import { validationSchema } from "../../../../data/validationSchema";

import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import {
  setTicketPhoneNumber,
  setTicketName,
  setTicketLastName,
  setTicketEmail,
  setTicketId,
} from "../../../../redux/actions/tickets";

export const CostumerData = () => {

  const name = useSelector((state) => state.tickets.ticketName);
  const lastName = useSelector((state) => state.tickets.ticketLastName);
  const phoneNumber = useSelector((state) => state.tickets.ticketPhoneNumber);
  const email = useSelector((state) => state.tickets.ticketEmail);
  const id = useSelector((state) => state.tickets.ticketId);
  const dispatch = useDispatch();

  const onChange = (field) => {
    switch (field.target.id) {
      case "name":
        dispatch(setTicketName(field.target.value));
        break;
      case "lastname":
        dispatch(setTicketLastName(field.target.value));
        break;
      case "phoneNumber":
        dispatch(setTicketPhoneNumber(field.target.value));
        break;
      case "email":
        dispatch(setTicketEmail(field.target.value));
        break;
      case "id":
        dispatch(setTicketId(field.target.value));
        break;
      default:
        break;
    }
    console.log(field.target.value);
  };
  const isString = (value) => {
    if (!/^[a-zA-ZÀ-ÿ\s]{1,40}$/.test(value)) {
      return "El campo es invalido";
    }
  };

  const isNumber = (value) => {
    if (!/^[0-9]*$/.test(value)) {
      return "El campo es Invalido";
    }
  };

  const isValidEmail = (value) => {
    if (!/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/.test(value)) {
      return "El correo electrónico no es valido";
    }
  };
  const validationSchema = (values) => {
    let errors = {};
    !values.firstName
      ? (errors.firstName = "Este campo es requerido")
      : (errors.firstName = isString(values.firstName));

    !values.lastName
      ? (errors.lastName = "Este campo es requerido")
      : (errors.lastName = isString(values.lastName));

    !values.phoneNumber
      ? (errors.phoneNumber = "Este campo es requerido")
      : (errors.phoneNumber = isNumber(values.phoneNumber));

    !values.identification
      ? (errors.identification = "Este campo es requerido")
      : (errors.identification = isNumber(values.identification));

    !values.email
      ? (errors.email = "Este campo es requerido")
      : (errors.email = isValidEmail(values.email));

    return errors;
  };

  return (
    <Formik
      initialValues={{
        firstName: "",
        lastName: "",
        phoneNumber: "",
        email: "",
        identification: "",
      }}
      validate={(values) => validationSchema(values)}
    >
      {({
        errors,
        values,
        touched,
        handleBlur,
        handleChange,
        handleSubmit,
      }) => (
        <div className="container-lg my-5 p-5">
          <p className="txt-compra text-center is-mobile">
            2. DATOS DEL COMPRADOR
          </p>

          <Form>
            <div className="row py-lg-3">
              <div className="col-lg-6 col-md-12 cm-3 is-size-8">
                <label htmlFor="firstName" className="form-label">
                  Nombre
                </label>
                <Field
                  id="firstName"
                  className="form-control rounded-0"
                  name="firstName"
                  value={values.firstName}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
                {touched.firstName && errors.firstName && (
                  <div className={styles.error}>
                    <div className={styles.dot}></div> {errors.firstName}
                  </div>
                )}
              </div>
              <div className="col-lg-6 col-md-12 cm-3 is-size-8">
                <label htmlFor="lastName" className="form-label">
                  Apellido
                </label>
                <Field
                  id="lastName"
                  className="form-control rounded-0"
                  name="lastName"
                  value={values.lastName}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
                {touched.lastName && errors.lastName && (
                  <div className={styles.error}>
                    <div className={styles.dot}></div>
                    {errors.lastName}
                  </div>
                )}
              </div>
            </div>
            <div className="row py-lg-3">
              <div className="col-lg-6 col-md-12 cm-3 is-size-8">
                <label htmlFor="phoneNumber" className="form-label">
                  Número telefónico
                </label>
                <Field
                  id="phoneNumber"
                  type="number"
                  className="form-control rounded-0"
                  name="phoneNumber"
                  value={values.phoneNumber}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
                {touched.phoneNumber && errors.phoneNumber && (
                  <div className={styles.error}>
                    <div className={styles.dot}></div>
                    {errors.phoneNumber}
                  </div>
                )}
              </div>
              <div className="col-lg-6 col-md-12 cm-3 is-size-8">
                <label htmlFor="email" className="form-label">
                  Correo electrónico
                </label>
                <Field
                  id="email"
                  className="form-control rounded-0"
                  name="email"
                  value={values.email}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
                {touched.email && errors.email && (
                  <div className={styles.error}>
                    <div className={styles.dot}></div>
                    {errors.email}
                  </div>
                )}
              </div>
            </div>
            <div className="row py-lg-3 justify-content-start">
              <div className="col-lg-6 col-sm-12 cm-3 is-size-8">
                <label htmlFor="identification" className="form-label">
                  Doc. de identidad
                </label>
                <Field
                  id="identification"
                  type="number"
                  className="form-control rounded-0"
                  name="identification"
                  value={values.identification}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
                {touched.identification && errors.identification && (
                  <div className={styles.error}>
                    <div className={styles.dot}></div>
                    {errors.identification}
                  </div>
                )}
              </div>
            </div>
            {/* <button type="submit">Submit</button> */}
          </Form>
        </div>
      )}
    </Formik>
  );
};
