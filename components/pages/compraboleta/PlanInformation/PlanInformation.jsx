import React, { useState } from "react";
import { DataPicker } from "../modal/DataPicker";
import { SelectProyection } from "../modal/SelectProyection";
import { TicketsNumber } from "../TicketsNumber/TicketsNumber";
import styles from "./PlanInformation.module.scss";

export const PlanInformation = () => {
  return (
    <div className="container-sm">
      <div className={styles.title}>
        <p className={`txt-compra text-center mb-5 is-mobile`}>
          1. INFORMACIÓN DEL PLAN
        </p>
        <div className="d-lg-flex justify-content-between align-items-center align-self-center">
          <DataPicker />
          <div className={styles.ticketNumber}>
            <TicketsNumber />
          </div>
          <div className={styles.selectProyection}>
            <SelectProyection />
          </div>
        </div>
      </div>
    </div>
  );
};
