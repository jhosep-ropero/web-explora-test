import Link from 'next/link'
import React from 'react'

export default function AboutPlanetary() {

return (
        <div id='WhatIsPlanetary' className='position-relative py-5 bg-dark text-white fw-light bg-cover' style={{backgroundImage: `url('../assets/planetary/domo.png')`}}>
            <div className='py-3'>
                <div className='container py-5' style={{backgroundImage: `url(../../assets/icons/bg-plus.svg)` ,backgroundSize: `cover`}}>
                    <div className='row d-flex justify-content-center py-5'>
                        <div className='col-lg-8 text-center py-5'>
                            <h2 className='fs-1 mb-3 fw-light mt-5' style={{color: `#EFC78C`}}>¿Qué es Planetario?</h2>
                            <p className='mb-5'>
                            El Planetario de Medellín Jesús Emilio Ramírez González ofrece a sus visitantes un escenario a la altura de los más modernos del mundo, con un domo digital para experiencias de inmersión de alta calidad. Trocamos la mirada de los planetarios tradicionales hacia las ciencias del espacio, e incorporamos las ciencias de la tierra para observar, entre el pasmo y la esperanza, nuestro desconocido planeta.
                            </p>
                            <Link href='/planetario' passHref>
                                <a href="" className='btn btn-primary mb-5 py-3 px-4 fw-bold text-uppercase fs-6'>
                                    Quiero saber más
                                    <i className='icon-explora icon-plus-b ms-3 fs-6' />
                                </a>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
