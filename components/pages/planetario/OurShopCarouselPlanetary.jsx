import Link from 'next/link'
import React from 'react'
import Slider from "react-slick";
import CardStorePlanetary from "../../modules/CardStorePlanetary";
import { storePlanetary } from "../../../data/storePlanetary";


export default function OurShopCarouselPlanetary ({colorSeparator}) {
    const settings = {
        dots: false,
        infinite: false,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 4,
        initialSlide: 0,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              initialSlide: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
      };
    return (
      <div id='OurShopPlanetary' className='text-white py-5' style={{backgroundColor: `#122227`}}>
        <div className='container py-3'>
          <h2 className={`sectiontitle fw-light text-uppercase fs-4 mb-5 position-relative d-inline-block`}>Nuestra tienda</h2>
        </div>
        <div className="section-carousel">
            <Slider {...settings} className="container">
              {storePlanetary.map(st =>(
                <CardStorePlanetary
                  key={st.id} 
                  img={st.img} 
                  title={st.title} 
                  price={st.price}
                  color={st.color}
                  size={st.size}
                />
              ))}
            </Slider>
        </div>
        <div className='container d-flex justify-content-center py-3'>
          <Link href='/planetario' passHref>
              <a href="" className='btn btn-warning m-5 py-3 px-4 fw-bold text-uppercase fs-6'>
                  Ir a la tienda
                  <i className='icon-explora icon-plus-b ms-3 fs-6' />
              </a>
          </Link>
        </div>
      </div>
    )
}