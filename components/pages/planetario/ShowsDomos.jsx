import Link from 'next/link';
import React from 'react';
import styles from "./Domos.module.scss";

export default function ShowsDomos ({
    backgroundItem1 = "url('../../assets/planetary/domos/lucia.png')",
    titleItem1 = 'Lucía, el secreto de las estrellas fugaces',
    tagItem1 = 'Categoría Infantil',
    colorTag1 = '#28E7EF',
    backgroundItem2 = "url('../../assets/slider-space2.png')",
    titleItem2 = 'Helios: relatos del vecindario solar',
    tagItem2,
    colorTag2,
    backgroundItem3 = "url('../../assets/planetary/domos/sol.png')",
    titleItem3 = 'El Sol: nuestra estrella de vida',
    tagItem3 = '* Nuevo show',
    colorTag3 = '#EFC78C',
    backgroundItem4 = "url('../../assets/planetary/domos/aliens.png')",
    titleItem4 = 'Somos Aliens',
    tagItem4,
    colorTag4,
    backgroundItem5 = "url('../../assets/planetary/domos/bg-arrecifes.jpg')",
    titleItem5 = 'Arrecifes',
    tagItem5,
    colorTag5,
})
 {
    return (
        <div id='ShowDomos' className={`text-white py-5 ${styles.bgdomos}`}>
            <div className='container-xl'>
                <h2 className={`sectiontitle fw-light text-uppercase fs-4 py-5 position-relative d-inline-block`}>Shows Domo</h2>
                <div className={styles.domos}>
                    <div className={`position-relative ${styles.item1}`} style={{backgroundImage: `${backgroundItem1}`}}>
                        <div>
                            <span className='tag fs-5' style={{color: `${colorTag1}`}}>{tagItem1}</span>
                            <h3 className='fs-4 fw-light'>{titleItem1}</h3>
                        </div>
                        <Link href='/planetario' passHref>
                            <a href="" className='btn btn-outline-light border'>Ver más</a>
                        </Link>
                        <div className={`overlay position-absolute top-0 start-0 w-100 h-100 p-5 ${styles.overlay}`}></div>
                    </div>
                    <div className={`position-relative ${styles.item2}`} style={{backgroundImage: `${backgroundItem2}`}}>
                        <div>
                            <span className='tag fs-5' style={{color: `${colorTag2}`}}>{tagItem2}</span>
                            <h3 className='fs-4 fw-light'>{titleItem2}</h3>
                        </div>
                        <Link href='/planetario' passHref>
                            <a href="" className='btn btn-outline-light border'>Ver más</a>
                        </Link>
                        <div className={`overlay position-absolute top-0 start-0 w-100 h-100 p-5 ${styles.overlay}`}></div>
                    </div>
                    <div className={`position-relative ${styles.item3}`} style={{backgroundImage: `${backgroundItem3}`}}>
                        <div>
                            <span className='tag fs-5' style={{color: `${colorTag3}`}}>{tagItem3}</span>
                            <h3 className='fs-4 fw-light'>{titleItem3}</h3>
                        </div>
                        <Link href='/planetario' passHref>
                            <a href="" className='btn btn-outline-light border'>Ver más</a>
                        </Link>
                        <div className={`overlay position-absolute top-0 start-0 w-100 h-100 p-5 ${styles.overlay}`}></div>
                    </div>
                    <div className={`position-relative ${styles.item4}`} style={{backgroundImage: `${backgroundItem4}`}}>
                        <div>
                            <span className='tag fs-5' style={{color: `${colorTag4}`}}>{tagItem4}</span>
                            <h3 className='fs-4 fw-light'>{titleItem4}</h3>
                        </div>
                        <Link href='/planetario' passHref>
                            <a href="" className='btn btn-outline-light border'>Ver más</a>
                        </Link>
                        <div className={`overlay position-absolute top-0 start-0 w-100 h-100 p-5 ${styles.overlay}`}></div>
                    </div>
                    <div className={`position-relative ${styles.item5}`} style={{backgroundImage: `${backgroundItem5}`}}>
                        <div>
                            <span className='tag fs-5' style={{color: `${colorTag5}`}}>{tagItem5}</span>
                            <h3 className='fs-4 fw-light'>{titleItem5}</h3>
                        </div>
                        <Link href='/planetario' passHref>
                            <a href="" className='btn btn-outline-light border'>Ver más</a>
                        </Link>
                        <div className={`overlay position-absolute top-0 start-0 w-100 h-100 p-5 ${styles.overlay}`}></div>
                    </div>
                </div>
                <Link href='/planetario' passHref>
                    <a href="" className='btn btn-primary text-uppercase my-5 d-table mx-auto fw-bold fs-6 py-3 px-4'>
                        Todos los shows
                        <i className='icon-explora icon-plus-b ms-3 fs-6' />
                        </a>
                </Link>
            </div>
        </div>
  );
}