import SliderFullWidth from "../../modules/SliderFullWidth";
import Slider from "react-slick";
import InformationBar from "../../modules/InformationBar";
import { Fragment } from "react";

export default function MainSliderPlanetary() {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
  };
  return (
    <div>
    <Slider {...settings} className='mainsliderplanetary'>
      <SliderFullWidth
        minHeightSlider = '100vh'
        videoBackground= "../../assets/videos/home-planetario.mp4"
        title = {<Fragment>Experiencias <br/>innmersivas de <br/>alta calidad</Fragment>}
        textButton = "funciones"
        linkButton = '/planetario'
      />
      <SliderFullWidth
        minHeightSlider = '100vh'
        imgBackground= {'url(assets/planetary/mainslider/planetario-medellin.jpg)'}
        title = {<Fragment>El Planetario <br/>tiene nuevos <br/>planes para ti</Fragment>}
        textButton = "funciones"
        linkButton = '/planetario'
      />
    </Slider>
    <InformationBar
      moreStyle = 'text-white position-absolute bottom-0 end-0 border-top border-start bg-transparent'
      iconCol1 = 'icon-planet fs-4'
      titleCol1 = 'Planetario'
      texxtCol1 = 'Quiénes Somos'
      iconCol2 = 'icon-clock-b fs-4'
      titleCol2 = 'Horarios y Tarifas'
      texxtCol2 = 'Hoy 8:00 a.m - 6:00 p.m'
      iconCol3 = 'icon-location-b fs-4'
      titleCol3 = 'Cómo llegar'
      texxtCol3 = 'Ubicación'
      linkAbout = ''
      linkSchedule = ''
      linkLocation = ''
    />
    </div>
  );
}
