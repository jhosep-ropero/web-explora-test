import Slider from "react-slick";
import SliderFullWidth from "../../modules/SliderFullWidth"
import { planetaryexhibitions } from "../../../data/planetaryexhibitions";
import { Fragment } from "react";

export default function Exhibitions() {
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        appendDots: dots => (
          <div
            style={{
              bottom: '2px',
              margin: 'auto'
              
            }}
          >
            <div className="container my-5 p-0">
            <ul style={{ margin: "0px" }}> {dots} </ul>
            </div>
          </div>
        ),
        customPaging: i => (
          <div
            className="p-3"
            style={{
              width: "auto",
              color: "white",
              borderRight: `${i == 2 ? '' : '1px solid #a7a7a7'}`,
            }}
          >
            { 
              <div className="d-flex justify-content-center align-items-center act pt-2">
                <h3 className="position-relative fs-1 pe-1 pe-sm-3 fw-lighter">
                  {'0' + (i + 1)}
                  <span className="colorline"></span>
                </h3>
                <p className="position-relative text-start d-none d-lg-block fw-light fs-5 m-0">
                  {planetaryexhibitions[i].titleSlick}
                  <span className="colorline"></span>
                </p>
              </div>  
            }
          </div>
        )
      };
    return (
        <div id="Exhibitions" className="slick-tabs">
          <div className="position-absolute w-100">
            <div className="position-relative container py-5">
              <h2 className={`sectiontitle fw-light text-uppercase fs-4 py-5 position-relative d-inline-block text-white`} style={{zIndex: `1`}}>Exhibiciones</h2>
            </div>
          </div>
          <Slider {...settings} className='sliderplanetaryexhibitions'>
            <SliderFullWidth
              minHeightSlider = '900px'
              imgBackground= 'url(../../assets/planetary/museo-planetario.png)'
              title = {<Fragment>Ven y visita nuestro <br />Museo Planetario</Fragment>}
              textButton = 'Horarios'
              linkButton = '/planetario'
            />
            <SliderFullWidth
              minHeightSlider = '900px'
              imgBackground= 'url(../../assets/planetary/biblioteca-planetario.png)'
              title = {<Fragment>Conoce nuestra <br />biblioteca</Fragment>}
              textButton = 'Horarios'
              linkButton = '/planetario'
            />
            <SliderFullWidth
              minHeightSlider = '900px'
              imgBackground= 'url(../../assets/planetary/elsol.png)'
              title = {<Fragment>Ven y visita nuestro <br />Museo Planetario</Fragment>}
              textButton = 'Horarios'
              linkButton = '/planetario'
            />                
          </Slider>
        </div>
    )
}
