import { useDispatch, useSelector } from "react-redux";
import Hbar from "../../ui/Hbar/Hbar";
import Separator from "../../ui/Separator/Separator";
import { uiTabFacebook, uiTabInstagram, uiTabTwitter } from "../../../redux/actions/ui";

export default function ExploraRedes() {
    const { facebookActive, instagramActive, twitterActive } = useSelector((state) => state.uiTabRedes);
    const dispatch = useDispatch();
    return (
        <div className="section-home06 mt-5">
            <Separator color='#F45858'/>
            <div className="container px-4 py-5">
                <Hbar color={'#F45858'} title='Explora en redes'/>
            
                <section className="row text-secondary mt-4 align-items-center text-center px-2">
                <div
                    className={`col-4 col-lg-2 mb-3 text-start px-1`}
                >
                    <button onClick={() => dispatch(uiTabFacebook())}>
                    Facebook
                    {facebookActive && (
                        <hr className="animate__animated animate__fadeInLeft" />
                    )}
                    </button>
                </div>
                <div
                    className={`col-4 col-lg-2 mb-3 text-start`}
                >
                    <button onClick={() => dispatch(uiTabInstagram())}>
                    Instagram
                    {instagramActive && (
                        <hr className="animate__animated animate__fadeInLeft" />
                    )}
                    </button>
                </div>
                <div
                    className={`${twitterActive && "active"
                    } col-4 col-lg-2 mb-3 text-start`}
                >
                    <button onClick={() => dispatch(uiTabTwitter())}>
                    Twitter
                    {twitterActive && (
                        <hr className="animate__animated animate__fadeInLeft" />
                    )}
                    </button>
                </div>
                
                </section>
                
            </div>
        </div>
    )
}
