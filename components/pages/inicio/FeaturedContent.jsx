import Slider from "react-slick";
import Hbar from "../../ui/Hbar/Hbar";
import SliderFullWidth from "../../modules/SliderFullWidth"
import { featured } from "../../../data/featured";
import { Fragment } from "react";

export default function FeaturedContent() {
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        appendDots: dots => (
          <div
            style={{
              bottom: '2px',
              margin: 'auto'
              
            }}
          >
            <div className="container"
              style={{
                backgroundColor: "#70707036",
                borderRadius: "1px",
                backdropFilter: 'blur(30px)'
                
              }}
            >
            <ul style={{ margin: "0px" }}> {dots} </ul>
            </div>
          </div>
        ),
        customPaging: i => (
          <div
            className="p-4"
            style={{
              width: "auto",
              color: "white",
              borderRight: `${i == 2 ? '' : '1px solid #a7a7a7'}`,
            }}
          >
            { 
              <div className="d-flex justify-content-center align-items-center act pt-3">
                <h3 className="position-relative fs-1 pe-1 pe-sm-3 fw-bold">
                  {'0' + (i + 1)}
                  <span className="colorline"></span>
                </h3>
                <p className="text-uppercase text-start d-none d-lg-block fw-bolder fs-4 m-0">{featured[i].titleSlick}</p>
              </div>  
            }
          </div>
        )
      };
    return (
        <div className="section-home03">
            <div className="container px-4 pt-5 pb-3">
                <Hbar color={'#F45858'} title='CONTENIDO DESTACADO'/>
            </div>
            <Slider {...settings} className='featurecontent'>
              <SliderFullWidth
                minHeightSlider = '800px'
                imgBackground= 'url(../../assets/destacados-001.png)'
                title = {<Fragment>Chiribiquete: <br />cuento interactivo</Fragment>}
                description={'Chiribiquete no debe ser visitado. Es un tesoro para el mundo cuyo brillo depende en buena medida de que no se toque, de que no se mire demasiado cerca.'}
                textButton = 'Ir al cuento'
                linkButton = ''
              />
              <SliderFullWidth
                minHeightSlider = '800px'
                imgBackground= 'url(../../assets/slider-earth.png)'
                title = {<Fragment>Viaje al Centro <br />De la Tierra</Fragment>}
                description={'Durante mucho tiempo imaginamos que el mundo subterráneo era hueco.'}
                textButton = 'Conoce esta historia'
                linkButton = ''
              />
              <SliderFullWidth
                minHeightSlider = '800px'
                imgBackground= 'url(../../assets/slider-ciencia.png)'
                title = {<Fragment>Ciencia <br />en Bicicleta</Fragment>}
                description={'La república plural de las preguntas.'}
                textButton = 'Quiero saber más'
                linkButton = ''
              />                
            </Slider>
        </div>
    )
}
