import Image from "next/image";

export default function MainBelow({
  moreStyle
}) {
  return (
    <div className={`main-bottom d-none d-lg-flex justify-content-between ${moreStyle}`}>
      <div className="d-flex pb-4 pt-5 px-5 border-end border-light">
        <div className="pt-1 pe-3">
          <Image
            src={"/assets/icon-folder.png"}
            alt="..."
            width="43"
            height="21"
          />
        </div>
        <div className="text-white">
          <h6 className="fw-bold text-uppercase ">Parque Explora</h6>
          <p className="fw-light">Quiénes somos</p>
        </div>
      </div>
      <div className="d-flex pb-4 pt-5 px-5 border-end border-light">
        <div className="pt-1 pe-3">
          <Image
            src={"/assets/icon-time.png"}
            alt="..."
            width="24"
            height="24"
          />
        </div>
        <div className="text-white">
          <h6 className="fw-bold text-uppercase">Horarios y tarifas</h6>
          <p className="fw-light">Hoy 8:00 a.m - 6:00 p.m</p>
        </div>
      </div>
      <div className="d-flex pb-4 pt-5 px-5">
        <div className="pt-1 pe-3">
          <Image
            src={"/assets/icon-location.png"}
            alt="..."
            width="20"
            height="24"
          />
        </div>
        <div className="text-white pe-xl-5 me-xl-5">
          <h6 className="fw-bold text-uppercase">Cómo llegar</h6>
          <p className="fw-light">Ubicación</p>
        </div>
      </div>
    </div>
  );
}
