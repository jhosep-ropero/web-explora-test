import Link from "next/link";
import { useSelector } from "react-redux";
import Hbar from "../../ui/Hbar/Hbar";
import Separator from "../../ui/Separator/Separator";
import { contractUs } from '../../../data/contractUs';

export default function ContractUs() {

    //usar reducer de accesibilidad
    const { hideImages } = useSelector((state) => state.accesibility)

    return (
        <div className="mt-5">
            <Separator color='#6FBE75'/>
            <div className="container px-4 py-5">
                <Hbar color={'#6FBE75'} title='Contrátanos'/>
            </div>
            <div id="Contratanos" className="container">
                <div className="row">
                    {contractUs.map((item,index) =>(
                    <div key={index} className="col-lg-4 mx-auto px-5 py-4 px-0 position-relative d-grid align-items-end text-white"style={{
                        background: !hideImages && `${item.imgBackground}`
                      }}>
                        <div className="overlay position-absolute w-100 h-100"></div>
                        <div style={{ zIndex: "1" }}>
                            <h2 className="fw-bold">{item.title}</h2>
                            <p className="description fs-5">{item.description}</p>
                        </div>
                        <div style={{ zIndex: "1" }}>
                            <Link href={`${item.url}`} passHref>
                                <a href="" className="btn btn-primary text-white text-uppercase fs-5 p-3 w-100">
                                    Contratar
                                    <i className="icon-explora icon-arrow-right fs-6 ms-2"/>    
                                </a>
                            </Link>
                            <hr className="m-0 col-3"/>
                        </div>
                    </div>
                    ))
                    }
                </div>
            </div>
        </div>
    )
}
