import SliderFullWidth from "../../modules/SliderFullWidth";
import Slider from "react-slick";
import InformationBar from "../../modules/InformationBar";
import { Fragment } from "react";

export default function MainSliderExplora() {
  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
  };
  return (
    <div>
      <Slider {...settings} className="sliderhomexplore">
        <SliderFullWidth
          minHeightSlider = '100vh'
          videoBackground = "../../assets/videos/sala-acuario.mp4"
          title = {<Fragment>Un acuario <br />con más de 158 <br/>especies</Fragment>}
          textButton = 'Adquiere tu boleta aquí'
          linkButton = '/boleteria'
        />
        <SliderFullWidth
          minHeightSlider = '100vh'
          imgBackground={'url(/assets/slider-Bird.png)'}
          title={<Fragment>El vuelo de Las Aves: <br />Una instalación <br/> inmersiva</Fragment>}
          textButton = 'Conoce esta exposición'
          linkButton = ''
        />
        <SliderFullWidth
          minHeightSlider = '100vh'
          imgBackground= "url(../../assets/slider-space2.png)"
          title={<Fragment>Diciembre, el mes <br />de los fenómenos <br/> Astronómicos</Fragment>}
          textButton = 'Conoce nuestro planetario'
          linkButton = '/planetario'
        />
      </Slider>
      <InformationBar
        moreStyle = 'text-white position-absolute bottom-0 end-0'
        iconCol1 = 'icon-about-b'
        titleCol1 = 'PARQUE EXPLORA'
        texxtCol1 = 'Quiénes Somos'
        iconCol2 = 'icon-clock-b fs-4'
        titleCol2 = 'HORARIOS Y TARIFAS'
        texxtCol2 = 'Hoy 8:00 a.m - 6:00 p.m'
        iconCol3 = 'icon-location-b fs-4'
        titleCol3 = 'CÓMO LLEGAR'
        texxtCol3 = 'Ubicación'
        linkAbout = ''
        linkSchedule = ''
        linkLocation = ''
      />
    </div>
  );
}
