import Hbar from "../../ui/Hbar/Hbar";
import Separator from "../../ui/Separator/Separator";
import Slider from "react-slick";
import { programate } from "../../../data/programate";
import CardEvents from "../../modules/CardEvents";
import '../../../styles/modules/Home.module.scss'
import ButtonBar from "../../ui/ButtonBar/ButtonBar";



export default function Programate() {
  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 4,
    initialSlide: 0,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          dots: true
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2
        }
      },
      {
        breakpoint: 521,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };
    return (
        <div className="section-home02 section-carousel">
            <Separator color='#F45858'/>
            <div className="container px-4 py-5">
                <Hbar color={'#F9D51D'} title='PROGRÁMATE'/>
            </div>
            <Slider {...settings} className="container">
              {programate.map(ev =>(
                <CardEvents 
                  key={ev.id} 
                  img={ev.img} 
                  date={ev.date} 
                  category={ev.category} 
                  title={ev.title} 
                  dateExtend={ev.dateExtend}
                />
              ))}
            </Slider>
            <div className="col-12 col-md-6 m-auto text-center">
              <ButtonBar text='Mira la programación' center={true} colorBtn='#F45858' colorTxt='#fff' colorHr='#F9D51D'/>
            </div>
        </div>
    )
}
