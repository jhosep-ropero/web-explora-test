
import ButtonBar from "../../../ui/ButtonBar/ButtonBar";

// Styles
import styles from "./BannerCourses.module.scss";
import PageBarAlt from "../../../ui/PageBar/PageBarAlt";

const BannerCourses = ({title}) => {
    return (        
        <div className={`${styles.bannerProgramming} d-flex flex-column`}>
            <PageBarAlt title={title} color={'#FFCB05'} transparent={true}/>
            <div className={`container ${styles.infoBannerProgramming} text-white ps-md-auto`}>
                <h3 className="is-size-2 is-bold">Musical experimental <br /> para niños</h3>
                <p className="is-color-white is-size-7 text-uppercase is-bold">Cursos destacado</p>
                <ButtonBar
                    text={'Ingresar al curso'}                    
                    colorBtn={'#F45858'}
                    colorHr={'#F9D41E'}
                />
            </div>
        </div>       
    );
}
 
export default BannerCourses;