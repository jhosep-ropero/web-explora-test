import { useEffect, useState } from 'react'
import { handleChangeFilter } from '../../../../helpers/handleChangeFilter'
import styles from './Filter.module.scss'

const checkedInit = {
    todos: true,
    educacion: false,
    familiar: false,
    presencial:false,
    exploratorio:false,
    adultos:false,
    online:false,
    explora:false,
    niños: false,
    planetario: false,
    maestros: false
}

export default function Filter({courses,setCoursesFilter}) {
    
    const [categories, setCategories] = useState([])
    //estado para manejar las selecciones
    const [checked, setChecked] = useState(checkedInit)
    //state para abrir filtro de cursos
    const [openFilter, setOpenFilter] = useState(false)
    //state para mostrar filtros
    const [viewFilters, setViewFilters] = useState(false)

    //cada vez que cambia el state categories se ejecuta
    useEffect(() => {
        
    }, [categories])

    //funcion eliminar categorias del estado
    const deleteCategory = (category)=>{            
        const newCategories = categories.filter(cat => cat !== category)
        setCategories(newCategories)
    }

    //funcion de manejador de cambios de los checkbox
    const handleChange = (e)=>{        
        handleChangeFilter(e, categories, setCategories, checked, setChecked,null,null,deleteCategory, checkedInit)
    }    

    const deleteAllFilters = () =>{
        setCategories([])
        setViewFilters(false)
        setCoursesFilter(courses)
    }
    
    const deleteFilter = (category)=>{
        const newCategories = categories.filter(cat => cat !== category)
        if(newCategories.length == 0){
            setViewFilters(false)
            setCategories([])
            setCoursesFilter(courses)
        }else{
            setCategories(newCategories)
            filterCourses(newCategories)
        }        
    }

    //filtrar cursos por categorias, se recorren ambos arroglos para buscar coincidencias
    const filterCourses = (newCategories=categories)=>{
        const coursesResult = [];
        
        if(newCategories.length === 0){
            return setCoursesFilter(courses)
        }            

        courses.map((course) => newCategories.map(categ => {            
            if(categ==='todos'){
                coursesResult.push(course)
                setCategories([])
                setViewFilters(false)
            }else if(categ === course.audience ){
                coursesResult.push(course)
            }else if(categ === course.locationCourse){
                coursesResult.push(course)                    
            }else if(categ === course.modality){
                coursesResult.push(course)
            }
        })
    );


        //eliminar duplicados
        const dataArr = new Set(coursesResult);
        let result = [...dataArr];
        
        if(result.length === 0){
            setCoursesFilter([])
        }else{
            setCoursesFilter(result);
        }
    }

    const handleSubmit = (e)=>{
        e.preventDefault()
        if(categories.length > 0){
            setViewFilters(true)
        }
        setChecked(checkedInit)
        setOpenFilter(false)
        filterCourses()
    }

    return (
        <>
            {
                !viewFilters ?
                    <div className={`${styles.selectFilter} "fw-light is-size-9 ps-1 pb-4 pb-md-0" `}>
                        Todos los cursos 
                        <span className={`${styles.pointer} " ps-3 text-black-50 "`} onClick={()=>setOpenFilter(!openFilter)}>▼</span> 
                    </div>
                :   <div className={`${styles.selectFilter2} "fw-light ps-1 pb-4 pb-md-0 h4" `}>
                        <span className={`${styles.deleteFilter} text-decoration-underline pe-5`}>
                            <span className={"pe-2 text-black-50"} onClick={deleteAllFilters}>&times;</span> 
                            Limpiar filtros
                        </span>
                        <div className='d-flex flex-wrap'>
                            {
                                categories.map((cat,i)=>
                                    <span key={i} className='text-primary px-2 is-size-9'> 
                                        <span className={`${styles.pointer} ' text-black-50 pe-1 '`} onClick={()=>deleteFilter(cat)}>&times;</span>
                                        {cat}
                                    </span>
                                )
                            }
                        </div>
                    </div>
            }

            {
                openFilter &&
                <div className={`${styles.filter} fw-light is-size-9`}>
                    <div className='ps-1 pb-4 pb-md-0'>
                        Todos los cursos 
                        <span className={`${styles.pointer} " ps-3 text-black-50 "`} onClick={()=>setOpenFilter(!openFilter)}>▼</span> 
                    </div>
                    <form onSubmit={handleSubmit}>
                        <ul className="d-flex h-100">                            
                            <li>
                                <input type="checkbox" id="todos" value="todos" onChange={handleChange} checked={checked.todos}/>
                                <label htmlFor="todos">Todos</label>
                                
                                <div className={styles.check}></div>
                            </li>
                        </ul>
                        <ul className="d-flex justify-content-start h-100">
                            <li className='me-1'>
                                <input type="checkbox" id="educacion" value="Educación" onChange={handleChange} checked={checked.educacion}/>
                                <label htmlFor="educacion">Educación </label>
                                
                                <div className={styles.check}></div>
                            </li>
                            <li className='ms-2 ms-md-4 me-md-5'>
                                <input type="checkbox" id="familiar" value="Familiar" onChange={handleChange} checked={checked.familiar}/>
                                <label htmlFor="familiar">Familia </label>
                                
                                <div className={styles.check}></div>
                            </li>
                            <li className='ms-2 ms-md-4'>
                                <input type="checkbox" id="prensencial"  value="Presencial" onChange={handleChange} checked={checked.presencial}/>
                                <label htmlFor="prensencial">Prensencial</label>
                                
                                <div className={styles.check}></div>
                            </li>
                        </ul>

                        <ul className="d-flex justify-content-start h-100">
                            <li className='me-1'>
                                <input type="checkbox" id="exploratorio" value="Exploratorio" onChange={handleChange} checked={checked.exploratorio}/>
                                <label htmlFor="exploratorio">Exploratorio </label>
                                
                                <div className={styles.check}></div>
                            </li>
                            <li className='ms-md-2'>
                                <input type="checkbox" id="adultos" value="Jóvenes y adultos" onChange={handleChange} checked={checked.adultos}/>
                                <label htmlFor="adultos">Jóvenes y adultos</label>
                                
                                <div className={styles.check}></div>
                            </li>
                            <li>
                                <input type="checkbox" id="online"  value="Online" onChange={handleChange} checked={checked.online}/>
                                <label htmlFor="online">Online </label>
                                
                                <div className={styles.check}></div>
                            </li>
                        </ul>

                        <ul className="d-flex justify-content-start h-100">
                            <li className='me-4'>
                                <input type="checkbox" id="explora" value="Parque Explora" onChange={handleChange} checked={checked.explora}/>
                                <label htmlFor="explora">Explora</label>
                                
                                <div className={styles.check}></div>
                            </li>
                            <li className='ms-1 ms-md-4'>
                                <input type="checkbox" id="niños" value="Niños" onChange={handleChange} checked={checked.niños}/>
                                <label htmlFor="niños">Niños </label>
                                
                                <div className={styles.check}></div>
                            </li>
                            
                        </ul>
                        <ul className="d-flex justify-content-start h-100">
                            <li className='me-2'> 
                                <input type="checkbox" id="planetario" value="Planetario" onChange={handleChange} checked={checked.planetario}/>
                                <label htmlFor="planetario">Planetario </label>
                                
                                <div className={styles.check}></div>
                            </li>
                            <li className='ms-1 ms-md-4'>
                                <input type="checkbox" id="maestros" value="Maestros" onChange={handleChange} checked={checked.maestros}/>
                                <label htmlFor="maestros">Maestros </label>
                                
                                <div className={`${styles.check}`}></div>
                            </li>
                            
                        </ul>

                        <div className='text-center'>
                            <input type="submit" value="ELEGIR" className={styles.btnSubmit} />

                        </div>

                    </form>
                </div>
            }
        
            
        </>
    )
}
