import { useState, useEffect } from "react";

// Components
import CardProgramate from "../../programate/CardProgramate/CardProgramate";
import Search from "../../programate/Search/Search";

// Data
import { allCourses } from "../../../../data/courses";

// Styles
import styles from "./OurCourses.module.scss";
import Filter from "../Filter/Filter";

export default function OurCourses() {
  const courses = allCourses;

  // State para manejar el filtro de eventos
  const [coursesFilter, setCoursesFilter] = useState(allCourses);
  // State para manejar la busqueda
  const [busqueda, setBusqueda] = useState("");

  const handleChange = (e) => {
    setBusqueda(e.target.value);
    searchCourses(e.target.value);
  };

  const searchCourses = (valor) => {
    const results = courses.filter((course) => {
      if (
        course.title.toLowerCase().includes(valor.toLowerCase()) ||
        course.locationCourse.toLowerCase().includes(valor.toLowerCase())
      ) {
        return course;
      }
    });
    setCoursesFilter(results);
  };

  return (
    <div>
      <div className="container py-5 d-sm-flex align-items-center justify-content-end position-relative">
        <Filter courses={courses} setCoursesFilter={setCoursesFilter} />
        <Search handleChange={handleChange} busqueda={busqueda} />
      </div>
      <div className="container d-flex justify-content-center">
        <div className="row justify-content-center justify-content-md-start w-100">
          {coursesFilter
            .sort((a, b) => {
              if (a.dateCalendar.month > b.dateCalendar.month) {
                return 1;
              }
              if (a.dateCalendar.month <= b.dateCalendar.month) {
                return -1;
              }
              return 0;
            })
            .map((ev) => (
              <div
                className={`col-12 col-md-6 col-lg-3 ${styles.listCardProgramate}`}
                key={ev.id}
              >
                <CardProgramate
                  locationCourse={ev.locationCourse}
                  img={ev.img}
                  audience={ev.audience}
                  modality={ev.modality}
                  price={ev.price}
                  dateCourse={ev.dateCourse}
                  title={ev.title}
                  time={ev.time}
                  dateExtend={ev.dateExtend}
                />
              </div>
            ))}
        </div>
      </div>
    </div>
  );
}
