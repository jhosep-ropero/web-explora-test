import Image from "next/image";
import Link from "next/link";
import { Fragment } from "react";
import Hbar from "../../../ui/Hbar/Hbar";
import Separator from "../../../ui/Separator/Separator";
import styles from "./Explora.module.scss";

export default function Explora() {
    return (
        <>
            <div className={styles.wrapperExplora} >
                <Separator color={'#F9D51D'}/>
                <div className="col-12 col-md-8 m-auto text-center mt-5 pt-5">

                    <Hbar color={'#fff'} title={<Fragment><span className="text-light fw-light">¿Qué es <strong className="fw-bold">Parque Explora?</strong> </span></Fragment>}/>
                </div>
            </div>
            <div className="container">
                <div className="row mt-5 pt-3">
                    <div className="col-12 col-md-6 m-auto m-lg-0">
                        <p>El Parque Explora, es un museo interactivo de ciencias en la ciudad de Medellín, Colombia, formado por un acuario con énfasis en la Amazonia, un planetario, un taller público de experimentación -Exploratorio- y un parque con más de 300 experiencias orientadas a la apropiación social del conocimiento. Tiene 22 mil metros cuadrados de área interna y 15 mil de plazas públicas, con jardines nativos que lo convierten en un atractivo parque.</p>
                    </div>

                </div>
                <div className="row mt-4 mb-5">
                    <div className={`${styles.buttonbar}`}>
                        <Link href="#" passHref>
                            <a href="">
                            <span className="pe-3 pt-1 text-uppercase">ver más</span>
                            <Image
                                src='/assets/icon-play.png'
                                alt="play"
                                width={"16"}
                                height={"16"}
                            />
                            </a>
                        </Link>
                        <hr />
                    </div>
                </div>
            </div>
        </>
    )
}
