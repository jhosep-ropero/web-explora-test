import Hbar from "../../../../ui/Hbar/Hbar";
import ButtonBar from "../../../../ui/ButtonBar/ButtonBar";
import Card from "./Card";

//styles
import styles from "./Styles.module.scss";

export default function PlayList({ playList }) {
  return (
    <div id="videos">
      <div className="container px-4 py-5">
        <Hbar color={"#F6571E"} title="LISTA DE REPRODUCCIÓN" />
        <div className="row justify-content-center justify-content-md-start">
          {playList.map((video, index) => (
            <div
              className={`col-12 col-md-6 col-lg-3 ${styles.listCardPlay}`}
              key={index}
            >
              <Card img={video.img.url} title={video.titulo} url={video.url} />
            </div>
          ))}
        </div>
        <div className="text-center mt-4">
          <ButtonBar
            text="Ver más videos"
            center={true}
            colorBtn="#F45858"
            colorTxt="#fff"
            colorHr="#F9D51D"
          />
        </div>
      </div>
    </div>
  );
}
