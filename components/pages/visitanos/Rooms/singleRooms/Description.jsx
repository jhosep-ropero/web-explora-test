import Image from "next/image";
import Link from "next/link";
import { useState } from "react";
import Hbar from "../../../../ui/Hbar/Hbar";
import styles from "./Styles.module.scss";

export default function Description({
  background,
  title,
  description,
  awards,
}) {
  const [openText, setOpenText] = useState(false);
  return (
    <div id="descripcion" className={styles.bgDescripcion}>
      <div className="container py-5">
        <Hbar title={"Descripción"} color={"#F6571E"} />

        <div className="row position-relative">
          <div className={styles.fondoDescripcionAcuario}>
            <Image
              src={background}
              width={"800px"}
              alt="acuario"
              height={"530px"}
            />
          </div>
          <div className="col-12 offset-lg-6 col-lg-6 text-dark is-size-9 px-md-4">
            <h3 className="is-size-4 fw-bold pb-2">{title}</h3>
            <p className="fw-light">{description}</p>
            {awards.length !== 0 && (
              <div>
                <h5 className="fw-bold mt-5 pt-4" id="openText">
                  Reconocimientos
                </h5>
                {awards.map((item, i) => (
                  <div className="d-flex align-items-center my-3" key={i}>
                    <span className="icon-explora icon-Traveller-choise is-size-5"></span>
                    <span className="ms-3">{item.title}</span>
                  </div>
                ))}
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
