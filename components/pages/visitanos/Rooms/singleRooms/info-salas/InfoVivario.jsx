import Image from "next/image";
import Link from "next/link";
import { useState } from "react";
import Hbar from "../../../../../ui/Hbar/Hbar";
import styles from '../Styles.module.scss'

export default function InfoVivario() {
    const [openText, setOpenText] = useState(false)
    return (
        <div className="bg-secondary">
            <div className="container py-5">
                <Hbar title={'Descripción'} color={'#F6571E'}/>

                <div className="row position-relative">
                    <div className={styles.fondoDescripcion}>
                        <Image
                            src={'/assets/rooms/vivario/fondo-descripcion.png'}
                            width={'800px'}
                            alt="acuario"
                            height={'530px'}
                        />
                    </div>
                    <div className="col-12 offset-lg-6 col-lg-6 text-dark is-size-9 px-md-4">
                        <h3 className="is-size-4 fw-bold pb-2">Nuestra historia</h3>
                        <p className="fw-light">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam laoreet egestas placerat. Aliquam in sapien eget augue molestie dignissim eu ut purus. Aliquam quis quam ante. Nulla sit amet auctor neque. Aliquam rutrum, arcu sit amet tincidunt vestibulum, eros nulla tempor urna, at cursus tortor ipsum vitae elit. Nullam tincidunt ac Leo tempor ornare. Etiam at rhoncus purus. Phasellus arcu sapien, vehicula mattis urna id, ultrices euismod risus. Praesent iaculis rhoncus arcu et condimentum.
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam laoreet egestas placerat. Aliquam in sapien eget augue molestie dignissim eu ut purus. Aliquam quis quam ante. Nulla sit amet auctor neque. Aliquam rutrum, arcu sit amet tincidunt vestibulum   
                        </p>

                        <h5 className="fw-bold mt-5 pt-4" id="openText">Reconocimientos</h5>
                        <div className="d-flex align-items-center my-3">
                            <span className="icon-explora icon-planet-store is-size-5"></span>
                            <span className="ms-3">Premio Travellers’ Choice, 2013.</span>
                        </div>
                        <div className="d-flex align-items-center my-3">
                            <span className="icon-explora icon-planet-store is-size-5"></span>
                            <span className="ms-3">Certificado de Excelencia de TripAdvisor®, 2016.</span>
                        </div>
                        <div className="d-flex align-items-center my-3">
                            <span className="icon-explora icon-planet-store is-size-5"></span>
                            <span className="ms-3">Puesto número 16 en el ranquin TripAdvisor® de los 25 <br/> acuarios más recomendados del mundo, 2014.</span>
                        </div>
                        <div className="d-flex align-items-center my-3">
                            <span className="icon-explora icon-planet-store is-size-5"></span>
                            <span className="ms-3">Moción de reconocimiento como Amigo de la Escuela de <br/> Microbiología de la Universidad de Antioquia, 2015.</span>
                        </div>
                    </div>
                    
                </div>

                {/* <div className="row mt-5 pt-5" >
                    <div className="col-12 col-lg-3 m-auto text-center">
                        <span 
                            className={`${styles.btnToogle} " border-0 border-bottom p-2 "`} 
                            onClick={()=>setOpenText(true)}>
                            Conoce nuestra historia ▼
                        </span>
                    </div>
                </div>
                {
                    openText &&
                    <div className="row mt-5 pt-3">
                        <div className="col-12 col-lg-4 fw-light px-md-4">
                            <p>
                                En acuario y vivario 2583 individuos y 252 especies -muchos rescatados de tenencias irregulares y ahora protegidos por nosotros- forman parte de los programas educativos y de conservación, especies de agua dulce y salada, nos ayudarán a descubrir a Colombia, país de agua. Una selva amazónica inundada recibe al visitante y le revela habitantes ocultos del agua como el pirarucús, el pez más grande de agua dulce de Suramérica… Es un espacio de naturaleza educativa que nos ayuda a entender la vida en su signo más rotundo: la diversidad.<br/>
                                Aliados: Acopazoa (Membresía). <br /> <br />
                                El Acuario de Parque Explora tiene un área construida de aproximadamente 2.200 m2 distribuidos en tres niveles. En el primer nivel está la pecera central, representación en gran formato de un bosque amazónico inundado, uno de los ecosistemas más diversos de la Tierra. Este tipo de bosque se caracteriza por tener luz solar todo el año y por ser uno de los lugares donde más llueve. Allí crecen árboles gigantescos con numerosas lianas, enredaderas y otras plantas trepadoras. En la pecera, más de 320.000 litros de agua rodean al gran pilón del centro, un árbol valorado por su madera y por las sustancias que produce, muy utilizadas para curtir cueros. En esas aguas nadan los peces más grandes del acuario: pirarucús, arawanas, cachamas y bagres.
                                </p>
                        </div>
                        <div className="col-12 col-lg-4 fw-light px-md-4">
                            <p>
                                Alrededor de la pecera principal hay otros escenarios de agua dulce, habitados por organismos representativos de los principales ríos de Colombia, como los bocachicos, originarios del río Magdalena, y los tucunarés, agresivos habitantes del río Orinoco. <br /> <br />
                                Los ecosistemas de agua dulce como los ríos, lagos y humedales ocupan menos del 2% de la superficie terrestre, y en ellos habitan plantas, invertebrados acuáticos (como larvas de muchos animales, escarabajos y libélulas) y vertebrados como peces y anfibios. Sin embargo, se estima que en la actualidad el 20% de los peces de agua dulce del mundo son vulnerables, están amenazados o en peligro de extinción, por factores como la sobrepesca, la minería, la destrucción de hábitats, la alteración física de los cuerpos de agua, el vertimiento de aguas residuales y la contaminación del aire y del agua.<br/> <br />
                                En la zona de agua dulce del Acuario hay 158 especies originarias del país, 41 especies endémicas (que sólo existen en Colombia), peces emblemáticos como el bocachico, el pez nacional, y varias especies de cuchas, coridoras, sardinas, cíclidos y mojarras. También hay pirarucús y peces pulmonados, más de 70 especies de carácidos, varias especies de bagres, más de 15 especies de loricáridos, 45 especies de cíclidos, 
                                </p>
                        </div>
                        <div className="col-12 col-lg-4 fw-light px-md-4">
                            <p>
                                2 especies de rayas de agua dulce, y algunos cuchillos y temblones que generan electricidad con su cuerpo para orientarse, delimitar su territorio y, en algunos casos, cazar y defenderse. <br /> <br />
                                Por su privilegiada posición en el trópico, Colombia posee una gran diversidad en ambientes marinos y costeros cuya extensión casi equivale a la de las tierras emergidas del país. Allí hay gran cantidad de recursos y asociaciones que constituyen una importante riqueza natural. En el segundo piso del Acuario hay una muestra de uno de los ecosistemas marinos más productivos del planeta, los arrecifes coralinos, conformados por colonias de pequeños organismos que forman una estructura rocosa en la que conviven especies de casi todos los grupos animales. Peces payaso, morenas, anémonas, medusas, estrellas de mar, erizos, entre otros, son los moradores marinos del Acuario Explora. <br /> <br />
                                El Acuario hace parte de la Asociación Colombiana de Parques Zoológicos y Acuarios de Colombia, Acopazoa, y es un laboratorio vivo para la investigación y la formación académica. Trabajamos en asocio con universidades y centros especializados, y promovemos la educación ambiental, la conservación y la protección de la biodiversidad.
                                </p>
                        </div>

                        <div className="col-10 col-md-3 m-auto text-center my-4">
                            <Link href={'#openText'} passHref>
                                <a 
                                    href=""
                                    className={`${styles.btnToogle} " py-2 px-4 "`} 
                                    onClick={()=>setOpenText(false)}>
                                    CERRAR
                                </a>
                            </Link>
                        </div>
                    </div>
                } */}
            </div>
        </div>
    )
}
