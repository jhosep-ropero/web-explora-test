import Link from "next/link";
import ReactPlayer from "react-player";
import { useSelector } from "react-redux";
import LinkDiv from "./LinkDiv";
import styles from "./Styles.module.scss";

export default function SliderOne({ background, title, room }) {
  //usar reducer de accesibilidad
  const { hideImages, stopAnimations } = useSelector(
    (state) => state.accesibility
  );
  return (
    <div className={styles.slider}>
      <div
        className={` position-absolute top-0 start-0 w-100 p-5`}
        style={{ minHeight: `100vh`, zIndex: "-1" }}
      >
        {!hideImages && (
          <ReactPlayer
            className={`bg-video`}
            url={background}
            playing={!stopAnimations}
            width={"auto"}
            height={"auto"}
            loop={true}
          />
        )}
      </div>
      <LinkDiv room={room} />
      <div className="container h-75 p-4 ">
        <div className="row align-content-center h-100">
          <div className={`${styles.info}  col-12 col-md-7 p-0 `}>
            <h2 className="is-size-3">{title}</h2>
            <div className="col-12 col-md-9 d-flex justify-content-between is-size-9">
              <Link href={"#galeria"} passHref>
                <a href="">Galería</a>
              </Link>
              <Link href={"#videos"} passHref>
                <a href="">Videos</a>
              </Link>
              <Link href={"#descripcion"} passHref>
                <a href="">Descripción</a>
              </Link>
            </div>
          </div>
          <Link href={"#intro"} passHref>
            <a href="" className={styles.LinkBottom}>
              <span className="icon-explora icon-arrow-down-cirlcle is-size-4 text-white"></span>
            </a>
          </Link>
        </div>
      </div>
    </div>
  );
}
