import Link from "next/link";
import styles from "./Styles.module.scss";

export default function SliderTwo({ background, title }) {
  return (
    <div
      id="intro"
      className={styles.slider}
      style={{
        backgroundImage: `url(${background})`,
        backgroundColor: `rgba(0,0,0,0)`,
      }}
    >
      <div className="container h-100 p-4">
        <div className="row align-content-center h-100">
          <div className={`${styles.info} col-12 mt-5 mx-auto pt-5 px-0`}>
            <h2 className="is-size-3 text-center pt-5 mt-5">{title}</h2>
            <div className="col-12 col-md-10 col-xl-6 m-auto d-flex justify-content-between is-size-9">
              <Link href={"#galeria"} passHref>
                <a href="">Galería</a>
              </Link>
              <Link href={"#videos"} passHref>
                <a href="">Videos</a>
              </Link>
              <Link href={"#descripcion"} passHref>
                <a href="">Descripción</a>
              </Link>
            </div>
          </div>
        </div>
        <Link href={"#galeria"} passHref>
          <a href="" className={styles.LinkBottom}>
            <span className="icon-explora icon-arrow-down-cirlcle is-size-4 text-white"></span>
          </a>
        </Link>
      </div>
    </div>
  );
}
