import Image from "next/image";
import Link from "next/link";
//styles
import styles from "./Styles.module.scss";

export default function Card({ title, img, url }) {
  return (
    <div className="mb-4">
      <div className="position-relative d-flex justify-content-center">
        <Image
          src={img}
          className="card-img-top img-fluid"
          alt="..."
          width="330px"
          height="387px"
        />

        <div className={`${styles.textCard}`}>
          <h5 className={`py-4 px-3 text-secondary is-size-8`}>{title}</h5>
          <div className="my-4">
            <a
              href={url}
              target={"_blank"}
              className="bg-primary ms-3 px-4 py-2"
              rel="noreferrer"
            >
              <span className="text-uppercase text-secondary me-2">
                Ver video
              </span>
              <Image
                src="/assets/arrow-right4079.png"
                alt="..."
                width={"18"}
                height={"13"}
              />
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}
