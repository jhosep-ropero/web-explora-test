export default function LeftNav({disabled,onClick}) {
    return (
        <button
            type="button"
            className="image-gallery-icon image-gallery-left-nav"
            disabled={disabled}
            onClick={onClick}
            aria-label="Previous Slide"
        >
            <span className="icon-explora icon-arrow-left-cirlcle text-dark is-size-4"></span>
        </button>
    )
}
