import React from "react";
import ImageGallery from "react-image-gallery";

import Hbar from "../../../../../ui/Hbar/Hbar";
import LeftNav from "./LeftNav";
import RightNav from "./RightNav";

export default function PhotoGallery({ images }) {
  const imgs = images.map((img) => ({
    original: img.img.url,
    thumbnail: img.img.imgix_url + "?fit=crop&w=200&h=200",
    description: img.description,
  }));

  return (
    <div id="galeria">
      <div className="container">
        <Hbar title={"Galería"} color={"#F45858"} />
        <div className="py-4">
          <ImageGallery
            items={imgs}
            showPlayButton={false}
            showIndex={true}
            renderLeftNav={(onClick, disabled) => (
              <LeftNav onClick={onClick} disabled={disabled} />
            )}
            renderRightNav={(onClick, disabled) => (
              <RightNav onClick={onClick} disabled={disabled} />
            )}
          />
          <div>{images.description}</div>
        </div>
      </div>
    </div>
  );
}
