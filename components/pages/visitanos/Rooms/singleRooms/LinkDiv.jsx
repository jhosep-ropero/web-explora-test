import Image from "next/image";
import Link from "next/link";
import arrow from '../../../../../public/assets/arrow-left-white.png';
import styles from './Styles.module.scss'

export default function LinkDiv({container, room, color}) {
    return (
        <div className={`${styles.link}  container d-flex align-items-center `}>
            <Link href={'/visitanos'} passHref>
                <a href="" className={styles.btnLink}>
                    <span className={`icon-explora icon-arrow-left-cirlcle is-size-4 ${color ? `text-${color}` : "text-white"}`}></span>
                </a>
            </Link>
            <div className='ps-4 ps-lg-1'>
                <h6 className={`${color ? `text-${color}` : "text-secondary"} fw-light is-size-9`}>{!container ? "Salas Explora" : container } &gt; {room}</h6>
            </div>
        </div>
    )
}
