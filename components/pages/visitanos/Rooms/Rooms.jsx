import Image from "next/image";
import Link from "next/link";
import { Fragment, useEffect, useState } from "react";
import ReactPlayer from "react-player";
import { useSelector } from "react-redux";
import Hbar from "../../../ui/Hbar/Hbar";
import Separator from "../../../ui/Separator/Separator";
import styles from "./Rooms.module.scss";

export default function Rooms({ rooms }) {
  const [state, setState] = useState("initial");
  const [background, setBackground] = useState("/assets/fondo-explora01.png");

  const initialBackground = () => {
    setState("initial");
    setBackground("/assets/fondo-explora01.png");
  };

  const changeBackground = (room, url) => {
    setState(room);
    setBackground(url);
  };
  const { hideImages, stopAnimations } = useSelector(
    (state) => state.accesibility
  );
  return (
    <div className={styles.wrapperRooms}>
      <div
        className={` position-absolute top-0 start-0 w-100 p-5`}
        style={{
          minHeight: `100vh`,
          zIndex: "-1",
          backgroundImage: `url(/assets/parqueexplora.jpg)`,
        }}
      >
        {!hideImages && (
          <ReactPlayer
            className={`bg-video`}
            url={background}
            playing={!stopAnimations}
            width={"auto"}
            height={"auto"}
            loop={true}
          />
        )}
      </div>
      <Separator color={"#F45858"} />

      <div className="container pt-4 pb-5">
        <Hbar title={"Salas explora"} color={"#F45858"} titleColor="#fff" />
        <div className={`${styles.roomsContainer} `}>
          {rooms.map((room, index) => (
            <Link
              key={index}
              href="/visitanos/[slug]"
              as={`/visitanos/${room.slug}`}
              passHref
            >
              <div
                key={index}
                className={`${styles.cardRooms} col-12 col-lg-4 d-flex`}
                onMouseOver={() =>
                  changeBackground(room.title, room.metadata.background.url)
                }
                onMouseLeave={initialBackground}
                onBlur={initialBackground}
                onFocus={() =>
                  changeBackground(room.title, room.metadata.background.url)
                }
              >
                {state === room.title ? (
                  <div className={styles.roomInfo}>
                    <div className="position-relative">
                      <span className="my-auto px-4 text-white text-uppercase">
                        {room.title}
                      </span>
                      <Image
                        src={"/assets/arrow-right4079.png"}
                        alt="..."
                        width={"23"}
                        height={"15"}
                      />
                      <a href={`/visitanos/${room.slug}`}>Ver más</a>
                    </div>
                  </div>
                ) : (
                  <div className="my-auto ps-4 text-white-50 text-uppercase">
                    {room.title}
                  </div>
                )}
              </div>
            </Link>
          ))}

          {rooms.length % 3 === 0 ? (
            <Fragment>
              <div
                className={`${styles.cardRooms} col-12 col-lg-4 d-flex`}
              ></div>
              <div
                className={`${styles.cardRooms} col-12 col-lg-4 d-flex`}
              ></div>
              <div
                className={`${styles.cardRooms} col-12 col-lg-4 d-flex`}
              ></div>
            </Fragment>
          ) : (
            new Array(6 - (rooms.length % 3))
              .fill(0)
              .map((_, index) => (
                <div
                  key={index}
                  className={`${styles.cardRooms} col-12 col-lg-4 d-flex`}
                ></div>
              ))
          )}
        </div>
      </div>
    </div>
  );
}
