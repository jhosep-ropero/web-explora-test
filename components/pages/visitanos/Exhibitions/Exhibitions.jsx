import Image from "next/image";
import Slider from "react-slick";
import Buttons from "../../../ui/Buttons/Buttons";
import Hbar from "../../../ui/Hbar/Hbar";
import Separator from "../../../ui/Separator/Separator";
import styles from "./Exhibitions.module.scss";

export default function Exhibitions() {
  const settings = {
    
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 2,
    slidesToScroll: 2,
    initialSlide: 0,
    responsive: [
      {
        breakpoint: 1920,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 1000,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  return (
    <div className="section-carousel">
      <Separator color="#6FBE75" />
      <div className="container px-4 py-5">
        <Hbar color={"#F45858"} title="Exposiciones" />
      </div>
      <Slider {...settings} className="container">
        <div className={styles.card}>
          <Image
            src={`/assets/exhibitions/img1.png`}
            className="card-img-top"
            alt="..."
            width="736px"
            height="654px"
          />
          <div className={`${styles.cardExhibition1MouseOver}`}>
            <h4>Viaje al centro de la tierra</h4>
            <p>
              Durante mucho tiempo imaginamos que el mundo subterráneo
              era hueco.
            </p>
            <Buttons text="ver más" colorBtn="#F45858" />
          </div>
          <div className={styles.cardExhibition1}>
            <h4>Viaje al centro de la tierra </h4>
          </div>
        </div>

        <div>
          <div className={styles.card}>
            <Image
              src={`/assets/exhibitions/img2.png`}
              className="card-img-top"
              alt="..."
              width="659px"
              height="289px"
            />
            <div className={`${styles.cardExhibition2MouseOver}`}>
              <h4>Chiribiquete: cuento interactivo</h4>
              <p>
                Chiribiquete no debe ser visitado. Es un tesoro 
                para el mundo cuyo brillo depende en buena 
                medida de que no se toque, de que no se mire demasiado
                cerca.
              </p>
              <Buttons text="ver más" colorBtn="#F9D51D" colorTxt="#44444A" />
            </div>
            <div className={styles.cardExhibition2}>
              <h4>Chiribiquete: cuento interactivo </h4>
            </div>
          </div>

          <div className={styles.card}>
            <Image
              src={`/assets/exhibitions/img3.png`}
              className="card-img-top"
              alt="..."
              width="659px"
              height="289px"
            />
            <div className={`${styles.cardExhibition3MouseOver}`}>
              <h4>Los más bellos insultos</h4>
              <p>
                La condición humana es altamente 
                compleja y existen múltiples factores que 
                afectan lo que consideramos sexo  biológico.
              </p>
              <Buttons text="ver más" colorBtn="#6FBE75" />
            </div>
            <div className={styles.cardExhibition3}>
              <h4>Los más bellos insultos </h4>
            </div>
          </div>
        </div>
        <div className={styles.card}>
          <Image
            src={`/assets/exhibitions/img1.png`}
            className="card-img-top"
            alt="..."
            width="736px"
            height="654px"
          />
          <div className={`${styles.cardExhibition1MouseOver}`}>
            <h4>Viaje al centro de la tierra</h4>
            <p>
              Durante mucho tiempo imaginamos que el <br /> mundo subterráneo
              era hueco.
            </p>
            <Buttons text="ver más" colorBtn="#F45858" />
          </div>
          <div className={styles.cardExhibition1}>
            <h4>Viaje al centro de la tierra </h4>
          </div>
        </div>
      </Slider>
    </div>
  );
}
