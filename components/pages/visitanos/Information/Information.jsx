import Image from "next/image";
import Link from "next/link";
import Hbar from "../../../ui/Hbar/Hbar";
import Separator from "../../../ui/Separator/Separator";
import styles from "./Information.module.scss";

export default function Information() {
  return (
    <div style={{ background: "#F2F2F7" }}>
      <Separator color={"#F9D51D"} />
      <div className="container py-4">
        <Hbar title={"información de interés"} color={"#ff6262"} />
        <div className="row my-5 pb-3">
          <div
            className={`col-12 col-md-6 col-lg-3 my-lg-5 pt-lg-5 pb-4 text-center`}
          >
            <div className={`${styles.cardInfo}`}>
              <Image
                src={"/assets/icon-time2.png"}
                alt="icon"
                width={"110px"}
                height={"110px"}
              />
              <h4>HORARIOS Y TARIFAS</h4>
              <Link href={"#"} passHref>
                <a href="">VER MÁS</a>
              </Link>
            </div>
          </div>
          <div
            className={`col-12 col-md-6 col-lg-3 my-lg-5 pt-lg-5 pb-4 text-center`}
          >
            <div className={`${styles.cardInfo}`}>
              <Image
                src={"/assets/icon-location2.png"}
                alt="icon"
                width={"86px"}
                height={"110px"}
              />
              <h4>VER MAPA</h4>
              <Link href={"#"} passHref>
                <a href="">VER MÁS</a>
              </Link>
            </div>
          </div>
          <div
            className={`col-12 col-md-6 col-lg-3 my-lg-5 pt-lg-5 pb-4 text-center`}
          >
            <div className={`${styles.cardInfo}`}>
              <Image
                src={"/assets/icon-people.png"}
                alt="icon"
                width={"178px"}
                height={"110px"}
              />
              <h4>PÚBLICO SUBSIDIADO</h4>
              <Link href={"#"} passHref>
                <a href="">VER MÁS</a>
              </Link>
            </div>
          </div>
          <div
            className={`col-12 col-md-6 col-lg-3 my-lg-5 pt-lg-5 pb-4 text-center`}
          >
            <div className={`${styles.cardInfo}`}>
              <Image
                src={"/assets/icon-percentage.png"}
                alt="icon"
                width={"110px"}
                height={"110px"}
              />
              <h4>INGRESO CON DESCUENTO</h4>
              <Link href={"#"} passHref>
                <a href="">VER MÁS</a>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
