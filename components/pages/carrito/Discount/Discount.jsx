import { Formik, Field, Form } from "formik";
import React from "react";
import styles from "./Discount.module.scss";
export const Discount = () => {
  return (
    <Formik
      initialValues={{
        discountCode: "",
      }}
    >
      {({ values }) => (
        <Form>
          <div className={`${styles.flexBox}`}>
            <div className="d-flex align-items-center">
              <i className="icon-explora icon-gift-ticket fs-2 text-primary mx-auto" />
              <p className="m-0 px-3 is-size-8">
                Si tienes un código de cupón, por favor aplícalo en la siguiente
                casilla
              </p>
            </div>
            <div className={styles.customPadding}>
              <Field
                id="discountCode"
                className="form-control rounded-0"
                type="text"
                name="discountCode"
                value=""
              />
              <button className={`btn btn-secondary is-size-8  ${styles.discountBtn}`}>
                Aplicar Cupón
                <i className={`icon-explora icon-arrow-right fs-5`} />
              </button>
            </div>
          </div>
        </Form>
      )}
    </Formik>
  );
};
