import React from "react";
import { NavbarMenu } from "../../../templates/navbar/NavbarMenu";
import { BilingForm } from "../BillingFrom/BilingForm";
import { Discount } from "../Discount/Discount";
import { OrderSummary } from "../OrderSummary/OrderSummary";
import styles from "./BillingPage.module.scss";
import Link from "next/link";

export const BillingPage = () => {
  return (
    <div className={styles.billingBackground}>
      <Link href={"/aprende"} passHref>
        <a href="">
          <i
            className={`icon-explora icon-arrow-left-cirlcle text-black ${styles.backButton}`}
          ></i>
        </a>
      </Link>
      <NavbarMenu />
      <main className={`container ${styles.billingGrid}`}>
        <div className={`px-4 py-5 ${styles.billingForm}`}>
          <p className="is-bold">DETALLES DE FACTURACIÓN</p>
          <BilingForm />
        </div>
        <div className={`px-4 py-4 ${styles.discount}`}>
          <Discount />
        </div>
        <div className={`px-4 py-5 ${styles.orderSummary}`}>
          <p className="is-bold">RESUMEN DE TU ORDEN</p>
          <OrderSummary />
        </div>
      </main>
    </div>
  );
};
