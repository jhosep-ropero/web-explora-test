import Image from "next/image";
import React from "react";
import thumpnail from "../../../../public/assets/courses/poster1.svg";
import styles from "./OrderSummary.module.scss";
import logoPlacetoPay from "../../../../public/assets/courses/PlacetoPay.png";
import { cartCourses } from "../../../../data/cartCourses";
import Link from "next/link";

export const OrderSummary = () => {
  const getsubTotal = () => {
    let subTotal = 0;
    cartCourses.forEach((item) => {
      subTotal = subTotal + item.price;
    });
    return subTotal;
  };
  return (
    <div>
      <div className={styles.summaryBox}>
        <div>
          <div className="d-flex justify-content-between">
            <p className="is-bold">PRODUCTO</p>
            <p className="is-bold">SUBTOTAL</p>
          </div>
          {cartCourses.map((item, index) => (
            <div
              key={index}
              className="d-flex justify-content-between align-items-center pb-3"
            >
              <div className="d-flex align-items-center">
                <Image
                  src={thumpnail}
                  width={69}
                  height={69}
                  alt="Imagen del curso"
                />
                <div className="px-3">
                  <p className="m-0 is-size-8">{item.title}</p>
                  <div className={styles.amountBtn}>
                    <i
                      className="icon-explora icon-less"
                      style={{ fontSize: "0.12rem" }}
                    />
                    {item.amount}
                    <i className="icon-explora icon-plus" />
                  </div>
                </div>
              </div>
              <div>
                <p className="m-0 is-bold is-size-8">
                  ${item.price} <i className="ps-3 icon-explora icon-close" />
                </p>
              </div>
            </div>
          ))}
        </div>
        <div>
          <div className={`${styles.flexBox}`}>
            <div className={styles.placeToPay}>
              <Image
                src={logoPlacetoPay}
                width={80}
                height={20}
                alt="Logo PlacetoPlay"
              />
              <p className="m-0 is-size-9">
                Paga seguro a través de PlacetoPay
              </p>
            </div>
            <p className={`is-size-8 ${styles.isLink}`}>Limpiar Carrito</p>
          </div>
          <div className="">
            <div className={styles.totalDisplay}>
              <p>TOTAL</p>
              <p>${getsubTotal()}</p>
            </div>
            <button className={`is-size-8 ${styles.shopBtn}`}>
              REALIZAR COMPRA
              <i className="icon-explora icon-arrow-right" />
            </button>
            <Link href={"/aprende"} passHref>
              <a href="">
                <button className={`is-size-8 ${styles.goToShopBtn}`}>SEGUIR COMPRANDO</button>
              </a>
            </Link>
          </div>
          <div className={`is-size-8 ${styles.isLinkGray}`}>
            Conozca nuestras condiciones legales
          </div>
        </div>
      </div>
      <div className={styles.summaryBoxMobile}>
        <div className="d-flex justify-content-between">
          <div>
            <p className="is-bold is-size-8">PRODUCTO</p>
          </div>
          <div>
            <p className="is-bold is-size-8">SUBTOTAL</p>
          </div>
        </div>
        <div>
          {cartCourses.map((item, index) => (
            <div key={index}>
              <div className="">
                {index > 0 && <hr className="my-4" />}
                <div className="row">
                  <div className="col-4">
                    <Image
                      src={thumpnail}
                      width={98}
                      height={98}
                      alt="Imagen del curso"
                    />
                  </div>
                  <div className="col">
                    <div className="px-3 d-flex justify-content-between">
                      <p className="m-0 is-size-8">{item.title}</p>
                      <i className="ps-3 icon-explora icon-close is-size-8" />
                    </div>
                  </div>
                </div>
                <div className="d-flex justify-content-between align-items-center">
                  <div className={styles.amountBtn}>
                    <i
                      className="icon-explora icon-less"
                      style={{ fontSize: "0.1rem" }}
                    />
                    {item.amount}
                    <i className="icon-explora icon-plus is-size-8" />
                  </div>
                  <div>
                    <p className="m-0 is-bold is-size-8">${item.price} </p>
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
        <div>
          <div className={`${styles.flexBox}`}>
            <div className={styles.placeToPay}>
              <Image
                src={logoPlacetoPay}
                width={80}
                height={20}
                alt="Logo PlacetoPlay"
              />
              <p className="m-0 is-size-9">
                Paga seguro a través de PlacetoPay
              </p>
            </div>
            <p className={`is-size-8 ${styles.isLink}`}>Limpiar Carrito</p>
          </div>
          <div className={styles.stickyBot}>
            <div>
              <div className={styles.totalDisplay}>
                <p>TOTAL</p>
                <p>${getsubTotal()}</p>
              </div>
              <button className={`is-size-8 ${styles.shopBtn}`}>
                REALIZAR COMPRA
                <i className="icon-explora icon-arrow-right" />
              </button>
              <Link href={"/aprende"} passHref>
                <a href="">
                  <button className={`is-size-8 ${styles.goToShopBtn}`}>
                    SEGUIR COMPRANDO
                  </button>
                </a>
              </Link>
            </div>
            <div className={`is-size-8 ${styles.isLinkGray}`}>
              Conozca nuestras condiciones legales
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
