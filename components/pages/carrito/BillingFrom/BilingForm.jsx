import React from "react";
import { Formik, Form, Field } from "formik";
// import { validationSchema } from "../../../../data/validationSchema";
import styles from "./BillingForm.module.scss";

export const BilingForm = () => {
  const isString = (value) => {
    if (!/^[a-zA-ZÀ-ÿ\s]{1,40}$/.test(value)) {
      return "El campo es invalido";
    }
  };

  const isNumber = (value) => {
    if (!/^[0-9]*$/.test(value)) {
      return "El campo es Invalido";
    }
  };

  const isValidEmail = (value) => {
    if (!/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/.test(value)) {
      return "El correo electrónico no es valido";
    }
  };

  const validationSchema = (values) => {
    let errors = {};
    !values.name
      ? (errors.name = "Este campo es requerido")
      : (errors.name = isString(values.name));

    !values.nameToGift
      ? (errors.nameToGift = "Este campo es requerido")
      : (errors.nameToGift = isString(values.nameToGift));

    !values.lastName
      ? (errors.lastName = "Este campo es requerido")
      : (errors.lastName = isString(values.lastName));

    !values.phoneNumber
      ? (errors.phoneNumber = "Este campo es requerido")
      : (errors.phoneNumber = isNumber(values.phoneNumber));

    !values.identification
      ? (errors.identification = "Este campo es requerido")
      : (errors.identification = isNumber(values.identification));

    !values.email
      ? (errors.email = "Este campo es requerido")
      : (errors.email = isValidEmail(values.email));

    !values.location && (errors.location = "Este campo es requerido");

    !values.country && (errors.country = "Este campo es requerido");

    !values.location && (errors.location = "Este campo es requerido");

    return errors;
  };

  return (
    <Formik
      initialValues={{
        name: "",
        nameToGift: "",
        lastName: "",
        identification: "",
        identificationToGift: "",
        phoneNumber: "",
        email: "",
        country: "",
        location: "",
        province: "",
        checked: [],
      }}
      validate={(values) => validationSchema(values)}
    >
      {({
        errors,
        values,
        touched,
        handleBlur,
        handleChange,
        handleSubmit,
      }) => (
        <Form>
          <div className="row">
            <div className="col-lg-6 col-sm-12 pt-4">
              <label htmlFor="name" className="form-label is-size-8">
                Nombre
              </label>
              <Field
                id="name"
                className="form-control rounded-0"
                type="text"
                name="name"
                value={values.name}
                onChange={handleChange}
                onBlur={handleBlur}
              />
              {touched.name && errors.name && (
                <div className="formError">
                  {" "}
                  <div className="formDot"></div> {errors.name}
                </div>
              )}
            </div>
            <div className="col-lg-6 col-sm-12 pt-4">
              <label htmlFor="lastName" className="form-label is-size-8">
                Apellido
              </label>
              <Field
                id="lastName"
                className="form-control rounded-0"
                type="text"
                name="lastName"
                value={values.lastName}
                onChange={handleChange}
                onBlur={handleBlur}
              />
              {touched.lastName && errors.lastName && (
                <div className="formError">
                  {" "}
                  <div className="formDot"></div> {errors.lastName}
                </div>
              )}
            </div>
            <div className="col-lg-6 col-sm-12 pt-4">
              <label htmlFor="identification" className="form-label is-size-8">
                Doc de identidad
              </label>
              <Field
                id="identification"
                className="form-control rounded-0"
                type="text"
                name="identification"
                value={values.identification}
                onChange={handleChange}
                onBlur={handleBlur}
              />
              {touched.identification && errors.identification && (
                <div className="formError">
                  {" "}
                  <div className="formDot"></div> {errors.identification}
                </div>
              )}
            </div>
            <div className="col-lg-6 col-sm-12 pt-4">
              <label htmlFor="phoneNumber" className="form-label is-size-8">
                Número telefónico
              </label>
              <Field
                id="phoneNumber"
                className="form-control rounded-0"
                type="text"
                name="phoneNumber"
                value={values.phoneNumber}
                onChange={handleChange}
                onBlur={handleBlur}
              />
              {touched.phoneNumber && errors.phoneNumber && (
                <div className="formError">
                  {" "}
                  <div className="formDot"></div> {errors.phoneNumber}
                </div>
              )}
            </div>
            <div className="col-lg-6 col-sm-12 pt-4">
              <label htmlFor="email" className="form-label is-size-8">
                Correo Electrónico
              </label>
              <Field
                id="email"
                className="form-control rounded-0"
                type="text"
                name="email"
                value={values.email}
                onChange={handleChange}
                onBlur={handleBlur}
              />
              {touched.email && errors.email && (
                <div className="formError">
                  {" "}
                  <div className="formDot"></div> {errors.email}
                </div>
              )}
            </div>
            <div className="col-lg-6 col-sm-12 pt-4">
              <label htmlFor="country" className="form-label is-size-8">
                País
              </label>
              <select
                name="country"
                className="form-select is-size-8"
                aria-label="Default select example"
                value={values.country}
                onChange={handleChange}
                onBlur={handleBlur}
              >
                <option className="is-size-8" selected>
                  Selecciona un país
                </option>
                <option className="is-size-8" value="1">
                  Colombia
                </option>
                <option className="is-size-8" value="2">
                  Venezuela
                </option>
                <option className="is-size-8" value="3">
                  Otro
                </option>
              </select>
              {touched.country && errors.country && (
                <div className="formError">
                  {" "}
                  <div className="formDot"></div> {errors.country}
                </div>
              )}
            </div>
            <div className="col-lg-6 col-sm-12 pt-4">
              <label htmlFor="location" className="form-label is-size-8">
                Dirección de residencia
              </label>
              <Field
                id="location"
                className="form-control rounded-0"
                type="text"
                name="location"
                value={values.location}
                onChange={handleChange}
                onBlur={handleBlur}
              />
              {touched.location && errors.location && (
                <div className="formError">
                  {" "}
                  <div className="formDot"></div> {errors.location}
                </div>
              )}
            </div>
            <div className="col-lg-6 col-sm-12 pt-4">
              <label htmlFor="province" className="form-label is-size-8">
                Región - provincia
              </label>
              <select
                name="province"
                className="form-select is-size-8"
                aria-label="Default select example"
                value={values.province}
                onChange={handleChange}
                onBlur={handleBlur}
              >
                <option className="is-size-8" selected>
                  Selecciona un país
                </option>
                <option className="is-size-8" value="1">
                  Opción 1
                </option>
                <option className="is-size-8" value="2">
                  Opción 2
                </option>
                <option className="is-size-8" value="3">
                  Otro
                </option>
              </select>
              {touched.province && errors.province && (
                <div className="formError">
                  {" "}
                  <div className="formDot"></div> {errors.province}
                </div>
              )}
            </div>
          </div>
          <div className={`py-4 is-size-8 ${styles.flexBox}`}>
            <div className="">
              <label className="">
                ¿Este curso lo realizará otra persona? *
                <Field
                  className="mx-4"
                  type="checkbox"
                  name="checked"
                  value="FirstOption"
                />
              </label>
            </div>
          </div>
          {values.checked.map((item, index) => (
            <div key={index} className="">
              {item == "FirstOption" && (
                <div className="row pb-3">
                  <div className="col-lg-6 col-sm-12">
                    <label
                      htmlFor="nameToGift"
                      className="form-label is-size-8"
                    >
                      Nombre
                    </label>
                    <Field
                      id="nameToGift"
                      className="form-control rounded-0"
                      type="text"
                      name="nameToGift"
                      value={values.nameToGift}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                  </div>
                  <div className="col-lg-6 col-sm-12">
                    <label
                      htmlFor="identificationToGift"
                      className="form-label is-size-8"
                    >
                      Doc. de indentificacion
                    </label>
                    <Field
                      id="identificationToGift"
                      className="form-control rounded-0"
                      type="text"
                      name="identificationToGift"
                      value={values.identificationToGift}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                  </div>
                </div>
              )}
            </div>
          ))}
          <div
            className={`is-size-8 ${styles.flexBox}`}
            role="group"
            aria-labelledby="my-radio-group"
          >
            <div className="d-flex">
              <div className={styles.checkFieldMobile}>
                ¿Está comprando el producto Regala una boleta? *
              </div>
              <label className="mx-lg-4">
                <Field
                  className={`mx-2 ${styles.customCheck}`}
                  type="checkbox"
                  name="checked"
                  value="SecondOption"
                />
              </label>
            </div>
          </div>
          {/* <button type="submit">Submit</button> */}
        </Form>
      )}
    </Formik>
  );
};
