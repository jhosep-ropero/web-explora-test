import React, { useState } from "react";
import styles from "./FilterRangePlanetary.module.scss";
import { months } from "../../../../data/months.js";
import "react-modern-calendar-datepicker/lib/DatePicker.css";
import { Calendar } from "react-modern-calendar-datepicker";

export const FilterRangePlanetary = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [selectedDayRange, setSelectedDayRange] = useState({
    from: null,
    to: null,
  });

  const [isOption, setIsOption] = useState();

  const openRangeFilter = () => {
    !isOpen ? setIsOpen(true) : setIsOpen(false);
  };

  const switchOption = (e) => {
    e.target.id == "month" && setIsOption(0);
    e.target.id == "rangeDate" && setIsOption(1);
  };

  return (
    <div className={styles.relative}>
      <div role={"button"} className="text-white" onClick={openRangeFilter}>
        Rangos de fecha <i className="icon-explora icon-triangle-bot fs-6" />
      </div>
      <div className={isOpen ? styles.filterRange : "d-none"}>
        <div role={"button"} className="text-dark ms-lg-5 ps-lg-5" onClick={openRangeFilter}>
          Rangos de fecha <i className="icon-explora icon-triangle-bot fs-6" />
        </div>
        <div className={styles.contentFilter}>
          <div>
            <ul>
              <li>
                <input
                  type="radio"
                  id="month"
                  name="forRange"
                  onClick={(e) => switchOption(e)}
                />
                <label htmlFor="month">
                  <span></span> Por mes
                </label>
              </li>
              <li>
                <input
                  type="radio"
                  id="rangeDate"
                  name="forRange"
                  onClick={(e) => switchOption(e)}
                />
                <label htmlFor="rangeDate">
                  <span></span> Por rango de fecha
                </label>
              </li>
            </ul>
          </div>
          {!isOption && (
            <div className={styles.contentFilter_Month}>
              <select className="form-select">
                {months.map(({ id, month, text }) => (
                  <option
                    key={id}
                    value={month}
                    className="is-size-8 is-regular"
                  >
                    {text}
                  </option>
                ))}
              </select>
            </div>
          )}
          {!!isOption && (
            <div className={`planetary ${styles.contentFilter_Range}`}>
              <Calendar
                value={selectedDayRange}
                onChange={setSelectedDayRange}
                colorPrimary="#23ADB2"
                shouldHighlightWeekends
                locale="en"
              />
            </div>
          )}
          <div className="d-flex flex-column">
              <button className={styles.filterBtn} onClick={openRangeFilter}>
                    Seleccione Fecha
              </button>
              <button className={styles.filterBtn} onClick={openRangeFilter}>
                    Cancelar
              </button>
          </div>
        </div>
      </div>
    </div>
  );
};
