import React from "react";
import { DisplayContent } from "../DisplayContent/DisplayContent";
import { MainSlider } from "../MainSlider/MainSlider";
import styles from "./ProgramYourself.module.scss";

export const ProgramYourself = () => {
  return (
    <div className={styles.overlay}>
      <main className={styles.mainSection}>
        <MainSlider />
      </main>
      <section className={`container ${styles.overlayItem}`}>
        <DisplayContent />
      </section>
    </div>
  );
};
