import React from "react";
import Slider from "react-slick";
import SliderFullWidth from "../../../modules/SliderFullWidth";
import { Fragment } from "react";

export const MainSlider = () => {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
  };
  return (
    <div className="planetary">
      <div id="ProgramYourself" className="slick-tabs">
        <Slider {...settings} className="sliderplanetaryexhibitions">
          <SliderFullWidth
            minHeightSlider="588px"
            imgBackground="url(../../assets/planetary/program/programfeatured1.png)"
            title={
              <Fragment>
                <h1 className="is-size-1 is-light">
                  ¡Seamos <br />
                  protoestrellas!
                </h1>
                <p className="is-size-6 mb-5">
                  Convocatoria para jóvenes de colegio
                </p>
              </Fragment>
            }
            textButton="Ver mas"
            linkButton="/planetario"
          />
          <SliderFullWidth
            minHeightSlider="588px"
            imgBackground="url(../../assets/planetary/biblioteca-planetario.png)"
            title={
              <Fragment>
                <h1 className="is-size-1 is-light">
                  ¡Seamos <br />
                  protoestrellas!
                </h1>
                <p className="is-size-6 mb-5">
                  Convocatoria para jóvenes de colegio
                </p>
              </Fragment>
            }
            textButton="Ver mas"
            linkButton="/planetario"
          />
          <SliderFullWidth
            minHeightSlider="588px"
            imgBackground="url(../../assets/planetary/elsol.png)"
            title={
              <Fragment>
                <h1 className="is-size-1 is-light">
                  ¡Seamos <br />
                  protoestrellas!
                </h1>
                <p className="is-size-6 mb-5">
                  Convocatoria para jóvenes de colegio
                </p>
              </Fragment>
            }
            textButton="Ver mas"
            linkButton="/planetario"
          />
        </Slider>
      </div>
    </div>
  );
};
