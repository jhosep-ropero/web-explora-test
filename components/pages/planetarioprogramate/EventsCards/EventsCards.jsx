import Image from "next/image";
import React from "react";
import styles from "./EventsCards.module.scss";
export const EventsCards = ({
  img,
  type,
  categories,
  date,
  hours,
  title,
  price,
}) => {
  return (
    <div className={styles.cardBox}>
      <div
        className={styles.overlay}
        // style={{
        //   background: "url(/assets/planetary/program/programfeatured1.png)",
        //   backgroundSize: "324.03px 209.87px",
        //   height: "209.87px",
        // }}
      >
        <Image
          src={img}
          className="card-img-top img-fluid "
          alt="..."
          width="100%"
          height="52%"
          layout="responsive"
          objectFit="cover"
        />
        <div className={styles.overlayTitle}>{type}</div>
        <div className={styles.overlayCategory}>
          <p>Jóvenes y adultos</p>
          <p>Presencial</p>
        </div>
      </div>
      <div>
        <div className={styles.date}>
          <p>05</p>
          <p>Ago</p>
        </div>
        <div className={styles.contentBox}>
          <p className="horario">8:00 a 10:00 a.m</p>
          <div>
            <h1 className="is-size-6 is-regular">
              ¡Seamos protoestrellas! Convocatoria para jóvenes
            </h1>
          </div>
          <p className="is-size-6">$35000</p>
          <button className={`is-size-8 ${styles.outlineBtn}`}>Ver mas</button>
          <button className={`is-size-8 ${styles.btn}`}>Inscribirme</button>
        </div>
      </div>
    </div>
  );
};
