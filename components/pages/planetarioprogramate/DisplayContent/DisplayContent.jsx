import { EventsCards } from "../EventsCards/EventsCards";
import { programs } from "../../../../data/dataProgramYourself.js";
import styles from "./DisplayContent.module.scss";
import { FilterPlanetary } from "../FilterPlanetary/FilterPlanetary";
import { FilterRangePlanetary } from "../FilterRangePlanetary/FilterRangePlanetary";

export const DisplayContent = () => {
  return (
    <div>
      <div className={`d-flex gap-5 mb-5 pb-3 ${styles.mobile}`}>
        <FilterPlanetary />
        <FilterRangePlanetary />
      </div>
      <div className="d-flex ">
        <div className="row mx-auto">
          {programs.map((item, index) => (
            <div key={index} className="col-12 col-md-6 col-lg-3">
              <EventsCards img={item.img} type={item.type} />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};
