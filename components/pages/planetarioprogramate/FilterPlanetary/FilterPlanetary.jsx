import { useState } from "react";
import styles from "./FilterPlanetary.module.scss";

export const FilterPlanetary = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [filters, setFilters] = useState({
    museum: [],
    audience: [],
    modality: [],
    price: [],
  });

  const programsFilters = {
    museum: ["all", "explora", "planetary", "exploratory"],
    audience: ["all", "family", "guys", "youngandadults", "teachers"],
    modality: ["facetoface", "online"],
    price: ["free", "donate"],
  };

  const isChecked = (e) => {
    if (e.target.className == "museum") {
      if (e.target.checked) {
        console.log("es verdadero museo");
      }
      console.log("es de museo");
    }

    if (e.target.className == "audience") {
      if (e.target.checked) {
        console.log("es verdadero audiencia");
      }
    }

    if (e.target.className == "modality") {
      if (e.target.checked) {
        console.log("es verdadero audiencia");
      }
    }

    if (e.target.className == "price") {
      if (e.target.checked) {
        console.log("es verdadero audiencia");
      }
    }
    // console.log(e.target.checked);
    // console.log(e.target.id);
    // console.log(e.target.className);
  };

  const openFilter = () => {
    !isOpen ? setIsOpen(true) : setIsOpen(false);
  };

  return (
    <div className={styles.relative}>
      <div role={"button"} className="text-white" onClick={openFilter}>
        Filtros <i className="icon-explora icon-triangle-bot fs-6" />
      </div>
      <div className={isOpen ? styles.filter : "d-none"}>
        <div role={"button"} className="text-dark" onClick={openFilter}>
          Filtros <i className="icon-explora icon-triangle-bot fs-6" />
        </div>
        <div className={`row ${styles.filterOptions}`}>
          <div className="col-lg-3 col-md-3 col-sm-6">
            <p>Museo</p>
            <ul>
              <li>
                <input
                  type="checkbox"
                  onClick={(e) => isChecked(e)}
                  id="allMuseum"
                  className="museum"
                />
                <label htmlFor="allMuseum">
                  <span></span> Todo el museo
                </label>
              </li>
              <li>
                <input
                  type="checkbox"
                  onClick={(e) => isChecked(e)}
                  id="explora"
                  className="museum"
                />
                <label htmlFor="explora">
                  <span></span>Parque Explora
                </label>
              </li>
              <li>
                <input
                  type="checkbox"
                  onClick={(e) => isChecked(e)}
                  id="planetary"
                  className="museum"
                />

                <label htmlFor="planetary">
                  <span></span>Planetario
                </label>
              </li>
              <li>
                <input
                  type="checkbox"
                  onClick={(e) => isChecked(e)}
                  id="exploratory"
                  className="museum"
                />
                <label htmlFor="exploratory">
                  <span></span>Exploratorio
                </label>
              </li>
            </ul>
          </div>
          <div className="col-lg-3 col-md-3 col-sm-6">
            <p>Público</p>
            <ul>
              <li>
                <input
                  type="checkbox"
                  onClick={(e) => isChecked(e)}
                  id="allPublic"
                  className="audience"
                />
                <label htmlFor="allPublic">
                  <span></span> Todo los públicos
                </label>
              </li>
              <li>
                <input
                  type="checkbox"
                  onClick={(e) => isChecked(e)}
                  id="family"
                  className="audience"
                />
                <label htmlFor="family">
                  <span></span>Familiar
                </label>
              </li>
              <li>
                <input
                  type="checkbox"
                  onClick={(e) => isChecked(e)}
                  id="guys"
                  className="audience"
                />
                <label htmlFor="guys">
                  <span></span>Niños
                </label>
              </li>
              <li>
                <input
                  type="checkbox"
                  onClick={(e) => isChecked(e)}
                  id="youngandadults"
                  className="audience"
                />
                <label htmlFor="youngandadults">
                  <span></span>Jóvenes y adultos
                </label>
              </li>
              <li>
                <input
                  type="checkbox"
                  onClick={(e) => isChecked(e)}
                  id="teachers"
                  className="audience"
                />
                <label htmlFor="teachers">
                  <span></span>Maestros
                </label>
              </li>
            </ul>
          </div>
          <div className="col-lg-3 col-md-3 col-sm-6">
            <p>Modalidad</p>
            <ul>
              <li>
                <input
                  type="checkbox"
                  onClick={(e) => isChecked(e)}
                  id="facetoface"
                  className="modality"
                />
                <label htmlFor="facetoface">
                  <span></span>Presencial
                </label>
              </li>
              <li>
                <input
                  type="checkbox"
                  onClick={(e) => isChecked(e)}
                  id="online"
                  className="modality"
                />
                <label htmlFor="online">
                  <span></span>Online
                </label>
              </li>
            </ul>
          </div>
          <div className="col-lg-3 col-md-3 col-sm-6">
            <p>Precio</p>
            <ul>
              <li>
                <input
                  type="checkbox"
                  onClick={(e) => isChecked(e)}
                  id="free"
                  className="price"
                />
                <label htmlFor="free">
                  <span></span>Sin Costo
                </label>
              </li>
              <li>
                <input
                  type="checkbox"
                  onClick={(e) => isChecked(e)}
                  id="donate"
                  className="price"
                />
                <label htmlFor="donate">
                  <span></span>Donación voluntarria
                </label>
              </li>
            </ul>
          </div>
        </div>
        <div className="text-center">
          <button className={` ${styles.filterBtn}`} onClick={openFilter}>
            ELEGIR
          </button>
        </div>
      </div>
    </div>
  );
};
