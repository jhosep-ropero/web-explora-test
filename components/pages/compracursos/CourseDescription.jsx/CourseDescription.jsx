import Link from "next/link";
import React from "react";
import styles from "./CourseDescription.module.scss";

export const CourseDescription = ({
  largeDescriptionFirstPart,
  largeDescriptionSecondPart,
  requirements,
  madality,
}) => {
  return (
    <div id="description" className="container">
      <hr size="2px" />
      <p className="is-bold is-size-4 pt-5 pb-3">Descripción del curso</p>
      <div className="row">
        <div className="col-lg-8 col-sm-12">
          <p className="is-size-6 is-regular pb-lg-3">
            {largeDescriptionFirstPart}
          </p>
          <p className={`is-size-8 is-regular ${styles.isHiddenMobile}`}>
            {largeDescriptionSecondPart}
          </p>
          <div className={`${styles.isHiddenMobile} ${styles.buttonContainer}`}>
            <button className={styles.addCartButton}>
              <i className="icon-explora icon-cart fs-4" />
              <p className="m-0">AGREGAR AL CARRO</p>
            </button>
            <Link href={`/tienda/carrito`} passHref>
              <a href="">
                <button className={`is-size-9 ${styles.shopNowButton}`}>
                  COMPRAR AHORA
                  <i className="icon-explora icon-arrow-right" />
                </button>
              </a>
            </Link>
          </div>
          <div className={styles.isHiddenMobile}>
            <Link href={"#product"} passHref>
              <a href="">
                <p className={`is-size-9 ${styles.link}`}>
                  Regresar al producto
                </p>
              </a>
            </Link>
          </div>
        </div>
        <div className={`container col-lg-4 col-sm-12 ${styles.paddingMobile}`}>
          <p className="pb-3 is-size-8">
            <span className="is-bold">Este taller no requiere </span>
            conocimientos previos, deberás desplazarte a El Planetario en
            Medellín.
          </p>
          <hr size="2px" />
          <p className="py-3 is-size-8">
            <span className="is-bold">El curso se lleva a cabo</span> con un
            mínimo de 8 participantes, si no se logra llenar, se realizará
            devolución del dinero.
          </p>
          <hr size="2px" />
          <p className="py-3 is-size-8">
            <span className="is-bold">Este curso es presencial</span> , conoce
            nuestros protocolos de bioseguridad
            <a className="text-primary is-bold"> aquí</a> .
          </p>
          <hr size="2px" />
          <p className="pt-3 is-size-8">
            Para dar cumplimiento al decreto 1408 de 2021, deberás presentar el
            carné de vacunación al ingresar. Los menores de entre 0 y 12 años se
            encuentran exceptuados de dicha medida.
          </p>
          <div className={`${styles.isMobile} ${styles.buttonContainer}`}>
            <button className={styles.addCartButton}>
              <i className="icon-explora icon-cart fs-4" />
              <p className="m-0">AGREGAR AL CARRO</p>
            </button>
            <Link href={`/tienda/carrito`} passHref>
              <a href="">
                <button className={`is-size-9 ${styles.shopNowButton}`}>
                  COMPRAR AHORA
                  <i className="icon-explora icon-arrow-right" />
                </button>
              </a>
            </Link>
          </div>
          <div className={`${styles.isMobile} ${styles.mobileFlex}`}>
            <Link href={"#product"} passHref>
              <a href="">
                <p className={`is-size-8 ${styles.link}`}>
                  Regresar al producto
                </p>
              </a>
            </Link>
            <p className={`is-size-8 ${styles.link}`}>
              Descargar ficha del curso{" "}
              <i className="icon-explora icon-download is-size-9" />{" "}
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};
