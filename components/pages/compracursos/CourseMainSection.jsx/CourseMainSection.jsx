import Image from "next/image";
import Link from "next/link";
import poster from "../../../../public/assets/courses/poster1.svg";
import Buttons from "../../../ui/Buttons/Buttons";
import PhotoGallery from "../ShopGallery/PhotoGallery";
import styles from "./CourseMainSection.module.scss";
import { course1 } from "../../../../data/imagescourses";

export const CourseMainSection = ({
  img,
  title,
  shortDescription,
  price,
  audience,
  dateCourse,
  modality,
  hourCourse,
  quota,
  teacher,
}) => {
  return (
    <div className="container">
      <div className="row">
        <div className="col-lg-6 col-sm-12">
          <PhotoGallery images={course1} />
          {/* <Image src={poster} alt="poster-course" /> */}
        </div>
        <div className="col-lg-6 col-sm-12">
          <div>
            <h1 className="is-size-4 is-bold">{title}</h1>
            <p className="is-size-7 is-regular">{shortDescription}</p>
            <p className="is-size-5 text-primary is-bold">{price}</p>
          </div>
          <div className={styles.mobilePadding}>
            <div className="d-flex">
              <i className="icon-explora icon-client text-dark me-3" />
              <p className="is-size-8 is-regular">{audience}</p>
            </div>
            <div className="d-flex">
              <i className="icon-explora icon-calendar text-dark me-3" />
              <p className="is-size-8 is-regular">{dateCourse}</p>
            </div>
            <div className="d-flex">
              <i className="icon-explora icon-location text-dark me-3" />
              <p className="is-size-8 is-regular">
                Parque explora - Modalidad {modality}
              </p>
            </div>
            <div className="d-flex">
              <i className="icon-explora icon-clock text-dark me-3" />
              <p className="is-size-8 is-regular">{hourCourse}</p>
            </div>
            <div className="d-flex">
              <i className="icon-explora icon-users text-dark me-3" />
              <p className="is-size-8 is-regular">Cupos disponibles: {quota}</p>
            </div>
            <div className="d-flex">
              <i className="icon-explora icon-teacher text-dark me-3" />
              <p className="is-size-8 is-regular">Profesor: {teacher}</p>
            </div>
          </div>
          <div className="d-flex gap-2">
            <div className="">
              <div className={styles.counterButton}>
                <i className={`icon-explora icon-less ${styles.less}`} />
                1
                <i className={`icon-explora icon-plus ${styles.plus}`} />
              </div>
            </div>
            <div className="col">
              <button className={styles.addCartButton}>
                <i className="icon-explora icon-cart fs-4" />
                <p className="m-0">AGREGAR AL CARRO</p>
              </button>
            </div>
          </div>
          <div className={styles.mobileMarginBot}>
            <Link href={`/tienda/carrito`} passHref>
              <a href="">
                <button className={styles.shopNowButton}>
                  COMPRAR AHORA
                  <i className="icon-explora icon-arrow-right" />
                </button>
              </a>
            </Link>
          </div>
          <div className={`${styles.isHiddenMobile} ${styles.extraData}`}>
            <Link href={"#description"} passHref>
              <a href="">
                <p>Descripción del curso</p>
              </a>
            </Link>
            <p>
              Descargar ficha del curso{" "}
              <i className="icon-explora icon-download ms-2" />{" "}
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};
