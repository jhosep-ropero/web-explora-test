export default function RightNav({ disabled, onClick }) {
  return (
    <button
      type="button"
      className="image-gallery-icon image-gallery-right-nav"
      disabled={disabled}
      onClick={onClick}
      aria-label="Next Slide"
    >
      <span className="icon-explora icon-arrow-right-cirlcle text-white is-size-4"></span>
    </button>
  );
}
