import React from "react";
import ImageGallery from "react-image-gallery";

import LeftNav from "./LeftNav";
import RightNav from "./RightNav";

export default function PhotoGallery({ images }) {
  return (
    <div id="shopGallery">
      <div className="">
        <ImageGallery
          items={images}
          showPlayButton={false}
          showIndex={true}
          renderLeftNav={(onClick, disabled) => (
            <LeftNav onClick={onClick} disabled={disabled} />
          )}
          renderRightNav={(onClick, disabled) => (
            <RightNav onClick={onClick} disabled={disabled} />
          )}
        />
        <div>{images.title}</div>
      </div>
    </div>
  );
}
