import Link from "next/link";
import React from "react";
import LinkDiv from "../../visitanos/Rooms/singleRooms/LinkDiv";
import styles from "./ActionBar.module.scss";
export const ActionBar = ({ container, room }) => {
  return (
    <div id="product">
      <div className="pb-3">
        <div className={`${styles.link}  container d-flex align-items-center `}>
          <Link href={"/visitanos"} passHref>
            <a href="" className={styles.btnLink}>
              <i
                className={`icon-explora icon-arrow-left-cirlcle text-black`}
              ></i>
            </a>
          </Link>
          <div className="ps-4 ps-lg-1">
            <h6 className={`text-black fw-light is-size-9`}>
              {" "}
              Cursos &gt; Los Colores del Sol
            </h6>
          </div>
        </div>
        <div className={styles.cartOverlay}>
          <Link href={`/tienda/carrito`} passHref>
            <a href="">
              <i className={`icon-explora icon-cart fs-2`} />
            </a>
          </Link>
        </div>
      </div>
    </div>
  );
};
