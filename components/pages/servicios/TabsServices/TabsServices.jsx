import { Tabs, Tab, Row, Col, Nav } from "react-bootstrap";

// Components
import DescriptionService from "../DescriptionService/DescriptionService";

// Import styles
import styles from "./TabsServices.module.scss";

const TabsServices = ({ tab, setTab }) => {
  return (    
    <Tab.Container      
      defaultActiveKey={tab}
      onSelect={(k) => setTab(k)}     
    >
      <div>
        <div className={`col-12 ${styles.tabsServices}`}>
          <Nav className={`d-flex justify-content-between`}>
            <Nav.Item className={`${tab === 'comunidad' ? styles.tabActive  :  styles.tabItem}`} >
              <Nav.Link eventKey="comunidad" className="is-size-5"><p>Comunidad educativa</p> <span className="activeLine"/></Nav.Link>              
            </Nav.Item>
            <Nav.Item className={`${tab === 'empresas' ? styles.tabActiveGray :  styles.tabItem}`}>
              <Nav.Link eventKey="empresas" className="is-size-5"><p>Empresas</p></Nav.Link>
            </Nav.Item>
            <Nav.Item  className={`${tab === 'publico' ? styles.tabActive:  styles.tabItem}`}>
              <Nav.Link eventKey="publico" className="is-size-5"><p>Público general</p></Nav.Link>
            </Nav.Item>
          </Nav>
        </div>  
        <div className="col-12">
          <Tab.Content>
            <Tab.Pane eventKey="comunidad">
              <DescriptionService 
                title="Promovemos el aprendizaje libre, lúdico e interactivo"
                description={<>La experimentación con fenómenos y objetos de la naturaleza, promovemos el aprendizaje libre, lúdico e interactivo, la experimentación con fenómenos y objetos de la naturaleza, y la interacción con las creaciones científicas y tecnológicas de la humanidad. <br/><br/> Con el fin de orientar este cruce de experiencias a partir de qué decimos, cómo lo decimos y con quién lo decimos el Parque se compromete con cuatro ejes fundamentales, cuatro líneas mediante las cuales queremos generar cambios y contribuir a la consolidación de una sociedad más consciente de su compromiso con el mundo: 1-Construcción de ciudadanía / Pensamiento crítico. 2- Apropiación del conocimiento / Pensamiento prosumidor. 3-Inclusión social / Diálogo de saberes. 4-Educación para la conservación de la biodiversidad / Convivencia plural.</>}
                bgColor="#F45858"
              />
            </Tab.Pane>
            <Tab.Pane eventKey="empresas">
              <DescriptionService 
                title="Parque Explora es un escenario ideal para compartir"
                description={<>Parque Explora es un escenario ideal para fortalecer competencias y desarrollar procesos de aprendizaje organizacional. Por eso, ofrecemos a las empresas programas para estimular la creatividad, el trabajo en equipo, la adaptabilidad y la comunicación, entre otras habilidades.Ofrecemos rutas cortas de dos horas en adelante en Parque Explora o el Planetario, y de tres horas en el Taller de Innovación Explora. <br/><br/> Nuestros contenidos están hechos a la medida del día a día de las organizaciones, para promover el trabajo en equipo, la adaptación al cambio, la obtención de resultados, la comunicación, el cuestionamiento, la audacia, la creatividad y la innovación. </>}
                bgColor="#44444A"
              />
            </Tab.Pane>
            <Tab.Pane eventKey="publico">
              <DescriptionService 
                title="Promovemos el aprendizaje libre, lúdico e interactivo"
                description={<>La experimentación con fenómenos y objetos de la naturaleza, promovemos el aprendizaje libre, lúdico e interactivo, la experimentación con fenómenos y objetos de la naturaleza, y la interacción con las creaciones científicas y tecnológicas de la humanidad. <br/><br/> Con el fin de orientar este cruce de experiencias a partir de qué decimos, cómo lo decimos y con quién lo decimos el Parque se compromete con cuatro ejes fundamentales, cuatro líneas mediante las cuales queremos generar cambios y contribuir a la consolidación de una sociedad más consciente de su compromiso con el mundo: 1-Construcción de ciudadanía / Pensamiento crítico. 2- Apropiación del conocimiento / Pensamiento prosumidor. 3-Inclusión social / Diálogo de saberes. 4-Educación para la conservación de la biodiversidad / Convivencia plural.</>}
                bgColor="#F45858"
              />
            </Tab.Pane>
          </Tab.Content>
        </div>
      </div>
    </Tab.Container>
  );
};

export default TabsServices;
