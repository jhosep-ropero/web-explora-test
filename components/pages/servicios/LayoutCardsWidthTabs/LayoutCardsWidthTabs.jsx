import { useState } from "react";

// Components
import InfoServices from "../InfoServices/InfoServices";
import PortfolioServices from "../PortfolioServices/PortfolioServices";

const LayoutCardsWidthTabs = () => {
  const [tab, setTab] = useState("comunidad");

  return (
    <>
      <InfoServices title={"Servicios"} tab={tab} setTab={setTab} />
      <PortfolioServices tab={tab} />
    </>
  );
};

export default LayoutCardsWidthTabs;
