import Image from "next/image";

// Styles
import styles from './CardPortfolio.module.scss';

const CardPortfolio = ({img, title}) => {
    return (
        <div className={styles.cardPortfolio}>            
            <Image 
                src={img} 
                className="card-img-top img-fluid"
                alt={`Imagen ${title}`}
                width="343px"
                height="233px"
                layout="responsive"
            />
            <div className={styles.cardPortfolio_Info}>            
                <h5 className="d-flex align-items-center card-title text-uppercase pt-2">{title}</h5>
                <button className="btn btn-info text-uppercase text-white fs-6 py-2 px-4 w-100">ver más</button>
            </div>
        </div>
    );
}
 
export default CardPortfolio;