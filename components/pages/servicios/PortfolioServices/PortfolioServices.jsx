// Components
import CardPortfolio from "../CardPortfolio/CardPortfolio";

// Data
import { portfolioServices } from "../../../../data/portfolioServices";

// Styles
import styles from './PortfolioServices.module.scss';

const PortfolioServices = ({tab}) => {  
  return (
    <> 
      <div className={`${tab === 'empresas' ? styles.portfolioServicesGray : styles.portfolioServices} `} >
        <h2 className="container mb-0 text-uppercase fw-normal is-size-5">
          nuestro portafolio para 2022
        </h2>
      </div>      
      <div className={styles.portfolioServices_bgGray} />      
      <div className={`container d-flex justify-content-center ${styles.portfolioServices_List}`}>
        <div className="row">
          {
            // Iterando sobre el arreglo
            portfolioServices.map((service, index) => (
              <div className={`col-12 col-md-6 col-lg-3`} key={index}>
                <CardPortfolio img={service.img} title={service.title} />
              </div>
            ))
          }
        </div>
      </div>           
    </>
  );
};

export default PortfolioServices;
