import Image from 'next/image';

// Data
import logoArrowDown from '../../../../public/assets/icon-arrow-bottom.png';

// Styles
import styles from './DescriptionService.module.scss';

const DescriptionService = ({title, description, bgColor}) => {
    return (
        <div className={styles.descriptionService} style={{background: `${bgColor}`}}>
            <div className="container">
                <div className="row flex-column flex-md-row">
                    <div className="col-12 col-lg-4 position-relative">
                        <h2 className='is-size-bold is-size-4 mb-0'>{title}</h2>
                        <a href="#">
                            <Image src={logoArrowDown} alt={`logo ${logoArrowDown}`} />
                        </a>
                    </div>
                    <div className="col-12 col-lg-8">
                    <p className='is-size-8 mb-0'>{description}</p>                  
                    </div>
                </div>                
            </div>
        </div>
    );
}
 
export default DescriptionService;