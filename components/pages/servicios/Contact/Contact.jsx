// Components
import ButtonTransparent from '../../../ui/ButtonTransparent/ButtonTransparent';
import SeparatorFullWidth from '../../../ui/SeparatorFullWidth/SeparatorFullWidth';

// Data
import logoWpp from '../../../../public/assets/icon-wpp.png';

// Styles
import styles from './Contact.module.scss';

const Contact = () => {
    return (
        <div className={styles.contact}>
            <ButtonTransparent 
                text="¿Tienes dudas? ¡Escríbenos"
                logo={logoWpp}
            />
            <SeparatorFullWidth color="#F45858" />            
        </div>
    );
}
 
export default Contact;