// Components
import PageBar from "../../../ui/PageBar/PageBar";
import TabsServices from "../TabsServices/TabsServices";

// Styles
import styles from "./InfoServices.module.scss";

const InfoServices = ({ title, tab, setTab }) => {
  return (
    <>
      {tab === "comunidad" && (
        <div className="">
          <div
            className={`${styles.infoServices} ${styles.educationalCommunity}`}
          >
            <PageBar title={title} color={"#FFCB05"} transparent={true} />
          </div>
          <TabsServices tab={tab} setTab={setTab} />         
        </div>
      )}
      {tab === "empresas" && (
        <div className="">
          <div className={`${styles.infoServices} ${styles.enterprises}`}>
            <PageBar title={title} color={"#FFCB05"} transparent={true} />
          </div>
          <TabsServices tab={tab} setTab={setTab} />         
        </div>
      )}
      {tab === "publico" && (
        <div className="">
          <div className={`${styles.infoServices} ${styles.generalPublic}`}>
            <PageBar title={title} color={"#FFCB05"} transparent={true} />
          </div>
          <TabsServices tab={tab} setTab={setTab} />         
        </div>
      )}
    </>
  );
};

export default InfoServices;
