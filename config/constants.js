const constants = {
  planetaryHome: "planetario",
  exploraHome: "explora",
  exploratorioHome: "exploratorio",
  visitUs: "visitanos",
  contractUs: "contrata",
  ourProgramming: "events",
};

export default constants;
