import Cosmic from "cosmicjs";
const api = Cosmic();

const bucket = api.bucket({
  slug: process.env.COSMIC_BUCKET_SLUG,
  read_key: process.env.COSMIC_READ_KEY,
});

export function getPageBySlug(slug, props, others) {
  return new Promise((resolve) => {
    bucket
      .getObjects({
        query: {
          slug,
        },
        props,
        ...others,
      })
      .then((res) => {
        resolve(res.objects[0]);
      })
      .catch((err) => {
        console.log(err);
        throw err;
      });
  });
}

export function getByType(type, props, others) {
  return new Promise((resolve) => {
    bucket
      .getObjects({
        query: {
          type,
        },
        props,
        ...others,
      })
      .then((res) => {
        resolve(res.objects);
      })
      .catch((err) => {
        console.log(err);
        throw err;
      });
  });
}
