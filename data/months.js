export const months = [{
        id: "01M",
        text: "Enero",
        month: "ENE"
    },
    {
        id: "02M",
        text: "Febrero",
        month: "FEB"
    },
    {
        id: "03M",
        text: "Marzo",
        month: "MAR"
    },
    {
        id: "04M",
        text: "Abril",
        month: "ABR"
    },
    {
        id: "05M",
        text: "Mayo",
        month: "MAY"
    },
    {
        id: "06M",
        text: "Junio",
        month: "JUN"
    },
    {
        id: "07M",
        text: "Julio",
        month: "JUL"
    },
    {
        id: "08M",
        text: "Agosto",
        month: "AGO"
    },
    {
        id: "09M",
        text: "Septiembre",
        month: "SEP"
    },
    {
        id: "10M",
        text: "Octubre",
        month: "OCT"
    },
    {
        id: "11M",
        text: "Noviembre",
        month: "NOV"
    },
    {
        id: "12M",
        text: "Diciembre",
        month: "DIC"
    }
]