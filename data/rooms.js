export const rooms = {
    acuario: {
        background1: '/assets/rooms/slide-acuario1.png',
        title1: 'En el Acuario habitan especies de agua dulce  y salada, así es Colombia',
        background2: '/assets/rooms/slide-acuario2.png',
        title2: 'Nuestro acuario cuenta con 2583 individuos y 252 especies que son parte de programas de conservación',
        gallery: [{
            original: '/assets/rooms/acuario/img1.png',
            thumbnail: '/assets/rooms/acuario/img1-thumb.png',
            description: 'Red Lionfish (Pterois volitans)'
        },
        {
            original: '/assets/rooms/acuario/img2.png',
            thumbnail: '/assets/rooms/acuario/img2-thumb.png',
            description: 'Red Lionfish (Pterois volitans)'
        },
        {
            original: '/assets/rooms/acuario/img3.png',
            thumbnail: '/assets/rooms/acuario/img3-thumb.png',
            description: 'Red Lionfish (Pterois volitans)'
        },
        {
            original: '/assets/rooms/acuario/img1.png',
            thumbnail: '/assets/rooms/acuario/img1-thumb.png',
            description: 'Red Lionfish (Pterois volitans)'
        },
        {
            original: '/assets/rooms/acuario/img2.png',
            thumbnail: '/assets/rooms/acuario/img2-thumb.png',
            description: 'Red Lionfish (Pterois volitans)'
        },
        {
            original: '/assets/rooms/acuario/img3.png',
            thumbnail: '/assets/rooms/acuario/img3-thumb.png',
            description: 'Red Lionfish (Pterois volitans)'
        }],
        playList: [{
            id:"0001",
            title: "Gran Acuario de Medellín En El Parque explora",
            img: "rooms/img-video1"
        },
        {
            id:"0002",
            title: <>Acuario <br/> El Parque explora</>,
            img: "rooms/img-video2"
        },
        {
            id:"0003",
            title:<>Wade <br/> Davis En explora</> ,
            img: "rooms/img-video3"
        },
        {
            id:"0004",
            title: "Gran Acuario de Medellín En El Parque explora",
            img: "rooms/img-video4"
        },
        {
            id:"0005",
            title: <>Acuario <br/>Parque explora</>,
            img: "rooms/img-video5"
        },
        {
            id:"0006",
            title: <>Exploradores <br/>acuáticos</>,
            img: "rooms/img-video6"
        },
        {
            id:"0007",
            title: "Gran Acuario de Medellín En El Parque explora",
            img: "rooms/img-video7"
        },
        {
            id:"0008",
            title: <>Acuario <br/>Parque explora</>,
            img: "rooms/img-video8"
        }],
        info: {
            background: '/assets/rooms/acuario/fondo-descripcion.png',
            title: 'Acuario',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam laoreet egestas placerat. Aliquam in sapien eget augue molestie dignissim eu ut purus. Aliquam quis quam ante. Nulla sit amet auctor neque. Aliquam rutrum, arcu sit amet tincidunt vestibulum, eros nulla tempor urna, at cursus tortor ipsum vitae elit. Nullam tincidunt ac Leo tempor ornare. Etiam at rhoncus purus. Phasellus arcu sapien, vehicula mattis urna id, ultrices euismod risus. Praesent iaculis rhoncus arcu et condimentum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam laoreet egestas placerat. Aliquam in sapien eget augue molestie dignissim eu ut purus. Aliquam quis quam ante. Nulla sit amet auctor neque. Aliquam rutrum, arcu sit amet tincidunt vestibulum',
            reconocimientos:[
                'Premio Travellers’ Choice, 2013.',
                'Certificado de Excelencia de TripAdvisor®, 2016.',
                'Puesto número 16 en el ranquin TripAdvisor® de los 25 acuarios más recomendados del mundo, 2014.',
                'Moción de reconocimiento como Amigo de la Escuela de Microbiología de la Universidad de Antioquia, 2015.'
            ]
        }
    },
    vivario:{
        background1: '/assets/rooms/vivario/slide-vivario1.png',
        background2: '/assets/rooms/vivario/slide-vivario2.png',
        gallery: [{
            original: '/assets/rooms/vivario/img1.png',
            thumbnail: '/assets/rooms/vivario/img1-thumb.png',
            description: 'Red Lionfish (Pterois volitans)'
        },
        {
            original: '/assets/rooms/vivario/img2.png',
            thumbnail: '/assets/rooms/vivario/img2-thumb.png',
            description: 'Red Lionfish (Pterois volitans)'
        },
        {
            original: '/assets/rooms/vivario/img3.png',
            thumbnail: '/assets/rooms/vivario/img3-thumb.png',
            description: 'Red Lionfish (Pterois volitans)'
        }],
        playList: [{
            id:"0001",
            title: <>Vivario <br/> El Parque explora</>,
            img: "rooms/vivario/img-video1"
        },
        {
            id:"0002",
            title: <>Ranitas comiendo <br/> En el vivario explora</>,
            img: "rooms/vivario/img-video2"
        },
        {
            id:"0003",
            title:<>Rana dorada <br/> Cantando</> ,
            img: "rooms/vivario/img-video3"
        },
        {
            id:"0004",
            title: <>Cortejo de los geckos <br/> en el #VivarioExplora</>,
            img: "rooms/vivario/img-video4"
        },
        {
            id:"0005",
            title: <>Lorem ipsum <br/>is simply</>,
            img: "rooms/vivario/img-video5"
        },
        {
            id:"0006",
            title: <>Lorem ipsum <br/>is simply</>,
            img: "rooms/vivario/img-video6"
        },
        {
            id:"0007",
            title: <>Lorem ipsum <br/>is simply</>,
            img: "rooms/vivario/img-video7"
        },
        {
            id:"0008",
            title: <>Lorem ipsum <br/>is simply</>,
            img: "rooms/vivario/img-video8"
        }],
        info: {
            background: '/assets/rooms/vivario/fondo-descripcion.png',
            title: 'Vivario',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam laoreet egestas placerat. Aliquam in sapien eget augue molestie dignissim eu ut purus. Aliquam quis quam ante. Nulla sit amet auctor neque. Aliquam rutrum, arcu sit amet tincidunt vestibulum, eros nulla tempor urna, at cursus tortor ipsum vitae elit. Nullam tincidunt ac Leo tempor ornare. Etiam at rhoncus purus. Phasellus arcu sapien, vehicula mattis urna id, ultrices euismod risus. Praesent iaculis rhoncus arcu et condimentum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam laoreet egestas placerat. Aliquam in sapien eget augue molestie dignissim eu ut purus. Aliquam quis quam ante. Nulla sit amet auctor neque. Aliquam rutrum, arcu sit amet tincidunt vestibulum',
        }
    },
    salamente:{
        background1: '/assets/rooms/salamente/slide-smente1.png',
        background2: '/assets/rooms/salamente/slide-smente2.png',
        gallery: [{
            original: '/assets/rooms/salamente/img1.png',
            thumbnail: '/assets/rooms/salamente/img1-thumb.png',
            description: 'Red Lionfish (Pterois volitans)'
        },
        {
            original: '/assets/rooms/salamente/img2.png',
            thumbnail: '/assets/rooms/salamente/img2-thumb.png',
            description: 'Red Lionfish (Pterois volitans)'
        },
        {
            original: '/assets/rooms/salamente/img3.png',
            thumbnail: '/assets/rooms/salamente/img3-thumb.png',
            description: 'Red Lionfish (Pterois volitans)'
        },
        {
            original: '/assets/rooms/salamente/img1.png',
            thumbnail: '/assets/rooms/salamente/img1-thumb.png',
            description: 'Red Lionfish (Pterois volitans)'
        },
        {
            original: '/assets/rooms/salamente/img2.png',
            thumbnail: '/assets/rooms/salamente/img2-thumb.png',
            description: 'Red Lionfish (Pterois volitans)'
        },
        {
            original: '/assets/rooms/salamente/img3.png',
            thumbnail: '/assets/rooms/salamente/img3-thumb.png',
            description: 'Red Lionfish (Pterois volitans)'
        }],
        playList: [{
            id:"0001",
            title: <>Vivario <br/> El Parque explora</>,
            img: "rooms/vivario/img-video1"
        },
        {
            id:"0002",
            title: <>Ranitas comiendo <br/> En el vivario explora</>,
            img: "rooms/vivario/img-video2"
        },
        {
            id:"0003",
            title:<>Rana dorada <br/> Cantando</> ,
            img: "rooms/vivario/img-video3"
        },
        {
            id:"0004",
            title: <>Cortejo de los geckos <br/> en el #VivarioExplora</>,
            img: "rooms/vivario/img-video4"
        },
        {
            id:"0005",
            title: <>Lorem ipsum <br/>is simply</>,
            img: "rooms/vivario/img-video5"
        },
        {
            id:"0006",
            title: <>Lorem ipsum <br/>is simply</>,
            img: "rooms/vivario/img-video6"
        },
        {
            id:"0007",
            title: <>Lorem ipsum <br/>is simply</>,
            img: "rooms/vivario/img-video7"
        },
        {
            id:"0008",
            title: <>Lorem ipsum <br/>is simply</>,
            img: "rooms/vivario/img-video8"
        }],
        info: {
            background: '/assets/rooms/salamente/fondo-salamente.png',
            title: 'Sala mente',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam laoreet egestas placerat. Aliquam in sapien eget augue molestie dignissim eu ut purus. Aliquam quis quam ante. Nulla sit amet auctor neque. Aliquam rutrum, arcu sit amet tincidunt vestibulum, eros nulla tempor urna, at cursus tortor ipsum vitae elit. Nullam tincidunt ac Leo tempor ornare. Etiam at rhoncus purus. Phasellus arcu sapien, vehicula mattis urna id, ultrices euismod risus. Praesent iaculis rhoncus arcu et condimentum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam laoreet egestas placerat. Aliquam in sapien eget augue molestie dignissim eu ut purus. Aliquam quis quam ante. Nulla sit amet auctor neque. Aliquam rutrum, arcu sit amet tincidunt vestibulum',
        }
    },
}