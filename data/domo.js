export const domo = [
  {
    id: "0001",
    hour: "10:00 a.m",
    title: "Lucía, el secreto de las estrellas fugaces",
    category: "Infantil",
    description:
      "Ven con tus hijos al Planetario para conocer a Lucía, una colibrí apasionada por las rocas que viajará al espacio con Vladimir, un oso polar y James, un pingüino, para resolver el enigma de las “piedras de luz”. Juntos viajarán a la Luna, al cinturón de asteroides y aterrizarán en el núcleo de un cometa; usando el método científico, hipótesis, observaciones y análisis encontrarán la respuesta al misterio de las estrellas fugaces. ",
  },
  {
    id: "0002",
    hour: "11:00 a.m",
    title: "El Sol: nuestra estrella de vida.",
    category: "Nuevo Show",
    description:
      "Ven con tus hijos al Planetario para conocer a Lucía, una colibrí apasionada por las rocas que viajará al espacio con Vladimir, un oso polar y James, un pingüino, para resolver el enigma de las “piedras de luz”. Juntos viajarán a la Luna, al cinturón de asteroides y aterrizarán en el núcleo de un cometa; usando el método científico, hipótesis, observaciones y análisis encontrarán la respuesta al misterio de las estrellas fugaces. ",
  },
  {
    id: "0003",
    hour: "12:00 a.m",
    title: "Arrecifes",
    category: "Infantil",
    description:
      "Ven con tus hijos al Planetario para conocer a Lucía, una colibrí apasionada por las rocas que viajará al espacio con Vladimir, un oso polar y James, un pingüino, para resolver el enigma de las “piedras de luz”. Juntos viajarán a la Luna, al cinturón de asteroides y aterrizarán en el núcleo de un cometa; usando el método científico, hipótesis, observaciones y análisis encontrarán la respuesta al misterio de las estrellas fugaces. ",
  },
  {
    id: "0004",
    hour: "1:00 a.m",
    title: "Somos aliens",
    category: "Infantil",
    description:
      "Ven con tus hijos al Planetario para conocer a Lucía, una colibrí apasionada por las rocas que viajará al espacio con Vladimir, un oso polar y James, un pingüino, para resolver el enigma de las “piedras de luz”. Juntos viajarán a la Luna, al cinturón de asteroides y aterrizarán en el núcleo de un cometa; usando el método científico, hipótesis, observaciones y análisis encontrarán la respuesta al misterio de las estrellas fugaces. ",
  },
  {
    id: "0005",
    hour: "2:00 a.m",
    title: "Lucía, el secreto de las estrellas fugaces",
    category: "Infantil",
    description:
      "Ven con tus hijos al Planetario para conocer a Lucía, una colibrí apasionada por las rocas que viajará al espacio con Vladimir, un oso polar y James, un pingüino, para resolver el enigma de las “piedras de luz”. Juntos viajarán a la Luna, al cinturón de asteroides y aterrizarán en el núcleo de un cometa; usando el método científico, hipótesis, observaciones y análisis encontrarán la respuesta al misterio de las estrellas fugaces. ",
  },
  {
    id: "0006",
    hour: "3:00 a.m",
    title: " El Sol: nuestra estrella de vida. ",
    category: "Nuevo Show",
    description:
      "Ven con tus hijos al Planetario para conocer a Lucía, una colibrí apasionada por las rocas que viajará al espacio con Vladimir, un oso polar y James, un pingüino, para resolver el enigma de las “piedras de luz”. Juntos viajarán a la Luna, al cinturón de asteroides y aterrizarán en el núcleo de un cometa; usando el método científico, hipótesis, observaciones y análisis encontrarán la respuesta al misterio de las estrellas fugaces. ",
  },
  {
    id: "0007",
    hour: "4:00 a.m",
    title: "Somos estrellas",
    category: "Infantil",
    description:
      "Ven con tus hijos al Planetario para conocer a Lucía, una colibrí apasionada por las rocas que viajará al espacio con Vladimir, un oso polar y James, un pingüino, para resolver el enigma de las “piedras de luz”. Juntos viajarán a la Luna, al cinturón de asteroides y aterrizarán en el núcleo de un cometa; usando el método científico, hipótesis, observaciones y análisis encontrarán la respuesta al misterio de las estrellas fugaces. ",
  },
  {
    id: "0008    ",
    hour: "5:00 a.m",
    title: "Helios: relatos del vecindario solar",
    category: "Infantil",
    description:
      "Ven con tus hijos al Planetario para conocer a Lucía, una colibrí apasionada por las rocas que viajará al espacio con Vladimir, un oso polar y James, un pingüino, para resolver el enigma de las “piedras de luz”. Juntos viajarán a la Luna, al cinturón de asteroides y aterrizarán en el núcleo de un cometa; usando el método científico, hipótesis, observaciones y análisis encontrarán la respuesta al misterio de las estrellas fugaces. ",
  },
];
