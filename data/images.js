export const acuario = [{
        original: '/assets/rooms/acuario/img1.png',
        thumbnail: '/assets/rooms/acuario/img1-thumb.png',
        description: 'Red Lionfish (Pterois volitans)'
    },
    {
        original: '/assets/rooms/acuario/img2.png',
        thumbnail: '/assets/rooms/acuario/img2-thumb.png',
        description: 'Red Lionfish (Pterois volitans)'
    },
    {
        original: '/assets/rooms/acuario/img3.png',
        thumbnail: '/assets/rooms/acuario/img3-thumb.png',
        description: 'Red Lionfish (Pterois volitans)'
    },
    {
        original: '/assets/rooms/acuario/img1.png',
        thumbnail: '/assets/rooms/acuario/img1-thumb.png',
        description: 'Red Lionfish (Pterois volitans)'
    },
    {
        original: '/assets/rooms/acuario/img2.png',
        thumbnail: '/assets/rooms/acuario/img2-thumb.png',
        description: 'Red Lionfish (Pterois volitans)'
    },
    {
        original: '/assets/rooms/acuario/img3.png',
        thumbnail: '/assets/rooms/acuario/img3-thumb.png',
        description: 'Red Lionfish (Pterois volitans)'
    },

]
export const vivario = [{
        original: '/assets/rooms/vivario/img1.png',
        thumbnail: '/assets/rooms/vivario/img1-thumb.png',
        description: 'Red Lionfish (Pterois volitans)'
    },
    {
        original: '/assets/rooms/vivario/img2.png',
        thumbnail: '/assets/rooms/vivario/img2-thumb.png',
        description: 'Red Lionfish (Pterois volitans)'
    },
    {
        original: '/assets/rooms/vivario/img3.png',
        thumbnail: '/assets/rooms/vivario/img3-thumb.png',
        description: 'Red Lionfish (Pterois volitans)'
    },

]
export const smente = [{
        original: '/assets/rooms/salamente/img1.png',
        thumbnail: '/assets/rooms/salamente/img1-thumb.png',
        description: 'Red Lionfish (Pterois volitans)'
    },
    {
        original: '/assets/rooms/salamente/img2.png',
        thumbnail: '/assets/rooms/salamente/img2-thumb.png',
        description: 'Red Lionfish (Pterois volitans)'
    },
    {
        original: '/assets/rooms/salamente/img3.png',
        thumbnail: '/assets/rooms/salamente/img3-thumb.png',
        description: 'Red Lionfish (Pterois volitans)'
    },
    {
        original: '/assets/rooms/salamente/img1.png',
        thumbnail: '/assets/rooms/salamente/img1-thumb.png',
        description: 'Red Lionfish (Pterois volitans)'
    },
    {
        original: '/assets/rooms/salamente/img2.png',
        thumbnail: '/assets/rooms/salamente/img2-thumb.png',
        description: 'Red Lionfish (Pterois volitans)'
    },
    {
        original: '/assets/rooms/salamente/img3.png',
        thumbnail: '/assets/rooms/salamente/img3-thumb.png',
        description: 'Red Lionfish (Pterois volitans)'
    },

]
export const programate = [{
        original: '/assets/programming/galery/img1.png',
        thumbnail: '/assets/programming/galery/img1-thumb.png',
    },
    {
        original: '/assets/programming/galery/img2.png',
        thumbnail: '/assets/programming/galery/img2-thumb.png',
    },
    {
        original: '/assets/programming/galery/img3.png',
        thumbnail: '/assets/programming/galery/img3-thumb.png',
    },
    {
        original: '/assets/programming/galery/img1.png',
        thumbnail: '/assets/programming/galery/img1-thumb.png',
    },
    {
        original: '/assets/programming/galery/img2.png',
        thumbnail: '/assets/programming/galery/img2-thumb.png',
    },
    {
        original: '/assets/programming/galery/img3.png',
        thumbnail: '/assets/programming/galery/img3-thumb.png',
    },
]