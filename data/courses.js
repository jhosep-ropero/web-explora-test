export const allCourses = [{
        id: "0001",
        title: "Curso de investigación Espacial",
        dateCourse: 'Diciembre 27',
        dateCalendar: {
            day: 27,
            month: 12,
            year: 2022
        },
        locationCourse: "Planetario",
        audience: "Niños",
        modality: "Presencial",
        price: "$35.000",
        img: "/assets/courses/img1.png"
    },
    {
        id: "0002",
        title: "Taller de sonido y Acústica experimental",
        dateCourse: 'Noviembre 27',
        dateCalendar: {
            day: 27,
            month: 11,
            year: 2022
        },
        locationCourse: "Parque Explora",
        audience: "Jóvenes y adultos",
        modality: "Presencial",
        price: "$45.000",
        img: "/assets/courses/img2.png"
    },
    {
        id: "0003",
        title: "musical Experimental para niños",
        dateCourse: 'Enero 27',
        dateCalendar: {
            day: 27,
            month: 1,
            year: 2022
        },
        locationCourse: "Parque Explora",
        audience: "Niños",
        modality: "Presencial",
        price: "$45.000",
        img: "/assets/courses/img3.png"
    },
    {
        id: "0004",
        title: "Reconocimiento geométrico",
        dateCourse: 'Mayo 27',
        dateCalendar: {
            day: 27,
            month: 5,
            year: 2022
        },
        locationCourse: "Educación",
        audience: "Familiar",
        modality: "Online",
        price: "0",
        img: "/assets/courses/img4.png"
    },
    {
        id: "0005",
        title: "Curso de investigación Espacial",
        dateCourse: 'Enero 27',
        dateCalendar: {
            day: 27,
            month: 1,
            year: 2022
        },
        locationCourse: "Planetario",
        audience: "Niños",
        modality: "Presencial",
        price: "$35.000",
        img: "/assets/courses/img1.png"
    },
    {
        id: "0006",
        title: "Taller de sonido y Acústica experimental",
        dateCourse: 'Noviembre 27',
        dateCalendar: {
            day: 27,
            month: 11,
            year: 2022
        },
        locationCourse: "Parque Explora",
        audience: "Jóvenes y adultos",
        modality: "Presencial",
        price: "$45.000",
        img: "/assets/courses/img2.png"
    },
    {
        id: "0007",
        title: "musical Experimental para niños",
        dateCourse: 'Febrero 27',
        dateCalendar: {
            day: 27,
            month: 2,
            year: 2022
        },
        locationCourse: "Parque Explora",
        audience: "Niños",
        modality: "Presencial",
        price: "$45.000",
        img: "/assets/courses/img3.png"
    },
    {
        id: "0008",
        title: "Reconocimiento geométrico",
        dateCourse: 'Mayo 27',
        dateCalendar: {
            day: 27,
            month: 5,
            year: 2022
        },
        locationCourse: "Educación",
        audience: "Familia",
        modality: "Online",
        price: "0",
        img: "/assets/courses/img4.png"
    },
    {
        id: "0009",
        title: "Curso de investigación Espacial",
        dateCourse: 'Diciembre 27',
        dateCalendar: {
            day: 27,
            month: 12,
            year: 2022
        },
        locationCourse: "Planetario",
        audience: "Niños",
        modality: "Presencial",
        price: "$35.000",
        img: "/assets/courses/img1.png"
    },
    {
        id: "0010",
        title: "Taller de sonido y Acústica experimental",
        dateCourse: 'Febrero 27',
        dateCalendar: {
            day: 27,
            month: 2,
            year: 2022
        },
        locationCourse: "Parque Explora",
        audience: "Jóvenes y adultos",
        modality: "Presencial",
        price: "$45.000",
        img: "/assets/courses/img2.png"
    },
    {
        id: "0011",
        title: "musical Experimental para niños",
        dateCourse: 'Mayo 27',
        dateCalendar: {
            day: 27,
            month: 5,
            year: 2022
        },
        locationCourse: "Parque Explora",
        audience: "Niños",
        modality: "Presencial",
        price: "$45.000",
        img: "/assets/courses/img3.png"
    },
    {
        id: "0012",
        title: "Reconocimiento geométrico",
        dateCourse: 'Diciembre 27',
        dateCalendar: {
            day: 27,
            month: 12,
            year: 2022
        },
        locationCourse: "Educación",
        audience: "Familiar",
        modality: "Online",
        price: "0",
        img: "/assets/courses/img4.png"
    },
    {
      id: "0013",
      title: "Los colores del sol",
      dateCourse: "Enero 22",
      dateCalendar: {
        day: 27,
        month: 12,
        year: 2022
      },
      hourCourse: "Hora: 8:00 a 10 am",
      locationCourse: "Educación",
      audience: "Jóvenes a partir de 16 años, y adultos",
      modality: "Presencial",
      price: "$55.000",
      img: "/assets/courses/poster1.svg",
      shortDescription:
        "En este taller recorreremos la historia que los humanos han contado sobre el Sol. Tendremos una charla, un taller práctico, un show en el domo y observación solar.",
      largeDescriptionFirstPart:
        "El Sol es la única estrella que tenemos lo suficientemente cerca como para recibir su calor, ver su superficie y entender por qué las demás estrellas brillan. En este taller de tres horas dedicaremos nuestro tiempo a entenderlo, desde las historias que contaban nuestros antepasados, quienes lo admiraban como una deidad, hasta la visión moderna que ha sido construida con la astrofísica.",
      largeDescriptionSecondPart:
        "Comenzaremos con la charla: “Viviendo con una estrella”, donde hablaremos de las principales características del Sol y resolveremos dudas. Luego construiremos un espectroscopio, un artefacto que nos permite descomponer la luz para entender de dónde proviene. Además disfrutaremos de un viaje en el espacio y el tiempo desde el domo planetario con el show “El Sol, nuestra estrella de vida”. Por último tendremos una sesión de observación solar desde nuestras terrazas con instrumentos especializados.",
      requirements:
        "Este taller no requiere conocimientos previos, deberás desplazarte a El Planetario en Medellín.",
      quota: "20",
      teacher: "Mauricio Arango",
    },

]
