export const programs = [
  {
    id: "0001",
    img: "/assets/planetary/program/programfeatured1.png",
    type: "Planetario",
    categories: ["Jóvenes y adultos", "Presencial"],
    date: ["05", "Ago"],
    hours: [
      {
        start: "8:00 am",
        end: "10:00 am",
      },
    ],
    title: "¡Seamos protoestrellas! Convocatoria para jóvenes",
    price: "35000",
  },
  {
    id: "0002",
    img: "/assets/planetary/program/programfeatured2.png",
    type: "Planetario",
    categories: ["Jóvenes y adultos", "Presencial"],
    date: ["05", "Ago"],
    hours: [
      {
        start: "8:00 am",
        end: "10:00 am",
      },
    ],
    title: "Coloquio de Astronomía: los colores del cometa Leonard",
    price: "35000",
  },
  {
    id: "0003",
    img: "/assets/planetary/program/programfeatured3.png",
    type: "Planetario",
    categories: ["Jóvenes y adultos", "Presencial"],
    date: ["05", "Ago"],
    hours: [
      {
        start: "8:00 am",
        end: "10:00 am",
      },
    ],
    title: "El cielo esta noche: las orejas de la Luna",
    price: "35000",
  },
  {
    id: "0004",
    img: "/assets/planetary/program/programfeatured4.png",
    type: "Planetario",
    categories: ["Jóvenes y adultos", "Presencial"],
    date: ["05", "Ago"],
    hours: [
      {
        start: "8:00 am",
        end: "10:00 am",
      },
    ],
    title: "Aguapanela Arví y otros arácnidos",
    price: "35000",
  },
  {
    id: "0005",
    img: "/assets/planetary/program/programfeatured1.png",
    type: "Planetario",
    categories: ["Jóvenes y adultos", "Presencial"],
    date: ["05", "Ago"],
    hours: [
      {
        start: "8:00 am",
        end: "10:00 am",
      },
    ],
    title: "¡Seamos protoestrellas! Convocatoria para jóvenes",
    price: "35000",
  },
  {
    id: "0006",
    img: "/assets/planetary/program/programfeatured2.png",
    type: "Planetario",
    categories: ["Jóvenes y adultos", "Presencial"],
    date: ["05", "Ago"],
    hours: [
      {
        start: "8:00 am",
        end: "10:00 am",
      },
    ],
    title: "¡Seamos protoestrellas! Convocatoria para jóvenes",
    price: "35000",
  },
  {
    id: "0007",
    img: "/assets/planetary/program/programfeatured3.png",
    type: "Planetario",
    categories: ["Jóvenes y adultos", "Presencial"],
    date: ["05", "Ago"],
    hours: [
      {
        start: "8:00 am",
        end: "10:00 am",
      },
    ],
    title: "¡Seamos protoestrellas! Convocatoria para jóvenes",
    price: "35000",
  },
  {
    id: "0008",
    img: "/assets/planetary/program/programfeatured4.png",
    type: "Planetario",
    categories: ["Jóvenes y adultos", "Presencial"],
    date: ["05", "Ago"],
    hours: [
      {
        start: "8:00 am",
        end: "10:00 am",
      },
    ],
    title: "¡Seamos protoestrellas! Convocatoria para jóvenes",
    price: "35000",
  },
];
