export const validationSchema = (values) => {
  let errors = {};

  if (!values.name) {
    errors.name = "Este campo es requerido";
  } else if (!/^[a-zA-ZÀ-ÿ\s]{1,40}$/.test(values.name)) {
    errors.name = "El nombre es Invalido";
  }

  if (!values.lastName) {
    errors.lastName = "Este campo es requerido";
  } else if (!/^[a-zA-ZÀ-ÿ\s]{1,40}$/.test(values.lastName)) {
    errors.lastName = "El apellido es Invalido";
  }

  if (!values.phoneNumber) {
    errors.phoneNumber = "Este campo es requerido";
  } else if (!/^[0-9]*$/.test(values.phoneNumber)) {
    errors.phoneNumber = "El numero es Invalido";
  }

  if (!values.email) {
    errors.email = "Este campo es requerido";
  } else if (
    !/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/.test(values.email)
  ) {
    errors.email = "El correo electrónico no es valido";
  }

  if (!values.identification) {
    errors.identification = "Este campo es requerido";
  } else if (!/^[0-9]*$/.test(values.identification)) {
    errors.identification = "El número de identificacion no es valido";
  }

  if (!values.location) {
    errors.location = "Este campo es requerido";
  }

  if (!values.country) {
    errors.country = "Este campo es requerido";
  }
  if (!values.province) {
    errors.province = "Este campo es requerido";
  }
  if (!values.firstPick) {
    errors.firstPick = "Este campo es requerido";
  }
  if (!values.secondPick) {
    errors.firstPick = "Este campo es requerido";
  }

  return errors;
};
