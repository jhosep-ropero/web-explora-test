export const contractUs= [
    {
        imgBackground: "url('/assets/acuario_explora.jpg')",
        title: "Rutas Pedagógicas",
        description: "Haz del Parque Explora el laboratorio de tu colegio",
        url: "/",
    },
    {
        imgBackground: "url('/assets/slider-ciencia.png')",
        title: "Ciencia en Bicicleta",
        description: "La república plural de las preguntas",
        url: "/",
    },
    {
        imgBackground: "url('/assets/domo.jpg')",
        title: "Espacios para Eventos",
        description: "Un teatro, dos auditorios, un acuario y un domo",
        url: "/",
    }
]