export const storePlanetary= [
    {
        id:"0001",
        title: "OurShopPlanetary",
        size: ['M','L','XL'],
        color: ['#FFFFFF', '#FFD524'],
        price: "$85.000",
        img: "store/img-01"
    },
    {
        id:"0002",
        title: "Juego Super Nova",
        price: "$85.000",
        img: "store/img-02"
    },
    {
        id:"0003",
        title: "Sistema Solar",
        color: ['#FFFFFF', '#FFD524'],
        price: "$85.000",
        img: "store/img-03"
    },
    {
        id:"0004",
        title: "Libreta Luna",
        color: ['#FFFFFF', '#FFD524'],
        price: "$85.000",
        img: "store/img-04"
    },
]