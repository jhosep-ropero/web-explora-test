export const programate = [{
        id: "0001",
        title: "Taller en Moravia: la Física de un soplido",
        date: {
            day: '15',
            month: 'SEP',
            monthExtend: 'Septiembre',
            year: '2022'
        },
        dateCalendar: {
            day: 15,
            month: 9,
            year: 2022
        },
        time: "8:00 a 10:00 a.m",
        dateExtend: "01 Septiempre - 8:00 a 10:00 a.m",
        category: "Aprende",
        location: "Parque Explora",
        audience: "Jóvenes y adultos",
        modality: "Presencial",
        price: "$35.000",
        img: "events/img1",
        shortDescription: "En este taller presencial de ciencia en familia conoceremos, con juegos y experimentos, el principio de Bernoulli, una muestra de que la Física tiene que ver con la vida. e una manguera, dejando un pequeño orificio, el chorro de agua aumenta su velocidad y llega más lejos.",
        descriptionModality: "Taller presencial y en vivo",
        descriptionExtend: {
            title: "aves, anfibios, reptiles y plantas",
            shortDescription: "Como dos manos que sostienen un hilo de agua, el encuentro entre las cordilleras  Central  y Occidental crea dos valles interandinos y en uno de ellos florece una de las regiones más biodiversas de Colombia, el cañón del río Cauca.",
            longDescription: [
                "A lado y lado del río, se levanta un espeso bosque seco tropical con más de 600 especies de plantas pertenecientes a 99 familias, como las gigantes ceibas o los árboles “fruto de cerebro” una especie única para la ciencia.",
                "Allí habita una de las serpientes más pequeñas del mundo que enroscada apenas supera el tamaño de una moneda, ranas arbóreas que solo están presentes en este lugar del planeta y una enorme variedad de insectos. La variedad climática que se da en el cañón, que abarca 80 kilómetros desde la zona semidesértica próxima a Santa Fe de Antioquia hasta las zonas lluviosas en Valdivia, permite que cerca de 290 especies de aves, como  el pájaro hormiguero pico de hacha o las guacamayas verdes, se alberguen allí.",
                "Te invitamos a empezar la noche con una expedición por la diversidad de la vida en la cuenca de este río, el jueves 4 de noviembre, a las 7:00 p.m. por YouTube y Facebook en un nuevo episodio de Ciencia en bicicleta."
            ],
            participants: [
                "Héctor Fabio Rivera Gutiérrez, Ph. D. en Biología, Universidad de Amberes, M. Sc. en Ecología Vrije Universiteit Brussels. coordinador del Grupo Ecología y Evolución de Vertebrados de la UdeA.  Investigador de las aves que habitan la zona del proyecto hidroeléctrico Ituango, con EPM  y docente de ornitología y bioacústica. Ganador del premio a la Extensión 2021 de la UdeA.",
                "Esteban Alzate Basto, biólogo herpetólogo, docente investigador y curador de la colección de herpetología de la Colecciones Biológicas de la Universidad CES.",
                " Álvaro Vásquez, ingeniero forestal, magíster en Bosques y Conservación Ambiental. Experto en áreas de restauración y conservación de bosque seco tropical en Colombia",
            ],
            moderators: [
                "Carolina Sanín, ecóloga, coordinadora de proyectos Parque Explora."
            ]
        },
        capacity: '15',
        keywords: "física, taller, moravia, ciencia, explora, aprende, explora"
    },
    {
        id: "0002",
        title: "Aguapanela arvi y otros arácnidos",
        date: {
            day: '15',
            month: 'SEP',
            monthExtend: 'Septiembre',
            year: '2022'
        },
        dateCalendar: {
            day: 15,
            month: 9,
            year: 2022
        },
        time: "8:00 a 10:00 a.m",
        dateExtend: "02 Septiembre - 8:00 a 10:00 a.m",
        category: "Aprende",
        location: "Parque Explora",
        audience: "Familiar",
        modality: "Presencial",
        price: "$35.000",
        img: "events/img2",
        shortDescription: "Como dos manos que sostienen un hilo de agua, el encuentro entre las cordilleras  Central  y Occidental crea dos valles interandinos y en uno de ellos florece una de las regiones más biodiversas de Colombia, el cañón del río Cauca.",
        descriptionModality: "Taller presencial y en vivo",
        descriptionExtend: {
            title: "aves, anfibios, reptiles y plantas",
            shortDescription: "Como dos manos que sostienen un hilo de agua, el encuentro entre las cordilleras  Central  y Occidental crea dos valles interandinos y en uno de ellos florece una de las regiones más biodiversas de Colombia, el cañón del río Cauca.",
            longDescription: [
                "A lado y lado del río, se levanta un espeso bosque seco tropical con más de 600 especies de plantas pertenecientes a 99 familias, como las gigantes ceibas o los árboles “fruto de cerebro” una especie única para la ciencia.",
                "Allí habita una de las serpientes más pequeñas del mundo que enroscada apenas supera el tamaño de una moneda, ranas arbóreas que solo están presentes en este lugar del planeta y una enorme variedad de insectos. La variedad climática que se da en el cañón, que abarca 80 kilómetros desde la zona semidesértica próxima a Santa Fe de Antioquia hasta las zonas lluviosas en Valdivia, permite que cerca de 290 especies de aves, como  el pájaro hormiguero pico de hacha o las guacamayas verdes, se alberguen allí.",
                "Te invitamos a empezar la noche con una expedición por la diversidad de la vida en la cuenca de este río, el jueves 4 de noviembre, a las 7:00 p.m. por YouTube y Facebook en un nuevo episodio de Ciencia en bicicleta."
            ],
            participants: [
                "Héctor Fabio Rivera Gutiérrez, Ph. D. en Biología, Universidad de Amberes, M. Sc. en Ecología Vrije Universiteit Brussels. coordinador del Grupo Ecología y Evolución de Vertebrados de la UdeA.  Investigador de las aves que habitan la zona del proyecto hidroeléctrico Ituango, con EPM  y docente de ornitología y bioacústica. Ganador del premio a la Extensión 2021 de la UdeA.",
                "Esteban Alzate Basto, biólogo herpetólogo, docente investigador y curador de la colección de herpetología de la Colecciones Biológicas de la Universidad CES.",
                " Álvaro Vásquez, ingeniero forestal, magíster en Bosques y Conservación Ambiental. Experto en áreas de restauración y conservación de bosque seco tropical en Colombia",
            ],
            moderators: [
                "Carolina Sanín, ecóloga, coordinadora de proyectos Parque Explora."
            ]
        },
        capacity: '15',
        keywords: "insectos, arvi, aguapanela, arácnidos, aprende, explora, familia"
    },
    {
        id: "0003",
        title: "Accesibilidad: ciencia con todos los sentidos",
        date: {
            day: '14',
            month: 'SEP',
            monthExtend: 'Septiembre',
            year: '2022'
        },
        dateCalendar: {
            day: 14,
            month: 9,
            year: 2022
        },
        time: "8:00 a 10:00 a.m",
        dateExtend: "03 Septiembre - 8:00 a 10:00 a.m",
        category: "Aprende",
        location: "Planetario",
        audience: "Niños",
        modality: "Presencial",
        price: "0",
        free: 'Gratis',
        img: "events/img3",
        shortDescription: "En este taller presencial de ciencia en familia conoceremos, con juegos y experimentos, el principio de Bernoulli, una muestra de que la Física tiene que ver con la vida. e una manguera, dejando un pequeño orificio, el chorro de agua aumenta su velocidad y llega más lejos.",
        descriptionModality: "Taller presencial y en vivo",
        descriptionExtend: {
            title: "aves, anfibios, reptiles y plantas",
            shortDescription: "Como dos manos que sostienen un hilo de agua, el encuentro entre las cordilleras  Central  y Occidental crea dos valles interandinos y en uno de ellos florece una de las regiones más biodiversas de Colombia, el cañón del río Cauca.",
            longDescription: [
                "A lado y lado del río, se levanta un espeso bosque seco tropical con más de 600 especies de plantas pertenecientes a 99 familias, como las gigantes ceibas o los árboles “fruto de cerebro” una especie única para la ciencia.",
                "Allí habita una de las serpientes más pequeñas del mundo que enroscada apenas supera el tamaño de una moneda, ranas arbóreas que solo están presentes en este lugar del planeta y una enorme variedad de insectos. La variedad climática que se da en el cañón, que abarca 80 kilómetros desde la zona semidesértica próxima a Santa Fe de Antioquia hasta las zonas lluviosas en Valdivia, permite que cerca de 290 especies de aves, como  el pájaro hormiguero pico de hacha o las guacamayas verdes, se alberguen allí.",
                "Te invitamos a empezar la noche con una expedición por la diversidad de la vida en la cuenca de este río, el jueves 4 de noviembre, a las 7:00 p.m. por YouTube y Facebook en un nuevo episodio de Ciencia en bicicleta."
            ],
            participants: [
                "Héctor Fabio Rivera Gutiérrez, Ph. D. en Biología, Universidad de Amberes, M. Sc. en Ecología Vrije Universiteit Brussels. coordinador del Grupo Ecología y Evolución de Vertebrados de la UdeA.  Investigador de las aves que habitan la zona del proyecto hidroeléctrico Ituango, con EPM  y docente de ornitología y bioacústica. Ganador del premio a la Extensión 2021 de la UdeA.",
                "Esteban Alzate Basto, biólogo herpetólogo, docente investigador y curador de la colección de herpetología de la Colecciones Biológicas de la Universidad CES.",
                " Álvaro Vásquez, ingeniero forestal, magíster en Bosques y Conservación Ambiental. Experto en áreas de restauración y conservación de bosque seco tropical en Colombia",
            ],
            moderators: [
                "Carolina Sanín, ecóloga, coordinadora de proyectos Parque Explora."
            ]
        },
        capacity: '15',
        keywords: "accesibilidad, ciencia, planetario, aprende, explora, niños"
    },
    {
        id: "0004",
        title: "Un videojuego educativo con Scractch",
        date: {
            day: '14',
            month: 'SEP',
            monthExtend: 'Septiembre',
            year: '2022'
        },
        dateCalendar: {
            day: 14,
            month: 9,
            year: 2022
        },
        time: "8:00 a 10:00 a.m",
        dateExtend: "04 Septiembre - 8:00 a 10:00 a.m",
        category: "Aprende",
        location: "Exploratorio",
        audience: "Maestros",
        modality: "Online",
        price: "0",
        free: 'Gratis',
        img: "events/img4",
        shortDescription: "En este taller presencial de ciencia en familia conoceremos, con juegos y experimentos, el principio de Bernoulli, una muestra de que la Física tiene que ver con la vida. e una manguera, dejando un pequeño orificio, el chorro de agua aumenta su velocidad y llega más lejos.",
        descriptionModality: "Taller presencial y en vivo",
        descriptionExtend: {
            title: "aves, anfibios, reptiles y plantas",
            shortDescription: "Como dos manos que sostienen un hilo de agua, el encuentro entre las cordilleras  Central  y Occidental crea dos valles interandinos y en uno de ellos florece una de las regiones más biodiversas de Colombia, el cañón del río Cauca.",
            longDescription: [
                "A lado y lado del río, se levanta un espeso bosque seco tropical con más de 600 especies de plantas pertenecientes a 99 familias, como las gigantes ceibas o los árboles “fruto de cerebro” una especie única para la ciencia.",
                "Allí habita una de las serpientes más pequeñas del mundo que enroscada apenas supera el tamaño de una moneda, ranas arbóreas que solo están presentes en este lugar del planeta y una enorme variedad de insectos. La variedad climática que se da en el cañón, que abarca 80 kilómetros desde la zona semidesértica próxima a Santa Fe de Antioquia hasta las zonas lluviosas en Valdivia, permite que cerca de 290 especies de aves, como  el pájaro hormiguero pico de hacha o las guacamayas verdes, se alberguen allí.",
                "Te invitamos a empezar la noche con una expedición por la diversidad de la vida en la cuenca de este río, el jueves 4 de noviembre, a las 7:00 p.m. por YouTube y Facebook en un nuevo episodio de Ciencia en bicicleta."
            ],
            participants: [
                "Héctor Fabio Rivera Gutiérrez, Ph. D. en Biología, Universidad de Amberes, M. Sc. en Ecología Vrije Universiteit Brussels. coordinador del Grupo Ecología y Evolución de Vertebrados de la UdeA.  Investigador de las aves que habitan la zona del proyecto hidroeléctrico Ituango, con EPM  y docente de ornitología y bioacústica. Ganador del premio a la Extensión 2021 de la UdeA.",
                "Esteban Alzate Basto, biólogo herpetólogo, docente investigador y curador de la colección de herpetología de la Colecciones Biológicas de la Universidad CES.",
                " Álvaro Vásquez, ingeniero forestal, magíster en Bosques y Conservación Ambiental. Experto en áreas de restauración y conservación de bosque seco tropical en Colombia",
            ],
            moderators: [
                "Carolina Sanín, ecóloga, coordinadora de proyectos Parque Explora."
            ]
        },
        capacity: '15',
        keywords: "videojuegos, educativo, scractch, aprende, explora, maestros, exploratorio"
    },
    {
        id: "0005",
        title: "Aguapanela arvi y otros arácnidos",
        date: {
            day: '15',
            month: 'AGO',
            monthExtend: 'Agosto',
            year: '2022'
        },
        dateCalendar: {
            day: 15,
            month: 8,
            year: 2022
        },
        time: "8:00 a 10:00 a.m",
        dateExtend: "05 Agosto - 8:00 a 10:00 a.m",
        category: "Aprende",
        location: "Parque Explora",
        audience: "Familiar",
        modality: "Presencial",
        price: "$35.000",
        img: "events/img2",
        shortDescription: "Como dos manos que sostienen un hilo de agua, el encuentro entre las cordilleras  Central  y Occidental crea dos valles interandinos y en uno de ellos florece una de las regiones más biodiversas de Colombia, el cañón del río Cauca.",
        descriptionModality: "Taller presencial y en vivo",
        descriptionExtend: {
            title: "aves, anfibios, reptiles y plantas",
            shortDescription: "Como dos manos que sostienen un hilo de agua, el encuentro entre las cordilleras  Central  y Occidental crea dos valles interandinos y en uno de ellos florece una de las regiones más biodiversas de Colombia, el cañón del río Cauca.",
            longDescription: [
                "A lado y lado del río, se levanta un espeso bosque seco tropical con más de 600 especies de plantas pertenecientes a 99 familias, como las gigantes ceibas o los árboles “fruto de cerebro” una especie única para la ciencia.",
                "Allí habita una de las serpientes más pequeñas del mundo que enroscada apenas supera el tamaño de una moneda, ranas arbóreas que solo están presentes en este lugar del planeta y una enorme variedad de insectos. La variedad climática que se da en el cañón, que abarca 80 kilómetros desde la zona semidesértica próxima a Santa Fe de Antioquia hasta las zonas lluviosas en Valdivia, permite que cerca de 290 especies de aves, como  el pájaro hormiguero pico de hacha o las guacamayas verdes, se alberguen allí.",
                "Te invitamos a empezar la noche con una expedición por la diversidad de la vida en la cuenca de este río, el jueves 4 de noviembre, a las 7:00 p.m. por YouTube y Facebook en un nuevo episodio de Ciencia en bicicleta."
            ],
            participants: [
                "Héctor Fabio Rivera Gutiérrez, Ph. D. en Biología, Universidad de Amberes, M. Sc. en Ecología Vrije Universiteit Brussels. coordinador del Grupo Ecología y Evolución de Vertebrados de la UdeA.  Investigador de las aves que habitan la zona del proyecto hidroeléctrico Ituango, con EPM  y docente de ornitología y bioacústica. Ganador del premio a la Extensión 2021 de la UdeA.",
                "Esteban Alzate Basto, biólogo herpetólogo, docente investigador y curador de la colección de herpetología de la Colecciones Biológicas de la Universidad CES.",
                " Álvaro Vásquez, ingeniero forestal, magíster en Bosques y Conservación Ambiental. Experto en áreas de restauración y conservación de bosque seco tropical en Colombia",
            ],
            moderators: [
                "Carolina Sanín, ecóloga, coordinadora de proyectos Parque Explora."
            ]
        },
        capacity: '15',
        keywords: "insectos, arvi, aguapanela, arácnidos, aprende, explora, familia"
    },
    {
        id: "0006",
        title: "Taller en Moravia: la Física de un soplido",
        date: {
            day: '01',
            month: 'AGO',
            monthExtend: 'Agosto',
            year: '2022'
        },
        dateCalendar: {
            day: 1,
            month: 8,
            year: 2022
        },
        time: "8:00 a 10:00 a.m",
        dateExtend: "01 Agosto - 8:00 a 10:00 a.m",
        category: "Aprende",
        location: "Parque Explora",
        audience: "Jóvenes y adultos",
        modality: "Presencial",
        price: "$35.000",
        img: "events/img1",
        shortDescription: "Como dos manos que sostienen un hilo de agua, el encuentro entre las cordilleras  Central  y Occidental crea dos valles interandinos y en uno de ellos florece una de las regiones más biodiversas de Colombia, el cañón del río Cauca.",
        descriptionModality: "Taller presencial y en vivo",
        descriptionExtend: {
            title: "aves, anfibios, reptiles y plantas",
            shortDescription: "Como dos manos que sostienen un hilo de agua, el encuentro entre las cordilleras  Central  y Occidental crea dos valles interandinos y en uno de ellos florece una de las regiones más biodiversas de Colombia, el cañón del río Cauca.",
            longDescription: [
                "A lado y lado del río, se levanta un espeso bosque seco tropical con más de 600 especies de plantas pertenecientes a 99 familias, como las gigantes ceibas o los árboles “fruto de cerebro” una especie única para la ciencia.",
                "Allí habita una de las serpientes más pequeñas del mundo que enroscada apenas supera el tamaño de una moneda, ranas arbóreas que solo están presentes en este lugar del planeta y una enorme variedad de insectos. La variedad climática que se da en el cañón, que abarca 80 kilómetros desde la zona semidesértica próxima a Santa Fe de Antioquia hasta las zonas lluviosas en Valdivia, permite que cerca de 290 especies de aves, como  el pájaro hormiguero pico de hacha o las guacamayas verdes, se alberguen allí.",
                "Te invitamos a empezar la noche con una expedición por la diversidad de la vida en la cuenca de este río, el jueves 4 de noviembre, a las 7:00 p.m. por YouTube y Facebook en un nuevo episodio de Ciencia en bicicleta."
            ],
            participants: [
                "Héctor Fabio Rivera Gutiérrez, Ph. D. en Biología, Universidad de Amberes, M. Sc. en Ecología Vrije Universiteit Brussels. coordinador del Grupo Ecología y Evolución de Vertebrados de la UdeA.  Investigador de las aves que habitan la zona del proyecto hidroeléctrico Ituango, con EPM  y docente de ornitología y bioacústica. Ganador del premio a la Extensión 2021 de la UdeA.",
                "Esteban Alzate Basto, biólogo herpetólogo, docente investigador y curador de la colección de herpetología de la Colecciones Biológicas de la Universidad CES.",
                " Álvaro Vásquez, ingeniero forestal, magíster en Bosques y Conservación Ambiental. Experto en áreas de restauración y conservación de bosque seco tropical en Colombia",
            ],
            moderators: [
                "Carolina Sanín, ecóloga, coordinadora de proyectos Parque Explora."
            ]
        },
        capacity: '15',
        keywords: "física, taller, moravia, ciencia, aprende, explora"
    },
    {
        id: "0007",
        title: "Aguapanela arvi y otros arácnidos",
        date: {
            day: '02',
            month: 'AGO',
            monthExtend: 'Agosto',
            year: '2022'
        },
        dateCalendar: {
            day: 2,
            month: 8,
            year: 2022
        },
        time: "8:00 a 10:00 a.m",
        dateExtend: "02 Agosto - 8:00 a 10:00 a.m",
        category: "Aprende",
        location: "Parque Explora",
        audience: "Familiar",
        modality: "Presencial",
        price: "$35.000",
        img: "events/img2",
        shortDescription: "Como dos manos que sostienen un hilo de agua, el encuentro entre las cordilleras  Central  y Occidental crea dos valles interandinos y en uno de ellos florece una de las regiones más biodiversas de Colombia, el cañón del río Cauca.",
        descriptionModality: "Taller presencial y en vivo",
        descriptionExtend: {
            title: "aves, anfibios, reptiles y plantas",
            shortDescription: "Como dos manos que sostienen un hilo de agua, el encuentro entre las cordilleras  Central  y Occidental crea dos valles interandinos y en uno de ellos florece una de las regiones más biodiversas de Colombia, el cañón del río Cauca.",
            longDescription: [
                "A lado y lado del río, se levanta un espeso bosque seco tropical con más de 600 especies de plantas pertenecientes a 99 familias, como las gigantes ceibas o los árboles “fruto de cerebro” una especie única para la ciencia.",
                "Allí habita una de las serpientes más pequeñas del mundo que enroscada apenas supera el tamaño de una moneda, ranas arbóreas que solo están presentes en este lugar del planeta y una enorme variedad de insectos. La variedad climática que se da en el cañón, que abarca 80 kilómetros desde la zona semidesértica próxima a Santa Fe de Antioquia hasta las zonas lluviosas en Valdivia, permite que cerca de 290 especies de aves, como  el pájaro hormiguero pico de hacha o las guacamayas verdes, se alberguen allí.",
                "Te invitamos a empezar la noche con una expedición por la diversidad de la vida en la cuenca de este río, el jueves 4 de noviembre, a las 7:00 p.m. por YouTube y Facebook en un nuevo episodio de Ciencia en bicicleta."
            ],
            participants: [
                "Héctor Fabio Rivera Gutiérrez, Ph. D. en Biología, Universidad de Amberes, M. Sc. en Ecología Vrije Universiteit Brussels. coordinador del Grupo Ecología y Evolución de Vertebrados de la UdeA.  Investigador de las aves que habitan la zona del proyecto hidroeléctrico Ituango, con EPM  y docente de ornitología y bioacústica. Ganador del premio a la Extensión 2021 de la UdeA.",
                "Esteban Alzate Basto, biólogo herpetólogo, docente investigador y curador de la colección de herpetología de la Colecciones Biológicas de la Universidad CES.",
                " Álvaro Vásquez, ingeniero forestal, magíster en Bosques y Conservación Ambiental. Experto en áreas de restauración y conservación de bosque seco tropical en Colombia",
            ],
            moderators: [
                "Carolina Sanín, ecóloga, coordinadora de proyectos Parque Explora."
            ]
        },
        capacity: '15',
        keywords: "insectos, arvi, aguapanela, arácnidos, aprende, explora, familia"
    },
    {
        id: "0008",
        title: "Accesibilidad: ciencia con todos los sentidos",
        date: {
            day: '03',
            month: 'AGO',
            monthExtend: 'Agosto',
            year: '2022'
        },
        dateCalendar: {
            day: 3,
            month: 8,
            year: 2022
        },
        time: "8:00 a 10:00 a.m",
        dateExtend: "03 Agosto - 8:00 a 10:00 a.m",
        category: "Aprende",
        location: "Planetario",
        audience: "Niños",
        modality: "Presencial",
        price: "0",
        free: 'Gratis',
        img: "events/img3",
        shortDescription: "Como dos manos que sostienen un hilo de agua, el encuentro entre las cordilleras  Central  y Occidental crea dos valles interandinos y en uno de ellos florece una de las regiones más biodiversas de Colombia, el cañón del río Cauca.",
        descriptionModality: "Taller presencial y en vivo",
        descriptionExtend: {
            title: "aves, anfibios, reptiles y plantas",
            shortDescription: "Como dos manos que sostienen un hilo de agua, el encuentro entre las cordilleras  Central  y Occidental crea dos valles interandinos y en uno de ellos florece una de las regiones más biodiversas de Colombia, el cañón del río Cauca.",
            longDescription: [
                "A lado y lado del río, se levanta un espeso bosque seco tropical con más de 600 especies de plantas pertenecientes a 99 familias, como las gigantes ceibas o los árboles “fruto de cerebro” una especie única para la ciencia.",
                "Allí habita una de las serpientes más pequeñas del mundo que enroscada apenas supera el tamaño de una moneda, ranas arbóreas que solo están presentes en este lugar del planeta y una enorme variedad de insectos. La variedad climática que se da en el cañón, que abarca 80 kilómetros desde la zona semidesértica próxima a Santa Fe de Antioquia hasta las zonas lluviosas en Valdivia, permite que cerca de 290 especies de aves, como  el pájaro hormiguero pico de hacha o las guacamayas verdes, se alberguen allí.",
                "Te invitamos a empezar la noche con una expedición por la diversidad de la vida en la cuenca de este río, el jueves 4 de noviembre, a las 7:00 p.m. por YouTube y Facebook en un nuevo episodio de Ciencia en bicicleta."
            ],
            participants: [
                "Héctor Fabio Rivera Gutiérrez, Ph. D. en Biología, Universidad de Amberes, M. Sc. en Ecología Vrije Universiteit Brussels. coordinador del Grupo Ecología y Evolución de Vertebrados de la UdeA.  Investigador de las aves que habitan la zona del proyecto hidroeléctrico Ituango, con EPM  y docente de ornitología y bioacústica. Ganador del premio a la Extensión 2021 de la UdeA.",
                "Esteban Alzate Basto, biólogo herpetólogo, docente investigador y curador de la colección de herpetología de la Colecciones Biológicas de la Universidad CES.",
                " Álvaro Vásquez, ingeniero forestal, magíster en Bosques y Conservación Ambiental. Experto en áreas de restauración y conservación de bosque seco tropical en Colombia",
            ],
            moderators: [
                "Carolina Sanín, ecóloga, coordinadora de proyectos Parque Explora."
            ]
        },
        capacity: '15',
        keywords: "accesibilidad, ciencia, planetario, aprende, explora, niños"
    },
    {
        id: "0009",
        title: "Un videojuego educativo con Scractch",
        date: {
            day: '04',
            month: 'AGO',
            monthExtend: 'Agosto',
            year: '2022'
        },
        dateCalendar: {
            day: 4,
            month: 8,
            year: 2022
        },
        time: "8:00 a 10:00 a.m",
        dateExtend: "04 Agosto - 8:00 a 10:00 a.m",
        category: "Aprende",
        location: "Exploratorio",
        audience: "Maestros",
        modality: "Online",
        price: "0",
        free: 'Gratis',
        img: "events/img4",
        shortDescription: "Como dos manos que sostienen un hilo de agua, el encuentro entre las cordilleras  Central  y Occidental crea dos valles interandinos y en uno de ellos florece una de las regiones más biodiversas de Colombia, el cañón del río Cauca.",
        descriptionModality: "Taller presencial y en vivo",
        descriptionExtend: {
            title: "aves, anfibios, reptiles y plantas",
            shortDescription: "Como dos manos que sostienen un hilo de agua, el encuentro entre las cordilleras  Central  y Occidental crea dos valles interandinos y en uno de ellos florece una de las regiones más biodiversas de Colombia, el cañón del río Cauca.",
            longDescription: [
                "A lado y lado del río, se levanta un espeso bosque seco tropical con más de 600 especies de plantas pertenecientes a 99 familias, como las gigantes ceibas o los árboles “fruto de cerebro” una especie única para la ciencia.",
                "Allí habita una de las serpientes más pequeñas del mundo que enroscada apenas supera el tamaño de una moneda, ranas arbóreas que solo están presentes en este lugar del planeta y una enorme variedad de insectos. La variedad climática que se da en el cañón, que abarca 80 kilómetros desde la zona semidesértica próxima a Santa Fe de Antioquia hasta las zonas lluviosas en Valdivia, permite que cerca de 290 especies de aves, como  el pájaro hormiguero pico de hacha o las guacamayas verdes, se alberguen allí.",
                "Te invitamos a empezar la noche con una expedición por la diversidad de la vida en la cuenca de este río, el jueves 4 de noviembre, a las 7:00 p.m. por YouTube y Facebook en un nuevo episodio de Ciencia en bicicleta."
            ],
            participants: [
                "Héctor Fabio Rivera Gutiérrez, Ph. D. en Biología, Universidad de Amberes, M. Sc. en Ecología Vrije Universiteit Brussels. coordinador del Grupo Ecología y Evolución de Vertebrados de la UdeA.  Investigador de las aves que habitan la zona del proyecto hidroeléctrico Ituango, con EPM  y docente de ornitología y bioacústica. Ganador del premio a la Extensión 2021 de la UdeA.",
                "Esteban Alzate Basto, biólogo herpetólogo, docente investigador y curador de la colección de herpetología de la Colecciones Biológicas de la Universidad CES.",
                " Álvaro Vásquez, ingeniero forestal, magíster en Bosques y Conservación Ambiental. Experto en áreas de restauración y conservación de bosque seco tropical en Colombia",
            ],
            moderators: [
                "Carolina Sanín, ecóloga, coordinadora de proyectos Parque Explora."
            ]
        },
        capacity: '15',
        keywords: "videojuegos, educativo, scractch, aprende, explora, maestros, exploratorio"
    },
    {
        id: "0010",
        title: "Aguapanela arvi y otros arácnidos",
        date: {
            day: '05',
            month: 'AGO',
            monthExtend: 'Agosto',
            year: '2022'
        },
        dateCalendar: {
            day: 5,
            month: 8,
            year: 2022
        },
        time: "8:00 a 10:00 a.m",
        dateExtend: "05 Agosto - 8:00 a 10:00 a.m",
        category: "Aprende",
        location: "Parque Explora",
        audience: "Familiar",
        modality: "Presencial",
        price: "$35.000",
        img: "events/img2",
        shortDescription: "Como dos manos que sostienen un hilo de agua, el encuentro entre las cordilleras  Central  y Occidental crea dos valles interandinos y en uno de ellos florece una de las regiones más biodiversas de Colombia, el cañón del río Cauca.",
        descriptionModality: "Taller presencial y en vivo",
        descriptionExtend: {
            title: "aves, anfibios, reptiles y plantas",
            shortDescription: "Como dos manos que sostienen un hilo de agua, el encuentro entre las cordilleras  Central  y Occidental crea dos valles interandinos y en uno de ellos florece una de las regiones más biodiversas de Colombia, el cañón del río Cauca.",
            longDescription: [
                "A lado y lado del río, se levanta un espeso bosque seco tropical con más de 600 especies de plantas pertenecientes a 99 familias, como las gigantes ceibas o los árboles “fruto de cerebro” una especie única para la ciencia.",
                "Allí habita una de las serpientes más pequeñas del mundo que enroscada apenas supera el tamaño de una moneda, ranas arbóreas que solo están presentes en este lugar del planeta y una enorme variedad de insectos. La variedad climática que se da en el cañón, que abarca 80 kilómetros desde la zona semidesértica próxima a Santa Fe de Antioquia hasta las zonas lluviosas en Valdivia, permite que cerca de 290 especies de aves, como  el pájaro hormiguero pico de hacha o las guacamayas verdes, se alberguen allí.",
                "Te invitamos a empezar la noche con una expedición por la diversidad de la vida en la cuenca de este río, el jueves 4 de noviembre, a las 7:00 p.m. por YouTube y Facebook en un nuevo episodio de Ciencia en bicicleta."
            ],
            participants: [
                "Héctor Fabio Rivera Gutiérrez, Ph. D. en Biología, Universidad de Amberes, M. Sc. en Ecología Vrije Universiteit Brussels. coordinador del Grupo Ecología y Evolución de Vertebrados de la UdeA.  Investigador de las aves que habitan la zona del proyecto hidroeléctrico Ituango, con EPM  y docente de ornitología y bioacústica. Ganador del premio a la Extensión 2021 de la UdeA.",
                "Esteban Alzate Basto, biólogo herpetólogo, docente investigador y curador de la colección de herpetología de la Colecciones Biológicas de la Universidad CES.",
                " Álvaro Vásquez, ingeniero forestal, magíster en Bosques y Conservación Ambiental. Experto en áreas de restauración y conservación de bosque seco tropical en Colombia",
            ],
            moderators: [
                "Carolina Sanín, ecóloga, coordinadora de proyectos Parque Explora."
            ]
        },
        capacity: '15',
        keywords: "insectos, arvi, aguapanela, arácnidos, aprende, explora, familia"
    },
    {
        id: "0011",
        title: "Taller en Moravia: la Física de un soplido",
        date: {
            day: '01',
            month: 'AGO',
            monthExtend: 'Agosto',
            year: '2022'
        },
        dateCalendar: {
            day: 1,
            month: 8,
            year: 2022
        },
        time: "8:00 a 10:00 a.m",
        dateExtend: "01 Agosto - 8:00 a 10:00 a.m",
        category: "Aprende",
        location: "Parque Explora",
        audience: "Jóvenes y adultos",
        modality: "Presencial",
        price: "$35.000",
        img: "events/img1",
        shortDescription: "Como dos manos que sostienen un hilo de agua, el encuentro entre las cordilleras  Central  y Occidental crea dos valles interandinos y en uno de ellos florece una de las regiones más biodiversas de Colombia, el cañón del río Cauca.",
        descriptionModality: "Taller presencial y en vivo",
        descriptionExtend: {
            title: "aves, anfibios, reptiles y plantas",
            shortDescription: "Como dos manos que sostienen un hilo de agua, el encuentro entre las cordilleras  Central  y Occidental crea dos valles interandinos y en uno de ellos florece una de las regiones más biodiversas de Colombia, el cañón del río Cauca.",
            longDescription: [
                "A lado y lado del río, se levanta un espeso bosque seco tropical con más de 600 especies de plantas pertenecientes a 99 familias, como las gigantes ceibas o los árboles “fruto de cerebro” una especie única para la ciencia.",
                "Allí habita una de las serpientes más pequeñas del mundo que enroscada apenas supera el tamaño de una moneda, ranas arbóreas que solo están presentes en este lugar del planeta y una enorme variedad de insectos. La variedad climática que se da en el cañón, que abarca 80 kilómetros desde la zona semidesértica próxima a Santa Fe de Antioquia hasta las zonas lluviosas en Valdivia, permite que cerca de 290 especies de aves, como  el pájaro hormiguero pico de hacha o las guacamayas verdes, se alberguen allí.",
                "Te invitamos a empezar la noche con una expedición por la diversidad de la vida en la cuenca de este río, el jueves 4 de noviembre, a las 7:00 p.m. por YouTube y Facebook en un nuevo episodio de Ciencia en bicicleta."
            ],
            participants: [
                "Héctor Fabio Rivera Gutiérrez, Ph. D. en Biología, Universidad de Amberes, M. Sc. en Ecología Vrije Universiteit Brussels. coordinador del Grupo Ecología y Evolución de Vertebrados de la UdeA.  Investigador de las aves que habitan la zona del proyecto hidroeléctrico Ituango, con EPM  y docente de ornitología y bioacústica. Ganador del premio a la Extensión 2021 de la UdeA.",
                "Esteban Alzate Basto, biólogo herpetólogo, docente investigador y curador de la colección de herpetología de la Colecciones Biológicas de la Universidad CES.",
                " Álvaro Vásquez, ingeniero forestal, magíster en Bosques y Conservación Ambiental. Experto en áreas de restauración y conservación de bosque seco tropical en Colombia",
            ],
            moderators: [
                "Carolina Sanín, ecóloga, coordinadora de proyectos Parque Explora."
            ]
        },
        capacity: '15',
        keywords: "física, taller, moravia, ciencia, aprende, explora"
    },
    {
        id: "0012",
        title: "Aguapanela arvi y otros arácnidos",
        date: {
            day: '02',
            month: 'AGO',
            monthExtend: 'Agosto',
            year: '2022'
        },
        dateCalendar: {
            day: 2,
            month: 8,
            year: 2022
        },
        time: "8:00 a 10:00 a.m",
        dateExtend: "02 Agosto - 8:00 a 10:00 a.m",
        category: "Aprende",
        location: "Parque Explora",
        audience: "Familiar",
        modality: "Presencial",
        price: "$35.000",
        img: "events/img2",
        shortDescription: "Como dos manos que sostienen un hilo de agua, el encuentro entre las cordilleras  Central  y Occidental crea dos valles interandinos y en uno de ellos florece una de las regiones más biodiversas de Colombia, el cañón del río Cauca.",
        descriptionModality: "Taller presencial y en vivo",
        descriptionExtend: {
            title: "aves, anfibios, reptiles y plantas",
            shortDescription: "Como dos manos que sostienen un hilo de agua, el encuentro entre las cordilleras  Central  y Occidental crea dos valles interandinos y en uno de ellos florece una de las regiones más biodiversas de Colombia, el cañón del río Cauca.",
            longDescription: [
                "A lado y lado del río, se levanta un espeso bosque seco tropical con más de 600 especies de plantas pertenecientes a 99 familias, como las gigantes ceibas o los árboles “fruto de cerebro” una especie única para la ciencia.",
                "Allí habita una de las serpientes más pequeñas del mundo que enroscada apenas supera el tamaño de una moneda, ranas arbóreas que solo están presentes en este lugar del planeta y una enorme variedad de insectos. La variedad climática que se da en el cañón, que abarca 80 kilómetros desde la zona semidesértica próxima a Santa Fe de Antioquia hasta las zonas lluviosas en Valdivia, permite que cerca de 290 especies de aves, como  el pájaro hormiguero pico de hacha o las guacamayas verdes, se alberguen allí.",
                "Te invitamos a empezar la noche con una expedición por la diversidad de la vida en la cuenca de este río, el jueves 4 de noviembre, a las 7:00 p.m. por YouTube y Facebook en un nuevo episodio de Ciencia en bicicleta."
            ],
            participants: [
                "Héctor Fabio Rivera Gutiérrez, Ph. D. en Biología, Universidad de Amberes, M. Sc. en Ecología Vrije Universiteit Brussels. coordinador del Grupo Ecología y Evolución de Vertebrados de la UdeA.  Investigador de las aves que habitan la zona del proyecto hidroeléctrico Ituango, con EPM  y docente de ornitología y bioacústica. Ganador del premio a la Extensión 2021 de la UdeA.",
                "Esteban Alzate Basto, biólogo herpetólogo, docente investigador y curador de la colección de herpetología de la Colecciones Biológicas de la Universidad CES.",
                " Álvaro Vásquez, ingeniero forestal, magíster en Bosques y Conservación Ambiental. Experto en áreas de restauración y conservación de bosque seco tropical en Colombia",
            ],
            moderators: [
                "Carolina Sanín, ecóloga, coordinadora de proyectos Parque Explora."
            ]
        },
        capacity: '15',
        keywords: "insectos, arvi, aguapanela, arácnidos, aprende, explora, familia"
    },
    {
        id: "0013",
        title: "Accesibilidad: ciencia con todos los sentidos",
        date: {
            day: '03',
            month: 'AGO',
            monthExtend: 'Agosto',
            year: '2022'
        },
        dateCalendar: {
            day: 3,
            month: 8,
            year: 2022
        },
        time: "8:00 a 10:00 a.m",
        dateExtend: "03 Agosto - 8:00 a 10:00 a.m",
        category: "Aprende",
        location: "Planetario",
        audience: "Niños",
        modality: "Presencial",
        price: "0",
        free: 'Gratis',
        img: "events/img3",
        shortDescription: "Como dos manos que sostienen un hilo de agua, el encuentro entre las cordilleras  Central  y Occidental crea dos valles interandinos y en uno de ellos florece una de las regiones más biodiversas de Colombia, el cañón del río Cauca.",
        descriptionModality: "Taller presencial y en vivo",
        descriptionExtend: {
            title: "aves, anfibios, reptiles y plantas",
            shortDescription: "Como dos manos que sostienen un hilo de agua, el encuentro entre las cordilleras  Central  y Occidental crea dos valles interandinos y en uno de ellos florece una de las regiones más biodiversas de Colombia, el cañón del río Cauca.",
            longDescription: [
                "A lado y lado del río, se levanta un espeso bosque seco tropical con más de 600 especies de plantas pertenecientes a 99 familias, como las gigantes ceibas o los árboles “fruto de cerebro” una especie única para la ciencia.",
                "Allí habita una de las serpientes más pequeñas del mundo que enroscada apenas supera el tamaño de una moneda, ranas arbóreas que solo están presentes en este lugar del planeta y una enorme variedad de insectos. La variedad climática que se da en el cañón, que abarca 80 kilómetros desde la zona semidesértica próxima a Santa Fe de Antioquia hasta las zonas lluviosas en Valdivia, permite que cerca de 290 especies de aves, como  el pájaro hormiguero pico de hacha o las guacamayas verdes, se alberguen allí.",
                "Te invitamos a empezar la noche con una expedición por la diversidad de la vida en la cuenca de este río, el jueves 4 de noviembre, a las 7:00 p.m. por YouTube y Facebook en un nuevo episodio de Ciencia en bicicleta."
            ],
            participants: [
                "Héctor Fabio Rivera Gutiérrez, Ph. D. en Biología, Universidad de Amberes, M. Sc. en Ecología Vrije Universiteit Brussels. coordinador del Grupo Ecología y Evolución de Vertebrados de la UdeA.  Investigador de las aves que habitan la zona del proyecto hidroeléctrico Ituango, con EPM  y docente de ornitología y bioacústica. Ganador del premio a la Extensión 2021 de la UdeA.",
                "Esteban Alzate Basto, biólogo herpetólogo, docente investigador y curador de la colección de herpetología de la Colecciones Biológicas de la Universidad CES.",
                " Álvaro Vásquez, ingeniero forestal, magíster en Bosques y Conservación Ambiental. Experto en áreas de restauración y conservación de bosque seco tropical en Colombia",
            ],
            moderators: [
                "Carolina Sanín, ecóloga, coordinadora de proyectos Parque Explora."
            ]
        },
        capacity: '15',
        keywords: "accesibilidad, ciencia, planetario, aprende, explora, niños"
    },
    {
        id: "0014",
        title: "Un videojuego educativo con Scractch",
        date: {
            day: '04',
            month: 'AGO',
            monthExtend: 'Agosto',
            year: '2022'
        },
        dateCalendar: {
            day: 4,
            month: 8,
            year: 2022
        },
        time: "8:00 a 10:00 a.m",
        dateExtend: "04 Agosto - 8:00 a 10:00 a.m",
        category: "Aprende",
        location: "Exploratorio",
        audience: "Maestros",
        modality: "Online",
        price: "0",
        free: 'Gratis',
        img: "events/img4",
        shortDescription: "Como dos manos que sostienen un hilo de agua, el encuentro entre las cordilleras  Central  y Occidental crea dos valles interandinos y en uno de ellos florece una de las regiones más biodiversas de Colombia, el cañón del río Cauca.",
        descriptionModality: "Taller presencial y en vivo",
        descriptionExtend: {
            title: "aves, anfibios, reptiles y plantas",
            shortDescription: "Como dos manos que sostienen un hilo de agua, el encuentro entre las cordilleras  Central  y Occidental crea dos valles interandinos y en uno de ellos florece una de las regiones más biodiversas de Colombia, el cañón del río Cauca.",
            longDescription: [
                "A lado y lado del río, se levanta un espeso bosque seco tropical con más de 600 especies de plantas pertenecientes a 99 familias, como las gigantes ceibas o los árboles “fruto de cerebro” una especie única para la ciencia.",
                "Allí habita una de las serpientes más pequeñas del mundo que enroscada apenas supera el tamaño de una moneda, ranas arbóreas que solo están presentes en este lugar del planeta y una enorme variedad de insectos. La variedad climática que se da en el cañón, que abarca 80 kilómetros desde la zona semidesértica próxima a Santa Fe de Antioquia hasta las zonas lluviosas en Valdivia, permite que cerca de 290 especies de aves, como  el pájaro hormiguero pico de hacha o las guacamayas verdes, se alberguen allí.",
                "Te invitamos a empezar la noche con una expedición por la diversidad de la vida en la cuenca de este río, el jueves 4 de noviembre, a las 7:00 p.m. por YouTube y Facebook en un nuevo episodio de Ciencia en bicicleta."
            ],
            participants: [
                "Héctor Fabio Rivera Gutiérrez, Ph. D. en Biología, Universidad de Amberes, M. Sc. en Ecología Vrije Universiteit Brussels. coordinador del Grupo Ecología y Evolución de Vertebrados de la UdeA.  Investigador de las aves que habitan la zona del proyecto hidroeléctrico Ituango, con EPM  y docente de ornitología y bioacústica. Ganador del premio a la Extensión 2021 de la UdeA.",
                "Esteban Alzate Basto, biólogo herpetólogo, docente investigador y curador de la colección de herpetología de la Colecciones Biológicas de la Universidad CES.",
                " Álvaro Vásquez, ingeniero forestal, magíster en Bosques y Conservación Ambiental. Experto en áreas de restauración y conservación de bosque seco tropical en Colombia",
            ],
            moderators: [
                "Carolina Sanín, ecóloga, coordinadora de proyectos Parque Explora."
            ]
        },
        capacity: '15',
        keywords: "videojuegos, educativo, scractch, aprende, explora, maestros, exploratorio"
    },
    {
        id: "0015",
        title: "Aguapanela arvi y otros arácnidos",
        date: {
            day: '05',
            month: 'AGO',
            monthExtend: 'Agosto',
            year: '2022'
        },
        dateCalendar: {
            day: 5,
            month: 8,
            year: 2022
        },
        time: "8:00 a 10:00 a.m",
        dateExtend: "05 Agosto - 8:00 a 10:00 a.m",
        category: "Aprende",
        location: "Parque Explora",
        audience: "Familiar",
        modality: "Presencial",
        price: "$35.000",
        img: "events/img2",
        shortDescription: "Como dos manos que sostienen un hilo de agua, el encuentro entre las cordilleras  Central  y Occidental crea dos valles interandinos y en uno de ellos florece una de las regiones más biodiversas de Colombia, el cañón del río Cauca.",
        descriptionModality: "Taller presencial y en vivo",
        descriptionExtend: {
            title: "aves, anfibios, reptiles y plantas",
            shortDescription: "Como dos manos que sostienen un hilo de agua, el encuentro entre las cordilleras  Central  y Occidental crea dos valles interandinos y en uno de ellos florece una de las regiones más biodiversas de Colombia, el cañón del río Cauca.",
            longDescription: [
                "A lado y lado del río, se levanta un espeso bosque seco tropical con más de 600 especies de plantas pertenecientes a 99 familias, como las gigantes ceibas o los árboles “fruto de cerebro” una especie única para la ciencia.",
                "Allí habita una de las serpientes más pequeñas del mundo que enroscada apenas supera el tamaño de una moneda, ranas arbóreas que solo están presentes en este lugar del planeta y una enorme variedad de insectos. La variedad climática que se da en el cañón, que abarca 80 kilómetros desde la zona semidesértica próxima a Santa Fe de Antioquia hasta las zonas lluviosas en Valdivia, permite que cerca de 290 especies de aves, como  el pájaro hormiguero pico de hacha o las guacamayas verdes, se alberguen allí.",
                "Te invitamos a empezar la noche con una expedición por la diversidad de la vida en la cuenca de este río, el jueves 4 de noviembre, a las 7:00 p.m. por YouTube y Facebook en un nuevo episodio de Ciencia en bicicleta."
            ],
            participants: [
                "Héctor Fabio Rivera Gutiérrez, Ph. D. en Biología, Universidad de Amberes, M. Sc. en Ecología Vrije Universiteit Brussels. coordinador del Grupo Ecología y Evolución de Vertebrados de la UdeA.  Investigador de las aves que habitan la zona del proyecto hidroeléctrico Ituango, con EPM  y docente de ornitología y bioacústica. Ganador del premio a la Extensión 2021 de la UdeA.",
                "Esteban Alzate Basto, biólogo herpetólogo, docente investigador y curador de la colección de herpetología de la Colecciones Biológicas de la Universidad CES.",
                " Álvaro Vásquez, ingeniero forestal, magíster en Bosques y Conservación Ambiental. Experto en áreas de restauración y conservación de bosque seco tropical en Colombia",
            ],
            moderators: [
                "Carolina Sanín, ecóloga, coordinadora de proyectos Parque Explora."
            ]
        },
        capacity: '15',
        keywords: "insectos, arvi, aguapanela, arácnidos, aprende, explora, familia"
    },
    {
        id: "00016",
        title: "Taller en Moravia: la Física de un soplido",
        date: {
            day: '01',
            month: 'AGO',
            monthExtend: 'Agosto',
            year: '2022'
        },
        dateCalendar: {
            day: 1,
            month: 8,
            year: 2022
        },
        time: "8:00 a 10:00 a.m",
        dateExtend: "01 Agosto - 8:00 a 10:00 a.m",
        category: "Aprende",
        location: "Parque Explora",
        audience: "Jóvenes y adultos",
        modality: "Presencial",
        price: "$35.000",
        img: "events/img1",
        shortDescription: "Como dos manos que sostienen un hilo de agua, el encuentro entre las cordilleras  Central  y Occidental crea dos valles interandinos y en uno de ellos florece una de las regiones más biodiversas de Colombia, el cañón del río Cauca.",
        descriptionModality: "Taller presencial y en vivo",
        descriptionExtend: {
            title: "aves, anfibios, reptiles y plantas",
            shortDescription: "Como dos manos que sostienen un hilo de agua, el encuentro entre las cordilleras  Central  y Occidental crea dos valles interandinos y en uno de ellos florece una de las regiones más biodiversas de Colombia, el cañón del río Cauca.",
            longDescription: [
                "A lado y lado del río, se levanta un espeso bosque seco tropical con más de 600 especies de plantas pertenecientes a 99 familias, como las gigantes ceibas o los árboles “fruto de cerebro” una especie única para la ciencia.",
                "Allí habita una de las serpientes más pequeñas del mundo que enroscada apenas supera el tamaño de una moneda, ranas arbóreas que solo están presentes en este lugar del planeta y una enorme variedad de insectos. La variedad climática que se da en el cañón, que abarca 80 kilómetros desde la zona semidesértica próxima a Santa Fe de Antioquia hasta las zonas lluviosas en Valdivia, permite que cerca de 290 especies de aves, como  el pájaro hormiguero pico de hacha o las guacamayas verdes, se alberguen allí.",
                "Te invitamos a empezar la noche con una expedición por la diversidad de la vida en la cuenca de este río, el jueves 4 de noviembre, a las 7:00 p.m. por YouTube y Facebook en un nuevo episodio de Ciencia en bicicleta."
            ],
            participants: [
                "Héctor Fabio Rivera Gutiérrez, Ph. D. en Biología, Universidad de Amberes, M. Sc. en Ecología Vrije Universiteit Brussels. coordinador del Grupo Ecología y Evolución de Vertebrados de la UdeA.  Investigador de las aves que habitan la zona del proyecto hidroeléctrico Ituango, con EPM  y docente de ornitología y bioacústica. Ganador del premio a la Extensión 2021 de la UdeA.",
                "Esteban Alzate Basto, biólogo herpetólogo, docente investigador y curador de la colección de herpetología de la Colecciones Biológicas de la Universidad CES.",
                " Álvaro Vásquez, ingeniero forestal, magíster en Bosques y Conservación Ambiental. Experto en áreas de restauración y conservación de bosque seco tropical en Colombia",
            ],
            moderators: [
                "Carolina Sanín, ecóloga, coordinadora de proyectos Parque Explora."
            ]
        },
        capacity: '15',
        keywords: "física, taller, moravia, ciencia, aprende, explora"
    },
]