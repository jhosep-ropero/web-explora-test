export const tickets= [
    {
        id:"001",
        img: "/assets/tickets/parqueexplora.jpg",
        icon: "icon-about-b",
        title: "Todo Explora",
        subtitle: "",
        currency: "$",
        price: "32.000",
        description: "Brazalete para ingreso al Parque. Incluye: Acuario, Exposición Dinosaurios, Vivario, Sala Abierta, todas las salas interactivas habilitadas en el tercer nivel del Parque.",
        button: "Comprar tiquetes"
    },
    {
        id:"002",
        img: "/assets/tickets/explora-planetario.jpg",
        icon: "icon-fullpack",
        title: "Combo Explora + Planetario",
        subtitle: "",
        currency: "$",
        price: "48.000",
        description: "Ingreso al Acuario + Exposición Dinosaurios, Sala Abierta + Acuario + Sala Tiempo + Sala Mente + Sala En Escena + Sala Música + Vivario y en el Planetario incluye ingreso una proyección en el Domo + experiencias del museo. Aplica para 2 días si en un día no termina el recorrido.",
        button: "Comprar tiquetes"
    },
    {
        id:"003",
        img: "/assets/tickets/acuarioexplora.jpg",
        icon: "icon-family",
        title: "Plan Familiar",
        subtitle: "",
        currency: "$",
        price: "116.000",
        description: "Ingreso para grupo familiar de 4 personas. Incluye el ingreso para 4 personas Sala abierta + Acuario + Sala tiempo + Sala mente + Sala en escena + Sala música+ Vivario + Exposición de Dinosaurios.",
        button: "Comprar tiquetes"
    },
    {
        id:"004",
        img: "/assets/tickets/regalaunaboleta.jpg",
        icon: "icon-gift-ticket",
        title: "Regala una boleta",
        subtitle: "",
        currency: "$",
        price: "32.000",
        description: "Sorprende a los que más quieres con una entrada al Parque. El brazalete para el ingreso incluye: Acuario, Exposición Dinosaurios, Vivario, Sala Abierta, todas las salas interactivas habilitadas en el tercer nivel del Parque.",
        button: "Regalar boleta"
    },
    {
        id:"005",
        img: "/assets/tickets/medellin.jpg",
        title: "Descuento atracciones turísticas",
        icon: "icon-city",
        subtitle: "Medellín city card",
        currency: "$",
        price: "86.000",
        description: "Entrada a Parque Explora, Parque Arví, Parque de la Conservación y Museo de Antioquia.",
        button: "Adquirir descuento"
    }
]